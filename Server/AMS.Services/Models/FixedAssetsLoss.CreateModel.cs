﻿namespace AMS.Services.Models;

public partial class FixedAssetsLoss
{
    public class CreateModel
    {
        /// <summary>
        /// 报损明细
        /// </summary>
        [DisplayName("报损明细")]
        public List<FixedAssetsLossDetail.CreateModel> LossDetails { get; set; } = new();

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }
    }
}
