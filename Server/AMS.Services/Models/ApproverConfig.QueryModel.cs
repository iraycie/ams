﻿namespace AMS.Services.Models;

public partial class ApproverConfig
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 业务流程的Key
        /// </summary>
        [DisplayName("Key")]
        public string Key { get; set; } = string.Empty;

        /// <summary>
        /// 业务名称
        /// </summary>
        [DisplayName("业务名称")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 审批人明细
        /// </summary>
        [DisplayName("审批人明细")]
        public IEnumerable<int> ApproverDetails { get; set; } = Enumerable.Empty<int>();
    }
}