﻿namespace AMS.Services.Models;

public partial class FreeAssetsConsuming
{
    public class CreateModel
    {
        /// <summary>
        /// 资产
        /// </summary>
        [DisplayName("资产")]
        public int AssetsId { get; set; }

        /// <summary>
        /// 领用数量
        /// </summary>
        [DisplayName("领用数量")]
        public int Count { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }
    }
}