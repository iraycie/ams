﻿namespace AMS.Services.Models;

/// <summary>
/// 审批
/// </summary>
[DisplayName("审批")]
public partial class Approval : EntityBase
{
    /// <summary>
    /// 审批类型
    /// </summary>
    [DisplayName("审批类型")]
    public ApprovalType Type { get; set; }

    /// <summary>
    /// 表单Id
    /// </summary>
    [DisplayName("表单ID")]
    public int FormId { get; set; }

    /// <summary>
    /// 资产类型
    /// </summary>
    [DisplayName("资产类型")]
    public AssetsType AssetsType { get; set; }
    
    /// <summary>
    /// 审批明细
    /// </summary>
    [DisplayName("审批明细")]
    public ICollection<ApprovalDetail> ApprovalDetails { get; set; } = new HashSet<ApprovalDetail>();

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 提交者
    /// </summary>
    [DisplayName("提交者")]
    public User Committer { get; set; } = null!;

    /// <summary>
    /// 审批状态
    /// </summary>
    [DisplayName("审批状态")]
    public ApprovalStatus Status { get; set; } = ApprovalStatus.Pending;
}