﻿namespace AMS.Services.Models;

/// <summary>
/// 审批配置
/// </summary>
[DisplayName("审批配置")]
public partial class ApproverConfig : EntityBase
{
    /// <summary>
    /// 业务流程的Key
    /// </summary>
    [DisplayName("Key")]
    [MaxLength(32)]
    public string Key { get; set; } = string.Empty;

    /// <summary>
    /// 业务名称
    /// </summary>
    [DisplayName("业务名称")]
    [MaxLength(32)]
    public string Name { get; set; } = String.Empty;

    /// <summary>
    /// 审批人明细
    /// </summary>
    [DisplayName("审批人明细")]
    public ICollection<ApproverConfigDetail> ApproverDetails { get; set; } = new HashSet<ApproverConfigDetail>();
}