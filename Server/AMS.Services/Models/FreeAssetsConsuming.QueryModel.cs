﻿namespace AMS.Services.Models;

public partial class FreeAssetsConsuming
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 资产
        /// </summary>
        [DisplayName("资产")]
        public FreeAssets.QueryModel Assets { get; set; } = null!;

        /// <summary>
        /// 领用数量
        /// </summary>
        [DisplayName("领用数量")]
        public int Count { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }

        /// <summary>
        /// 领用人
        /// </summary>
        [DisplayName("领用人")]
        public User.QueryModel Consumer { get; set; } = null!;

        /// <summary>
        /// 审批状态
        /// </summary>
        [DisplayName("审批状态")]
        public ApprovalStatus Status { get; set; }
    }
}