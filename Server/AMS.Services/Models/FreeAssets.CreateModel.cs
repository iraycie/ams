﻿namespace AMS.Services.Models;

public partial class FreeAssets
{
    public class CreateModel
    {
        /// <summary>
        /// 资产名称
        /// </summary>
        [DisplayName("资产名称")]
        [MaxLength(64)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 资产类别
        /// </summary>
        [DisplayName("资产类别")]
        public int CategoryId { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        [DisplayName("供应商")]
        public int SupplierId { get; set; }

        /// <summary>
        /// 批次
        /// </summary>
        [DisplayName("批次")]
        [MaxLength(32)]
        public string Batch { get; set; } = string.Empty;

        /// <summary>
        /// 单价
        /// </summary>
        [DisplayName("单价")]
        public decimal Univalent { get; set; }

        /// <summary>
        /// 描述信息
        /// </summary>
        [DisplayName("描述信息")]
        public string? Description { get; set; }
    }
}