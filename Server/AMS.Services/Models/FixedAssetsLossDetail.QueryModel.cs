﻿namespace AMS.Services.Models;

public partial class FixedAssetsLossDetail
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 持有资产
        /// </summary>
        [DisplayName("持有资产")]

        public FixedAssets.QueryModel Assets { get; set; } = null!;

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }
    }
}