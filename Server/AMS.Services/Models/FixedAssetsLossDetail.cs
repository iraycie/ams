﻿namespace AMS.Services.Models;

/// <summary>
/// 报损明细
/// </summary>
[DisplayName("报损明细")]
public partial class FixedAssetsLossDetail : EntityBase
{
    /// <summary>
    /// 持有资产
    /// </summary>
    [DisplayName("持有资产")]
    public FixedAssets Assets { get; set; } = null!;

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    public string? Remark { get; set; }
}