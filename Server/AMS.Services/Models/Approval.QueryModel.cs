﻿namespace AMS.Services.Models;

public partial class Approval
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 审批类型
        /// </summary>
        [DisplayName("审批类型")]
        public ApprovalType Type { get; set; }

        /// <summary>
        /// 资产类型
        /// </summary>
        [DisplayName("资产类型")]
        public AssetsType AssetsType { get; set; }

        /// <summary>
        /// 审批明细
        /// </summary>
        [DisplayName("审批明细")]
        public IEnumerable<ApprovalDetail.QueryModel> ApprovalDetails { get; set; } = Enumerable.Empty<ApprovalDetail.QueryModel>();

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }

        /// <summary>
        /// 提交者
        /// </summary>
        [DisplayName("提交者")]
        public User.QueryModel Committer { get; set; } = null!;

        /// <summary>
        /// 审批状态
        /// </summary>
        [DisplayName("审批状态")]
        public ApprovalStatus Status { get; set; } = ApprovalStatus.Pending;
    }
}