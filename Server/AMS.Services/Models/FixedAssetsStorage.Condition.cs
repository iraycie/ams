﻿namespace AMS.Services.Models;

public partial class FixedAssetsStorage
{
    [DisplayName("条件查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("关键词")]
        public string? Keywords { get; set; }

        [DisplayName("请选择")]
        public ApprovalStatus? StatusSelector { get; set; }

        [DisplayName("请选择")]
        public int? SupplierSelectorId { get; set; }

        [DisplayName("请选择")]
        public int? CategorySelectorId { get; set; }
    }
}