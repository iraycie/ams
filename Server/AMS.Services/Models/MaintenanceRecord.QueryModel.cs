﻿namespace AMS.Services.Models;

public partial class MaintenanceRecord
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 描述
        /// </summary>
        [DisplayName("描述")]
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// 检修人
        /// </summary>
        [DisplayName("检修人")]
        public string Overhauler { get; set; } = string.Empty;

        /// <summary>
        /// 联系方式
        /// </summary>
        [DisplayName("联系方式")]
        public string Phone { get; set; } = string.Empty;

        /// <summary>
        /// 维修日期
        /// </summary>
        [DisplayName("维修日期")]
        public DateTime RepairDate { get; set; }
    }
}