﻿namespace AMS.Services.Models;

public partial class FixedAssetsLoss
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 报损明细
        /// </summary>
        [DisplayName("报损明细")]
        public IEnumerable<FixedAssetsLossDetail.QueryModel> LossDetails { get; set; } = Enumerable.Empty<FixedAssetsLossDetail.QueryModel>();

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }

        /// <summary>
        /// 上报人
        /// </summary>
        [DisplayName("上报人")]
        public User.QueryModel Reporter { get; set; } = null!;

        /// <summary>
        /// 报损进度
        /// </summary>
        [DisplayName("报损进度")]
        public LossStatus Status { get; set; } = LossStatus.Pending;
    }
}