﻿namespace AMS.Services.Models;

/// <summary>
/// 易耗品资产库存
/// </summary>
[DisplayName("易耗品资产库存")]
public partial class FreeAssetsRepo : EntityBase
{
    /// <summary>
    /// 资产
    /// </summary>
    [DisplayName("资产")]
    public FreeAssets Assets { get; set; } = null!;

    /// <summary>
    /// 库存
    /// </summary>
    [DisplayName("库存")]
    public int Stock { get; set; }

    /// <summary>
    /// 资产状态
    /// </summary>
    [DisplayName("资产状态")]
    public AssetsStatus Status { get; set; } = AssetsStatus.Normal;
}