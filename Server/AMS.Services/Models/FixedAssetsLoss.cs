﻿using System.Text;

namespace AMS.Services.Models;

/// <summary>
/// 资产报损
/// </summary>
[DisplayName("资产报损")]
public partial class FixedAssetsLoss : EntityBase
{
    /// <summary>
    /// 报损明细
    /// </summary>
    [DisplayName("报损明细")]
    public ICollection<FixedAssetsLossDetail> LossDetails { get; set; } = new HashSet<FixedAssetsLossDetail>();

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 上报人
    /// </summary>
    [DisplayName("上报人")]
    public User Reporter { get; set; } = null!;

    /// <summary>
    /// 报损进度
    /// </summary>
    [DisplayName("报损进度")]
    public LossStatus Status { get; set; } = LossStatus.Pending;

    public override string ToString()
    {
        var sb = new StringBuilder();

        var idx = 1;
        foreach (var detail in LossDetails)
        {
            if (detail.Remark is not null)
            {
                sb.AppendLine($"{idx++}. {detail.Assets.Name}：{detail.Remark}");
            }
            else
            {
                sb.AppendLine($"{idx++}. {detail.Assets.Name}");
            }
        }

        return sb.ToString();
    }
}