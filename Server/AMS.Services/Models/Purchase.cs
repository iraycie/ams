﻿using System.Text;

namespace AMS.Services.Models;

/// <summary>
/// 资产采购
/// </summary>
[DisplayName("资产采购")]
public partial class Purchase : EntityBase
{
    /// <summary>
    /// 采购明细
    /// </summary>
    [DisplayName("采购明细")]
    public ICollection<PurchaseDetail> PurchaseDetails { get; set; } = new HashSet<PurchaseDetail>();

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("采购事由")]
    public string? Remark { get; set; }

    /// <summary>
    /// 申请人
    /// </summary>
    [DisplayName("申请人")]
    public User Commiter { get; set; } = null!;

    /// <summary>
    /// 采购状态
    /// </summary>
    [DisplayName("采购状态")]
    public PurchaseStatus Status { get; set; } = PurchaseStatus.Pending;

    public override string ToString()
    {
        var sb = new StringBuilder();

        var idx = 1;
        var totalPrice = 0m;
        foreach (var detail in PurchaseDetails)
        {
            totalPrice += detail.Univalent * detail.Count;
            sb.AppendLine($"{idx++}. {detail.Name} * {detail.Count}（供应商：{detail.Supplier.Name}）");
        }

        sb.Append($"总金额：{Math.Round(totalPrice, 2)}");

        return sb.ToString();
    }
}