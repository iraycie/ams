﻿namespace AMS.Services.Models;

public partial class Supplier
{
    public class CreateModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName("名称")]
        [MaxLength(32)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 联系人
        /// </summary>
        [DisplayName("联系人")]
        [MaxLength(32)]
        public string Contactor { get; set; } = string.Empty;

        /// <summary>
        /// 联系方式
        /// </summary>
        [DisplayName("联系方式")]
        [RegularExpression(@"^(0|86)?(13[0-9]|15[0-35-9]|17[013678]|18[0-9]|14[57])\d{8}$", ErrorMessage = "联系方式格式不正确。")]
        public string Phone { get; set; } = string.Empty;
    }
}