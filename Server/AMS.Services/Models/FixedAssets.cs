﻿namespace AMS.Services.Models;

/// <summary>
/// 固定资产
/// </summary>
[DisplayName("固定资产")]
public partial class FixedAssets : EntityBase
{
    /// <summary>
    /// 资产序列号
    /// </summary>
    [DisplayName("资产序列号")]
    public string SerialNo { get; set; } = string.Empty;

    /// <summary>
    /// 资产名称
    /// </summary>
    [DisplayName("资产名称")]
    [MaxLength(64)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 资产类别
    /// </summary>
    [DisplayName("资产类别")]
    public AssetsCategory Category { get; set; } = null!;

    /// <summary>
    /// 规格型号
    /// </summary>
    [DisplayName("规格型号")]
    [MaxLength(32)]
    public string Specs { get; set; } = string.Empty;

    /// <summary>
    /// 单价
    /// </summary>
    [DisplayName("单价")]
    [Precision(9, 3)]
    public decimal Univalent { get; set; }

    /// <summary>
    /// 供应商
    /// </summary>
    [DisplayName("供应商")]
    [MaxLength(32)]
    public Supplier Supplier { get; set; } = null!;

    /// <summary>
    /// 描述信息
    /// </summary>
    [DisplayName("描述信息")]
    public string? Description { get; set; }
}