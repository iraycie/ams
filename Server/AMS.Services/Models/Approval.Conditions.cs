﻿using AMS.Services.Enums;

namespace AMS.Services.Models;

public partial class Approval
{
    [DisplayName("条件查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("审批状态")]
        public ApprovalStatus? SelectedApprovalStatus { get; set; }

        [DisplayName("审批类型")]
        public ApprovalType? SelectedApprovalType { get; set; }

        [DisplayName("资产类型")]
        public AssetsType? SelectedAssetsType { get; set; }

        [DisplayName("起始时间")]
        public DateTime? CreatedDateSelectorBeginDate { get; set; }

        [DisplayName("截止时间")]
        public DateTime? CreatedDateSelectorEndDate { get; set; }
    }

    [DisplayName("日期范围")]
    public class CreatedDateSelectorCondition : PageParameter
    {
        [DisplayName("起始时间")]
        public DateTime? BeginDate { get; set; }

        [DisplayName("截止时间")]
        public DateTime? EndDate { get; set; }
    }
}