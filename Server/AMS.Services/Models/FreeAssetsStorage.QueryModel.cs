﻿namespace AMS.Services.Models;

public partial class FreeAssetsStorage
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 资产名称
        /// </summary>
        [DisplayName("资产名称")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 资产类别
        /// </summary>
        [DisplayName("资产类别")]
        public AssetsCategory.QueryModel Category { get; set; } = null!;

        /// <summary>
        /// 供应商
        /// </summary>
        [DisplayName("供应商")]
        public Supplier.QueryModel Supplier { get; set; } = null!;

        /// <summary>
        /// 批次
        /// </summary>
        [DisplayName("批次")]
        public string Batch { get; set; } = string.Empty;

        /// <summary>
        /// 入库数量
        /// </summary>
        [DisplayName("入库数量")]
        public int Count { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        [DisplayName("单价")]
        public decimal Univalent { get; set; }

        /// <summary>
        /// 描述信息
        /// </summary>
        [DisplayName("描述信息")]
        public string? Description { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }

        /// <summary>
        /// 申请人
        /// </summary>
        [DisplayName("申请人")]
        public User.QueryModel Commiter { get; set; } = null!;

        /// <summary>
        /// 审批状态
        /// </summary>
        [DisplayName("审批状态")]
        public ApprovalStatus Status { get; set; }
    }
}