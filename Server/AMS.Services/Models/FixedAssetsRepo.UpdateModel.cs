﻿namespace AMS.Services.Models;

public partial class FixedAssetsRepo
{
    public class UpdateModel
    {
        /// <summary>
        /// 存放地点
        /// </summary>
        [DisplayName("存放地点")]
        [MaxLength(32)]
        public string StorageLocation { get; set; } = string.Empty;
    }
}