﻿namespace AMS.Services.Models;

public partial class FixedAssets
{
    public class UpdateModel
    {
        /// <summary>
        /// 资产名称
        /// </summary>
        [DisplayName("资产名称")]
        [MaxLength(64)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 资产类别
        /// </summary>
        [DisplayName("资产类别")]
        public int CategoryId { get; set; }

        /// <summary>
        /// 规格型号
        /// </summary>
        [DisplayName("规格型号")]
        [MaxLength(32)]
        public string Specs { get; set; } = string.Empty;

        /// <summary>
        /// 单价
        /// </summary>
        [DisplayName("单价")]
        public decimal Univalent { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        [DisplayName("供应商")]
        [MaxLength(32)]
        public int SupplierId { get; set; }

        /// <summary>
        /// 描述信息
        /// </summary>
        [DisplayName("描述信息")]
        public string? Description { get; set; }
    }
}