﻿namespace AMS.Services.Models;

/// <summary>
/// 资产类别
/// </summary>
[DisplayName("资产类别")]
public partial class AssetsCategory : EntityBase
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    [MaxLength(32)]
    public string Name { get; set; } = string.Empty;
}