﻿namespace AMS.Services.Models;

/// <summary>
/// 固定资产库存
/// </summary>
[DisplayName("固定资产库存")]
public partial class FixedAssetsRepo : EntityBase
{
    /// <summary>
    /// 资产
    /// </summary>
    [DisplayName("资产")]
    public FixedAssets Assets { get; set; } = null!;

    /// <summary>
    /// 持有人
    /// </summary>
    [DisplayName("持有人")]
    public User? Owner { get; set; }

    /// <summary>
    /// 存放地点
    /// </summary>
    [DisplayName("存放地点")]
    [MaxLength(32)]
    public string StorageLocation { get; set; } = string.Empty;

    /// <summary>
    /// 使用地点
    /// </summary>
    [DisplayName("使用地点")]
    [MaxLength(32)]
    public string? UseLocation { get; set; }

    /// <summary>
    /// 购买日期
    /// </summary>
    [DisplayName("购买日期")]
    public DateTime? PurchaseDate { get; set; }

    /// <summary>
    /// 录入日期
    /// </summary>
    [DisplayName("录入日期")]
    public DateTime EntryDate { get; set; } = DateTime.UtcNow;

    /// <summary>
    /// 资产状态
    /// </summary>
    [DisplayName("资产状态")]
    public AssetsStatus Status { get; set; } = AssetsStatus.Normal;
}