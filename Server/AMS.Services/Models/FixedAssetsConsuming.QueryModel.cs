﻿namespace AMS.Services.Models;

public partial class FixedAssetsConsuming
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 资产
        /// </summary>
        [DisplayName("资产")]
        public FixedAssets.QueryModel Assets { get; set; } = null!;

        /// <summary>
        /// 使用地点
        /// </summary>
        [DisplayName("使用地点")]
        public string? UseLocation { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }

        /// <summary>
        /// 领用人
        /// </summary>
        [DisplayName("领用人")]
        public User.QueryModel Consumer { get; set; } = null!;

        /// <summary>
        /// 归还中
        /// </summary>
        [DisplayName("归还中")]
        public bool? Returning { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
        [DisplayName("审批状态")]
        public ApprovalStatus Status { get; set; }
    }
}