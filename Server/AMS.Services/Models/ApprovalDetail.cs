﻿namespace AMS.Services.Models;

/// <summary>
/// 审批明细
/// </summary>
[DisplayName("审批明细")]
public partial class ApprovalDetail : EntityBase

{
    /// <summary>
    /// 审批序号
    /// </summary>
    [DisplayName("审批序号")]
    public int Sort { get; set; }

    /// <summary>
    /// 审批人
    /// </summary>
    [DisplayName("审批人")]
    public User Approver { get; set; } = null!;

    /// <summary>
    /// 审批备注
    /// </summary>
    [DisplayName("审批备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 审批状态
    /// </summary>
    [DisplayName("审批状态")]
    public ApprovalStatus Status { get; set; } = ApprovalStatus.Pending;
}