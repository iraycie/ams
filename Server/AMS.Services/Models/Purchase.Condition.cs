﻿namespace AMS.Services.Models;

public partial class Purchase
{
    [DisplayName("条件查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("采购状态")]
        public PurchaseStatus? PurchaseStatus { get; set; }

        [DisplayName("起始时间")]
        public DateTime? CreateDateSelectorBeginDate { get; set; }

        [DisplayName("截止时间")]
        public DateTime? CreateDateSelectorEndDate { get; set; }
    }

    [DisplayName("日期范围")]
    public class CreateDateSelectorCondition : PageParameter
    {
        [DisplayName("起始时间")]
        public DateTime? BeginDate { get; set; }

        [DisplayName("截止时间")]
        public DateTime? EndDate { get; set; }
    }
}