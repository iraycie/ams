﻿namespace AMS.Services.Models;

public partial class MaintenanceRecord
{
    public class CreateModel
    {
        /// <summary>
        /// 描述
        /// </summary>
        [DisplayName("描述")]
        [MinLength(15, ErrorMessage = "描述信息不少于15个字。")]
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// 检修人
        /// </summary>
        [DisplayName("检修人")]
        public string Overhauler { get; set; } = string.Empty;

        /// <summary>
        /// 联系方式
        /// </summary>
        [DisplayName("联系方式")]
        [RegularExpression(@"^(0|86)?(13[0-9]|15[0-35-9]|17[013678]|18[0-9]|14[57])\d{8}$", ErrorMessage = "手机号码格式不正确。")]
        public string Phone { get; set; } = string.Empty;

        /// <summary>
        /// 维修日期
        /// </summary>
        [DisplayName("维修日期")]
        public DateTime RepairDate { get; set; } = DateTime.UtcNow;
    }
}