﻿namespace AMS.Services.Models;

/// <summary>
/// 采购明细
/// </summary>
[DisplayName("采购明细")]
public partial class PurchaseDetail : EntityBase

{
    /// <summary>
    /// 物品名称
    /// </summary>
    [DisplayName("物品名称")]
    [MaxLength(64)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 采购数量
    /// </summary>
    [DisplayName("采购数量")]
    public int Count { get; set; }

    /// <summary>
    /// 单价
    /// </summary>
    [DisplayName("单价")]
    public decimal Univalent { get; set; }

    /// <summary>
    /// 供应商
    /// </summary>
    [DisplayName("供应商")]
    public Supplier Supplier { get; set; } = null!;
}