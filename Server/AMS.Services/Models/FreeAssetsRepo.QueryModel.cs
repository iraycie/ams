﻿namespace AMS.Services.Models;

public partial class FreeAssetsRepo
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 资产
        /// </summary>
        [DisplayName("资产")]
        public FreeAssets.QueryModel Assets { get; set; } = null!;

        /// <summary>
        /// 库存
        /// </summary>
        [DisplayName("库存")]
        public int Stock { get; set; }

        /// <summary>
        /// 资产状态
        /// </summary>
        [DisplayName("资产状态")]
        public AssetsStatus Status { get; set; } = AssetsStatus.Normal;
    }
}