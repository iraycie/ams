﻿namespace AMS.Services.Models;

public partial class FixedAssetsLoss
{
    [DisplayName("条件查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("报损进度")]
        public LossStatus? LossStatus { get; set; }

        [DisplayName("起始时间")]
        public DateTime? CreateDateSelectorBeginDate { get; set; }

        [DisplayName("截止时间")]
        public DateTime? CreateDateSelectorEndDate { get; set; }
    }

    [DisplayName("日期范围")]
    public class CreateDateSelectorCondition : PageParameter
    {
        [DisplayName("起始时间")]
        public DateTime? BeginDate { get; set; }

        [DisplayName("截止时间")]
        public DateTime? EndDate { get; set; }
    }
}