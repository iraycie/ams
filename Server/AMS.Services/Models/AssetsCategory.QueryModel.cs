﻿namespace AMS.Services.Models;

public partial class AssetsCategory
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName("名称")]
        public string Name { get; set; } = string.Empty;
    }
}