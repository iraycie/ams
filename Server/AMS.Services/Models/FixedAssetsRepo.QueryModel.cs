﻿namespace AMS.Services.Models;

public partial class FixedAssetsRepo
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 资产
        /// </summary>
        [DisplayName("资产")]
        public FixedAssets.QueryModel Assets { get; set; } = null!;

        /// <summary>
        /// 持有人
        /// </summary>
        [DisplayName("持有人")]
        public User.QueryModel? Owner { get; set; }

        /// <summary>
        /// 存放地点
        /// </summary>
        [DisplayName("存放地点")]
        public string StorageLocation { get; set; } = null!;

        /// <summary>
        /// 使用地点
        /// </summary>
        [DisplayName("使用地点")]
        public string? UseLocation { get; set; }

        /// <summary>
        /// 购买日期
        /// </summary>
        [DisplayName("购买日期")]
        public DateTime? PurchaseDate { get; set; }

        /// <summary>
        /// 录入日期
        /// </summary>
        [DisplayName("录入日期")]
        public DateTime EntryDate { get; set; }

        /// <summary>
        /// 资产状态
        /// </summary>
        [DisplayName("资产状态")]
        public AssetsStatus Status { get; set; }
    }
}