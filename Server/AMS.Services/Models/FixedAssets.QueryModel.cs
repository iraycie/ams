﻿namespace AMS.Services.Models;

public partial class FixedAssets
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 资产序列号
        /// </summary>
        [DisplayName("资产序列号")]
        public string SerialNo { get; set; } = string.Empty;

        /// <summary>
        /// 资产名称
        /// </summary>
        [DisplayName("资产名称")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 资产类别
        /// </summary>
        [DisplayName("资产类别")]
        public AssetsCategory.QueryModel Category { get; set; } = null!;

        /// <summary>
        /// 规格型号
        /// </summary>
        [DisplayName("规格型号")]
        public string Specs { get; set; } = string.Empty;

        /// <summary>
        /// 单价
        /// </summary>
        [DisplayName("单价")]
        public decimal Univalent { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        [DisplayName("供应商")]
        public Supplier.QueryModel Supplier { get; set; } = null!;

        /// <summary>
        /// 描述信息
        /// </summary>
        [DisplayName("描述信息")]
        public string? Description { get; set; }
    }
}