﻿namespace AMS.Services.Models;

public partial class PurchaseDetail
{
    public class CreateModel
    {
        /// <summary>
        /// 物品名称
        /// </summary>
        [DisplayName("物品名称")]
        [MaxLength(64)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 采购数量
        /// </summary>
        [DisplayName("采购数量")]
        public int Count { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        [DisplayName("单价")]
        public decimal Univalent { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        [DisplayName("供应商")]
        public int SupplierId { get; set; }
    }
}
