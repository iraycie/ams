﻿namespace AMS.Services.Models;

public partial class Purchase
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 采购明细
        /// </summary>
        [DisplayName("采购明细")]
        public IEnumerable<PurchaseDetail.QueryModel> PurchaseDetails { get; set; } = Enumerable.Empty<PurchaseDetail.QueryModel>();

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("采购事由")]
        public string? Remark { get; set; } = string.Empty;

        /// <summary>
        /// 申请人
        /// </summary>
        [DisplayName("申请人")]
        public User.QueryModel Commiter { get; set; } = null!;

        /// <summary>
        /// 采购状态
        /// </summary>
        [DisplayName("采购状态")]
        public PurchaseStatus Status { get; set; }
    }
}
