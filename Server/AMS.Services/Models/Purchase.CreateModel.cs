﻿namespace AMS.Services.Models;

public partial class Purchase
{
    public class CreateModel
    {
        /// <summary>
        /// 采购明细
        /// </summary>
        [DisplayName("采购明细")]
        public List<PurchaseDetail.CreateModel> PurchaseDetails { get; set; } = new();

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("采购事由")]
        public string? Remark { get; set; } = string.Empty;
    }
}
