﻿namespace AMS.Services.Models;

public partial class ApproverConfigDetail
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 审批人
        /// </summary>
        [DisplayName("审批人")]
        public User.QueryModel Approver { get; set; } = null!;

        /// <summary>
        /// 审批顺序
        /// </summary>
        [DisplayName("审批顺序")]
        public int Sort { get; set; }
    }
}