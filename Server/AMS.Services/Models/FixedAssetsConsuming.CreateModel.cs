﻿namespace AMS.Services.Models;

public partial class FixedAssetsConsuming
{
    public class CreateModel
    {
        /// <summary>
        /// 资产
        /// </summary>
        [DisplayName("资产")]
        public int AssetsId { get; set; }

        /// <summary>
        /// 使用地点
        /// </summary>
        [DisplayName("使用地点")]
        [MaxLength(32)]
        public string? UseLocation { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; } = string.Empty;
    }
}