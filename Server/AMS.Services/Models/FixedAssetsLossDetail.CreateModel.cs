﻿namespace AMS.Services.Models;

public partial class FixedAssetsLossDetail
{
    public class CreateModel
    {
        /// <summary>
        /// 持有资产
        /// </summary>
        [DisplayName("持有资产")]
        public int AssetsId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }
    }
}