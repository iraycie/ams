﻿namespace AMS.Services.Models;

/// <summary>
/// 易耗品资产领用
/// </summary>
[DisplayName("易耗品资产领用")]
public partial class FreeAssetsConsuming : EntityBase

{
    /// <summary>
    /// 资产
    /// </summary>
    [DisplayName("资产")]
    public FreeAssets Assets { get; set; } = null!;

    /// <summary>
    /// 领用数量
    /// </summary>
    [DisplayName("领用数量")]
    public int Count { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 领用人
    /// </summary>
    [DisplayName("领用人")]
    public User Consumer { get; set; } = null!;

    /// <summary>
    /// 审批状态
    /// </summary>
    [DisplayName("审批状态")]
    public ApprovalStatus Status { get; set; } = ApprovalStatus.Pending;
}