﻿namespace AMS.Services.Models;

/// <summary>
/// 审批配置明细
/// </summary>
[DisplayName("审批配置明细")]
public partial class ApproverConfigDetail : EntityBase
{
    /// <summary>
    /// 审批人
    /// </summary>
    [DisplayName("审批人")]
    public User Approver { get; set; } = null!;

    /// <summary>
    /// 审批顺序
    /// </summary>
    [DisplayName("审批顺序")]
    public int Sort { get; set; }

    /// <summary>
    /// 外键
    /// </summary>
    public int ApproverConfigId { get; set; }
}