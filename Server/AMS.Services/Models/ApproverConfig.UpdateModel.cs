﻿namespace AMS.Services.Models;

public partial class ApproverConfig
{
    public class UpdateModel
    {
        public int Id { get; set; }

        /// <summary>
        /// 审批人明细
        /// </summary>
        [DisplayName("审批人明细")]
        public List<int> ApproverDetails { get; set; } = new();
    }
}