﻿namespace AMS.Services.Models;

public partial class FixedAssetsStorage
{
    public class CreateModel
    {
        /// <summary>
        /// 资产名称
        /// </summary>
        [DisplayName("资产名称")]
        [MaxLength(64)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 资产类别
        /// </summary>
        [DisplayName("资产类别")]
        public int CategoryId { get; set; }

        /// <summary>
        /// 规格型号
        /// </summary>
        [DisplayName("规格型号")]
        [MaxLength(32)]
        public string Specs { get; set; } = string.Empty;

        /// <summary>
        /// 单价
        /// </summary>
        [DisplayName("单价")]
        public decimal Univalent { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        [DisplayName("供应商")]
        public int SupplierId { get; set; }

        /// <summary>
        /// 存放地点
        /// </summary>
        [DisplayName("存放地点")]
        [MaxLength(32)]
        public string StorageLocation { get; set; } = "机房";

        /// <summary>
        /// 购买日期
        /// </summary>
        [DisplayName("购买日期")]
        public DateTime? PurchaseDate { get; set; }

        /// <summary>
        /// 描述信息
        /// </summary>
        [DisplayName("描述信息")]
        public string? Description { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string? Remark { get; set; }
    }
}