﻿namespace AMS.Services.Models;

public partial class Supplier
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName("名称")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 联系人
        /// </summary>
        [DisplayName("联系人")]
        public string Contactor { get; set; } = string.Empty;

        /// <summary>
        /// 联系方式
        /// </summary>
        [DisplayName("联系方式")]
        public string Phone { get; set; } = string.Empty;
    }
}