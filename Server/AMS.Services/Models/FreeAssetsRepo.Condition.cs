﻿namespace AMS.Services.Models;

public partial class FreeAssetsRepo
{
    [DisplayName("条件查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("关键字")]
        public string? Keywords { get; set; }

        [DisplayName("资产状态")]
        public AssetsStatus? StatusSelector { get; set; }
    }
}