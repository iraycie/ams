﻿namespace AMS.Services.Models;

/// <summary>
/// 易耗品资产
/// </summary>
[DisplayName("易耗品资产")]
public partial class FreeAssets : EntityBase

{
    /// <summary>
    /// 资产名称
    /// </summary>
    [DisplayName("资产名称")]
    [MaxLength(64)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 资产类别
    /// </summary>
    [DisplayName("资产类别")]
    public AssetsCategory Category { get; set; } = null!;

    /// <summary>
    /// 供应商
    /// </summary>
    [DisplayName("供应商")]
    public Supplier Supplier { get; set; } = null!;

    /// <summary>
    /// 批次
    /// </summary>
    [DisplayName("批次")]
    [MaxLength(32)]
    public string Batch { get; set; } = string.Empty;

    /// <summary>
    /// 单价
    /// </summary>
    [DisplayName("单价")]
    [Precision(9, 3)]
    public decimal Univalent { get; set; }

    /// <summary>
    /// 描述信息
    /// </summary>
    [DisplayName("描述信息")]
    public string? Description { get; set; }
}