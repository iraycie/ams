﻿namespace AMS.Services.Models;

public partial class ApproverConfigDetail
{
    public class UpdateModel
    {
        /// <summary>
        /// 审批人
        /// </summary>
        [DisplayName("审批人")]
        public int ApproverId { get; set; }

        /// <summary>
        /// 审批顺序
        /// </summary>
        [DisplayName("审批顺序")]
        public int Sort { get; set; }
    }
}