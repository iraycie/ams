﻿namespace AMS.Services.Models;

public partial class FixedAssetsConsuming
{
    [DisplayName("条件查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("关键词")]
        public string? Keywords { get; set; }

        [DisplayName("审批状态")]
        public ApprovalStatus? StatusSelector { get; set; }
    }
}