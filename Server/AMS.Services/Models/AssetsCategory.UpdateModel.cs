﻿namespace AMS.Services.Models;

public partial class AssetsCategory
{
	public class UpdateModel
	{
		/// <summary>
		/// 名称
		/// </summary>
		[DisplayName("名称")]
		[MaxLength(32)]
		public string Name { get; set; } = string.Empty;
	}
}