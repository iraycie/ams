﻿namespace AMS.Services.Models;

public partial class ApprovalDetail
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 审批序号
        /// </summary>
        [DisplayName("审批序号")]
        public int Sort { get; set; }

        /// <summary>
        /// 审批人
        /// </summary>
        [DisplayName("审批人")]
        public User.QueryModel Approver { get; set; } = null!;

        /// <summary>
        /// 审批备注
        /// </summary>
        [DisplayName("审批备注")]
        public string? Remark { get; set; }

        /// <summary>
        /// 审批状态
        /// </summary>
        [DisplayName("审批状态")]
        public ApprovalStatus Status { get; set; } = ApprovalStatus.Pending;
    }
}