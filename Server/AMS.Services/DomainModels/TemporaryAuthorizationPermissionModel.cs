﻿using OA.Infrastructure.DomainModels.TemporaryAuthorization;

namespace AMS.Services.DomainModels;

public class TemporaryAuthorizationPermissionModel : TemporaryAuthorizationBase
{
    public string[] ApiPermissions { get; set; } = Array.Empty<string>();

    public string RequireContent { get; set; } = string.Empty;
}