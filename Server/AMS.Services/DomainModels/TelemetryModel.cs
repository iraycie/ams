﻿namespace AMS.Services.DomainModels;

public class TelemetryModel
{
    public int ApprovalCount { get; set; }
    public int NewApprovalCount { get; set; }
    public int FixedAssetsCount { get; set; }
    public int NewFixedAssetsCount { get; set; }
    public int FreeAssetsCount { get; set; }
    public int NewFreeAssetsCount { get; set; }
    public int UserCount { get; set; }
    public int NewUserCount { get; set; }
}