﻿namespace AMS.Services.DomainModels;

internal static class RegisteredApprovalFlows
{
    public static (string Key, string BizName) FixedAssetsStorageApprovalFlowDefinition { get; } = ("C249E5DA60E146CC84886CE729D7983B", "固定资产入库");
    public static (string Key, string BizName) FixedAssetsConsumingApprovalFlowDefinition { get; } = ("3768CAD72F224B4CB5440C2789179B5F", "固定资产领用");
    public static (string Key, string BizName) FixedAssetsReturnApprovalFlowDefinition { get; } = ("201D9D949DB342D6A2CF876341E055F6", "固定资产归还");
    public static (string Key, string BizName) FreeAssetsStorageApprovalFlowDefinition { get; } = ("58B84E063907403D96184BC7986EAE62", "易耗品资产入库");
    public static (string Key, string BizName) FreeAssetsConsumingApprovalFlowDefinition { get; } = ("1F28A42BBBBD415A9316E6F44072C758", "易耗品资产领用");
    public static (string Key, string BizName) PurchaseApprovalFlowDefinition { get; } = ("0A7774CEB0F448E4A52427823594D8C2", "资产采购");
    public static (string Key, string BizName) LossApprovalFlowDefinition { get; } = ("B3306A96DDD84CF79BCB0803DA77958A", "固定资产报损");
}