﻿using AMS.Services.EventHandlers;
using OA.Infrastructure.DomainModels.Callback;
using OA.Infrastructure.WeCom.Abstractions;

namespace Microsoft.Extensions.DependencyInjection;

public static class WeComEventHandlerExtensions
{
    public static IServiceCollection RegisterWeComEventHandlers(this IServiceCollection services)
    {
        services.AddScoped<IWeComCallbackEventHandler<WeComTemplateCardEvent>, WeComTemplateCardEventHandler>();

        return services;
    }
}