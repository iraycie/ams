using AMS.Services.DomainModels;
using AMS.Services.Impl;
using OA.Infrastructure.Abstractions;
using OA.Infrastructure.Impl;

namespace Microsoft.Extensions.DependencyInjection;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddServices(this IServiceCollection services)
    {
        services.AddScoped<IApprovalService, ApprovalService>();
        services.AddScoped<ILossService, LossService>();
        services.AddScoped<IPurchaseService, PurchaseService>();
        services.AddScoped<ILogService, LogService>();
        services.AddScoped<IFixedAssetsConsumingService, FixedAssetsConsumingService>();
        services.AddScoped<IFixedAssetsStorageService, FixedAssetsStorageService>();
        services.AddScoped<IFixedAssetsRepoService, FixedAssetsRepoService>();
        services.AddScoped<IFreeAssetsConsumingService, FreeAssetsConsumingService>();
        services.AddScoped<IFreeAssetsStorageService, FreeAssetsStorageService>();        
        services.AddScoped<IFreeAssetsRepoService, FreeAssetsRepoService>();
        services.AddScoped<IAssetsCategoryService, AssetsCategoryService>();
        services.AddScoped<ISupplierService, SupplierService>();
        services.AddScoped<IMaintenanceRecordService, MaintenanceRecordService>();
        services.AddScoped<IApproverConfigService, ApproverConfigService>();

        services.AddScoped<ITemporaryAuthorizationService<TemporaryAuthorizationPermissionModel>, PermissionTemporaryAuthorizationService>();

        return services;
    }
}