﻿namespace AMS.Services.Enums;

/// <summary>
/// 采购状态
/// </summary>
public enum PurchaseStatus
{
    /// <summary>
    /// 审批遭拒
    /// </summary>
    Rejected = -1,

    /// <summary>
    /// 等待审批
    /// </summary>
    Pending = 0,

    /// <summary>
    /// 拨款中
    /// </summary>
    Appropriation = 1,

    /// <summary>
    /// 采购完成
    /// </summary>
    Completed = 2,
}