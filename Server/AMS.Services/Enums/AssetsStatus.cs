﻿namespace AMS.Services.Enums;

/// <summary>
/// 资产状态
/// </summary>
public enum AssetsStatus
{
    /// <summary>
    /// 审批遭拒
    /// </summary>
    Rejected = -1,

    /// <summary>
    /// 正常
    /// </summary>
    Normal = 0,

    /// <summary>
    /// 待审批
    /// </summary>
    Approving = 1,

    /// <summary>
    /// 已下架
    /// </summary>
    Withdrawn = 2,

    /// <summary>
    /// 报损中
    /// </summary>
    Lossing = 3,

    /// <summary>
    /// 库存缺失
    /// </summary>
    OutOfStock = 4,
}