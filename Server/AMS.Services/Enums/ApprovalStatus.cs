﻿namespace AMS.Services.Enums;

/// <summary>
/// 审批状态
/// </summary>
public enum ApprovalStatus
{
    /// <summary>
    /// 待审批
    /// </summary>
    Pending = 0,

    /// <summary>
    /// 通过
    /// </summary>
    Passed = 1,

    /// <summary>
    /// 驳回
    /// </summary>
    Rejected = 2,

    /// <summary>
    /// 等待上一位审批
    /// </summary>
    WaitingForLast = 3,
}