﻿namespace AMS.Services.Enums;

/// <summary>
/// 资产类型
/// </summary>
public enum AssetsType
{
    /// <summary>
    /// 例如笔、卷纸
    /// </summary>
    Consumable = 1,

    /// <summary>
    /// 例如书
    /// </summary>
    Durable = 2,
}