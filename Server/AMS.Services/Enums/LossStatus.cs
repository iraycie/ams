﻿namespace AMS.Services.Enums;

/// <summary>
/// 报损状态
/// </summary>
public enum LossStatus
{
    /// <summary>
    /// 审批遭拒
    /// </summary>
    Rejected = -1,

    /// <summary>
    /// 待审批
    /// </summary>
    Pending = 0,

    /// <summary>
    /// 维修中
    /// </summary>
    Fixing = 1,

    /// <summary>
    /// 维修成功
    /// </summary>
    Succeed = 2,

    /// <summary>
    /// 维修失败
    /// </summary>
    Failed = 3,
}