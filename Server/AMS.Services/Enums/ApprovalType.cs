﻿namespace AMS.Services.Enums;

/// <summary>
/// 审批类型
/// </summary>
public enum ApprovalType
{
    /// <summary>
    /// 资产入库
    /// </summary>
    Storage = 1,

    /// <summary>
    /// 资产领用
    /// </summary>
    Consuming = 2,

    /// <summary>
    /// 资产归还
    /// </summary>
    Return = 3,

    /// <summary>
    /// 采购申请
    /// </summary>
    Requisition = 4,

    /// <summary>
    /// 资产报损
    /// </summary>
    Loss = 5,
}