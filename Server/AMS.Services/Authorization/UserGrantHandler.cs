﻿using OA.Infrastructure.Abstractions;
using DynamicAuthorization.Sdk.Abstarctions;
using Microsoft.AspNetCore.Mvc;

namespace AMS.Services.Authorization;

public class UserGrantHandler : IUserGrantHandler
{
    private readonly IUserService _userService;

    public UserGrantHandler(IUserService userService)
    {
        _userService = userService;
    }

    public async Task<string?> GetUserGrantsAsync(string username, CancellationToken cancellationToken = default)
    {
        return !string.IsNullOrEmpty(username) ? await _userService.GetUserGrantsAsync(username, cancellationToken) : null;
    }

    public Task<bool> GrantAsync<TController>(string? permission = null) where TController : ControllerBase
    {
        throw new NotImplementedException();
    }

    public Task<bool> GrantRangeAsync<TController>(IEnumerable<string> permissions) where TController : ControllerBase
    {
        throw new NotImplementedException();
    }

    public Task<bool> RevokeAsync<TController>(string? permission = null) where TController : ControllerBase
    {
        throw new NotImplementedException();
    }

    public Task<bool> RevokeRangeAsync<TController>(IEnumerable<string> permissions) where TController : ControllerBase
    {
        throw new NotImplementedException();
    }
}