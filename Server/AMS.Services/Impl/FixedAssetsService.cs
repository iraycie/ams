﻿namespace AMS.Services.Impl;

internal class FixedAssetsService
{
    public static FixedAssets.QueryModel GetQueryModel(FixedAssets fixedAssets)
    {
        return new FixedAssets.QueryModel
        {
            Id = fixedAssets.Id,
            CreatedDate = fixedAssets.CreatedDate,
            LastModifiedDate = fixedAssets.LastModifiedDate,
            SerialNo = fixedAssets.SerialNo,
            Name = fixedAssets.Name,
            Category = AssetsCategoryService.GetQueryModel(fixedAssets.Category),
            Specs = fixedAssets.Specs,
            Univalent = fixedAssets.Univalent,
            Supplier = SupplierService.GetQueryModel(fixedAssets.Supplier),
            Description = fixedAssets.Description,
        };
    }
}