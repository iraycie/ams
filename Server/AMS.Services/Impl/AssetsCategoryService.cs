﻿namespace AMS.Services.Impl;

public class AssetsCategoryService : IAssetsCategoryService
{
    private readonly DbContext _context;

    public AssetsCategoryService(DbContext context)
    {
        _context = context;
    }

    public async Task<ListResult<AssetsCategory.QueryModel>> GetListAsync(CancellationToken cancellationToken)
    {
        var result = new ListResult<AssetsCategory.QueryModel>();

        try
        {
            var assetsCategorys = await _context.Set<AssetsCategory>()
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            result.Data = assetsCategorys.Select(x => GetQueryModel(x));

            result.Status = OperationalResult.SUCCESS;
        }
        catch (Exception error)
        {
            result.Status = OperationalResult.ERROR;

            result.Message = error.ToString();
        }

        return result;
    }

    public async Task<PageResult<AssetsCategory.QueryModel>> GetPageAsync(PageParameter input, CancellationToken cancellationToken)
    {
        var result = new PageResult<AssetsCategory.QueryModel>();

        try
        {
            var assetsCategorys = await _context.Set<AssetsCategory>()
                .Skip((input.PageNum - 1) * input.PageSize)
                .Take(input.PageSize)
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            result.Data = assetsCategorys.Select(x => GetQueryModel(x));

            result.PageNum = input.PageNum;

            result.PageSize = input.PageSize;

            result.Total = _context.Set<AssetsCategory>().Count();

            result.Status = OperationalResult.SUCCESS;
        }
        catch (Exception error)
        {
            result.Status = OperationalResult.ERROR;

            result.Message = error.ToString();
        }

        return result;
    }

    public async Task<OperationalResult> CreateAsync(AssetsCategory.CreateModel assetsCategory, CancellationToken cancellationToken)
    {
        var result = new OperationalResult();

        try
        {
            var entity = new AssetsCategory
            {
                Name = assetsCategory.Name,
            };

            await _context.Set<AssetsCategory>().AddAsync(entity, cancellationToken);

            var affectedRows = await _context.SaveChangesAsync(cancellationToken);

            result.Status = affectedRows > 0 ? OperationalResult.SUCCESS : OperationalResult.FAILURE;

            result.Message = $"{affectedRows} rows affected.";
        }
        catch (Exception error)
        {
            result.Status = OperationalResult.ERROR;

            result.Message = error.ToString();
        }

        return result;
    }

    public async Task<OperationalResult> UpdateAsync(int id, AssetsCategory.UpdateModel assetsCategory, CancellationToken cancellationToken)
    {
        var result = new OperationalResult();

        try
        {
            var entity = await _context.Set<AssetsCategory>()
                .Where(m => m.Id.Equals(id))
                .FirstOrDefaultAsync(cancellationToken);

            if (entity is null)
            {
                result.Status = OperationalResult.FAILURE;

                result.Message = "0 rows affected.";

                return result;
            }

            entity.Name = assetsCategory.Name;

            _context.Set<AssetsCategory>().Update(entity);

            var affectedRows = await _context.SaveChangesAsync(cancellationToken);

            result.Status = affectedRows > 0 ? OperationalResult.SUCCESS : OperationalResult.FAILURE;

            result.Message = $"{affectedRows} rows affected.";
        }
        catch (Exception error)
        {
            var exists = await _context.Set<AssetsCategory>().AnyAsync(m => m.Id.Equals(id), cancellationToken);

            if (!exists)
            {
                result.Status = OperationalResult.FAILURE;

                result.Message = "0 rows affected.";
            }
            else
            {
                result.Status = OperationalResult.ERROR;

                result.Message = error.ToString();
            }
        }

        return result;
    }

    public async Task<OperationalResult> DeleteAsync(int id, CancellationToken cancellationToken)
    {
        var result = new OperationalResult();

        try
        {
            var assetsCategory = await _context.Set<AssetsCategory>()
                .Where(m => m.Id.Equals(id))
                .FirstOrDefaultAsync(cancellationToken);

            if (assetsCategory is null)
            {
                result.Status = OperationalResult.FAILURE;

                result.Message = "0 rows affected.";

                return result;
            }

            _context.Set<AssetsCategory>().Remove(assetsCategory);

            var affectedRows = await _context.SaveChangesAsync(cancellationToken);

            result.Status = affectedRows > 0 ? OperationalResult.SUCCESS : OperationalResult.FAILURE;

            result.Message = $"{affectedRows} rows affected.";
        }
        catch (Exception error)
        {
            result.Status = OperationalResult.ERROR;

            result.Message = error.ToString();
        }

        return result;
    }

    public static AssetsCategory.QueryModel GetQueryModel(AssetsCategory assetsCategory)
    {
        return new AssetsCategory.QueryModel
        {
            Id = assetsCategory.Id,
            CreatedDate = assetsCategory.CreatedDate,
            LastModifiedDate = assetsCategory.LastModifiedDate,
            Name = assetsCategory.Name,
        };
    }
}