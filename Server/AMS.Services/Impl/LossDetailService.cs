﻿namespace AMS.Services.Impl;

internal class LossDetailService
{
    public static FixedAssetsLossDetail.QueryModel GetQueryModel(FixedAssetsLossDetail lossDetail)
    {
        return new FixedAssetsLossDetail.QueryModel
        {
            Id = lossDetail.Id,
            CreatedDate = lossDetail.CreatedDate,
            LastModifiedDate = lossDetail.LastModifiedDate,
            Assets = FixedAssetsService.GetQueryModel(lossDetail.Assets),
        };
    }
}