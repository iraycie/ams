﻿using OA.Infrastructure.Extensions;

namespace AMS.Services.Impl;

internal class ApproverConfigDetailService : IApproverConfigDetailService
{
    public static ApproverConfigDetail.QueryModel GetQueryModel(ApproverConfigDetail approverConfigDetail)
    {
        return new ApproverConfigDetail.QueryModel
        {
            Id = approverConfigDetail.Id,
            CreatedDate = approverConfigDetail.CreatedDate,
            LastModifiedDate = approverConfigDetail.LastModifiedDate,
            Approver = DefinitionServiceExtensions.GetUserQueryModel(approverConfigDetail.Approver),
            Sort = approverConfigDetail.Sort
        };
    }
}