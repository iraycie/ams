﻿namespace AMS.Services.Impl;

internal class FreeAssetsService
{
    public static FreeAssets.QueryModel GetQueryModel(FreeAssets freeAssets)
    {
        return new FreeAssets.QueryModel
        {
            Id = freeAssets.Id,
            CreatedDate = freeAssets.CreatedDate,
            LastModifiedDate = freeAssets.LastModifiedDate,
            Name = freeAssets.Name,
            Category = AssetsCategoryService.GetQueryModel(freeAssets.Category),
            Supplier = SupplierService.GetQueryModel(freeAssets.Supplier),
            Batch = freeAssets.Batch,
            Univalent = freeAssets.Univalent,
            Description = freeAssets.Description,
        };
    }
}