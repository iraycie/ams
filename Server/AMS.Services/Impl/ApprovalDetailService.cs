﻿using OA.Infrastructure.Extensions;

namespace AMS.Services.Impl;

internal class ApprovalDetailService
{
    public static ApprovalDetail.QueryModel GetQueryModel(ApprovalDetail approvalDetail)
    {
        return new ApprovalDetail.QueryModel
        {
            Id = approvalDetail.Id,
            CreatedDate = approvalDetail.CreatedDate,
            LastModifiedDate = approvalDetail.LastModifiedDate,
            Sort = approvalDetail.Sort,
            Approver = DefinitionServiceExtensions.GetUserQueryModel(approvalDetail.Approver),
            Remark = approvalDetail.Remark,
            Status = approvalDetail.Status,
        };
    }
}