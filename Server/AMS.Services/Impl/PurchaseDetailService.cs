﻿namespace AMS.Services.Impl;

internal class PurchaseDetailService
{
    public static PurchaseDetail.QueryModel GetQueryModel(PurchaseDetail purchaseDetail)
    {
        return new PurchaseDetail.QueryModel
        {
            Id = purchaseDetail.Id,
            CreatedDate = purchaseDetail.CreatedDate,
            LastModifiedDate = purchaseDetail.LastModifiedDate,
            Name = purchaseDetail.Name,
            Count = purchaseDetail.Count,
            Univalent = purchaseDetail.Univalent,
            Supplier = SupplierService.GetQueryModel(purchaseDetail.Supplier),
        };
    }
}