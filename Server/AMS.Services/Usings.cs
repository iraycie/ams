﻿global using AMS.Common;
global using AMS.Common.Linq;
global using OA.Infrastructure.Models;
global using AMS.Services.Abstractions;
global using AMS.Services.Enums;
global using AMS.Services.Models;

global using Microsoft.EntityFrameworkCore;
global using Microsoft.Extensions.DependencyInjection;
global using System.ComponentModel;
global using System.ComponentModel.DataAnnotations;
global using System.Linq.Expressions;