﻿namespace AMS.Services.Abstractions;

public interface IFreeAssetsConsumingService
{
    Task<PageResult<FreeAssetsConsuming.QueryModel>> GetPageAsync(FreeAssetsConsuming.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(FreeAssetsConsuming.CreateModel freeAssetsConsuming, CancellationToken cancellationToken);
}