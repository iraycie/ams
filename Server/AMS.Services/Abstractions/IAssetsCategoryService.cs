﻿namespace AMS.Services.Abstractions;

public interface IAssetsCategoryService
{
    Task<ListResult<AssetsCategory.QueryModel>> GetListAsync(CancellationToken cancellationToken);

    Task<PageResult<AssetsCategory.QueryModel>> GetPageAsync(PageParameter input, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(AssetsCategory.CreateModel assetsCategory, CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(int id, AssetsCategory.UpdateModel assetsCategory, CancellationToken cancellationToken);

    Task<OperationalResult> DeleteAsync(int id, CancellationToken cancellationToken);
}