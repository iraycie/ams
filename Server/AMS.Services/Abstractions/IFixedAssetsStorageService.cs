﻿namespace AMS.Services.Abstractions;

public interface IFixedAssetsStorageService
{
    Task<PageResult<FixedAssetsStorage.QueryModel>> GetPageAsync(FixedAssetsStorage.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(FixedAssetsStorage.CreateModel fixedAssetsStorage, CancellationToken cancellationToken);
}