﻿namespace AMS.Services.Abstractions;

public interface IFixedAssetsConsumingService
{
    Task<PageResult<FixedAssetsConsuming.QueryModel>> GetPageAsync(FixedAssetsConsuming.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(FixedAssetsConsuming.CreateModel fixedAssetsConsuming, CancellationToken cancellationToken);

    Task<OperationalResult> ReturnAsync(int id, CancellationToken cancellationToken);
}