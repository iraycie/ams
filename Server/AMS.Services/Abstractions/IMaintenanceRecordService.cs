﻿namespace AMS.Services.Abstractions;

public interface IMaintenanceRecordService
{
    Task<ListResult<MaintenanceRecord.QueryModel>> GetListAsync(CancellationToken cancellationToken);

    Task<PageResult<MaintenanceRecord.QueryModel>> GetPageAsync(PageParameter input, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(MaintenanceRecord.CreateModel maintenanceRecord, CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(int id, MaintenanceRecord.UpdateModel maintenanceRecord, CancellationToken cancellationToken);
}