﻿namespace AMS.Services.Abstractions;

public interface IPurchaseService
{
    Task<PageResult<Purchase.QueryModel>> GetPageAsync(Purchase.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(Purchase.CreateModel purchase, CancellationToken cancellationToken);
}