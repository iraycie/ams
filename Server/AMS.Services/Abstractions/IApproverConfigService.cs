﻿namespace AMS.Services.Abstractions;

public interface IApproverConfigService
{
    Task<ListResult<ApproverConfig.QueryModel>> GetListAsync(CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(IEnumerable<ApproverConfig.UpdateModel> approverConfigs, CancellationToken cancellationToken);

    Task<ListResult<object>> GetApproverListAsync(CancellationToken cancellationToken);
}