﻿namespace AMS.Services.Abstractions;

public interface IFixedAssetsRepoService
{
    Task<PageResult<FixedAssetsRepo.QueryModel>> GetPageAsync(FixedAssetsRepo.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(int id, FixedAssetsRepo.UpdateModel fixedAssetsRepo, CancellationToken cancellationToken);

    Task<ListResult<FixedAssetsRepo.QueryModel>> GetApprovedAsync(CancellationToken cancellationToken);

    Task<ListResult<FixedAssetsRepo.QueryModel>> GetOwnedAsync(CancellationToken cancellationToken);
}