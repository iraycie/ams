﻿namespace AMS.Services.Abstractions;

public interface IFreeAssetsRepoService
{
    Task<PageResult<FreeAssetsRepo.QueryModel>> GetPageAsync(FreeAssetsRepo.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<ListResult<FreeAssetsRepo.QueryModel>> GetApprovedAsync(CancellationToken cancellationToken);
}