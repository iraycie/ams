﻿namespace AMS.Services.Abstractions;

public interface IFreeAssetsStorageService
{
    Task<PageResult<FreeAssetsStorage.QueryModel>> GetPageAsync(FreeAssetsStorage.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(FreeAssetsStorage.CreateModel freeAssetsStorage, CancellationToken cancellationToken);
}