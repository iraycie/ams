﻿using AMS.Services.Models;

namespace AMS.Services.Abstractions;

public interface IApprovalService
{
    Task<PageResult<Approval.QueryModel>> GetPageAsync(Approval.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> CreateApprovalAsync(User committer,
        int formId,
        ApprovalType approvalType,
        AssetsType assetsType,
        string? remark,
        Func<DbContext, ICollection<ApprovalDetail>> factory,
        CancellationToken cancellationToken);

    Task<OperationalResult> PassAsync(int id, CancellationToken cancellationToken);

    Task<OperationalResult> RejectAsync(int id, string remark, CancellationToken cancellationToken);
}