﻿namespace AMS.Services.Abstractions;

public interface ILossService
{
    Task<PageResult<FixedAssetsLoss.QueryModel>> GetPageAsync(FixedAssetsLoss.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(FixedAssetsLoss.CreateModel loss, CancellationToken cancellationToken);
}