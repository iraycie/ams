﻿namespace AMS.Services.Abstractions;

public interface ISupplierService
{
    Task<ListResult<Supplier.QueryModel>> GetListAsync(CancellationToken cancellationToken);

    Task<PageResult<Supplier.QueryModel>> GetPageAsync(PageParameter input, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(Supplier.CreateModel supplier, CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(int id, Supplier.UpdateModel supplier, CancellationToken cancellationToken);

    Task<OperationalResult> DeleteAsync(int id, CancellationToken cancellationToken);
}