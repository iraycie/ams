﻿using AMS.EFCore.Configurations;
using OA.Infrastructure;
using OA.Infrastructure.Logger.Abstractions;

namespace AMS.EFCore.Data;

public class DataContext : AnalysisDbContext<DataContext>
{
    public DataContext(DbContextOptions<DataContext> options, ILoggerOptions loggerOptions)
        : base(options, loggerOptions)
    {

    }

    public DbSet<Approval> Approval { get; set; } = null!;
    public DbSet<ApprovalDetail> ApprovalDetail { get; set; } = null!;
    public DbSet<ApproverConfig> ApproverConfig { get; set; } = null!;
    public DbSet<ApproverConfigDetail> ApproverConfigDetail { get; set; } = null!;
    public DbSet<FixedAssetsLoss> FixedAssetsLoss { get; set; } = null!;
    public DbSet<FixedAssetsLossDetail> FixedAssetsLossDetail { get; set; } = null!;
    public DbSet<Purchase> Purchase { get; set; } = null!;
    public DbSet<PurchaseDetail> PurchaseDetail { get; set; } = null!;
    public DbSet<FixedAssets> FixedAssets { get; set; } = null!;
    public DbSet<FixedAssetsConsuming> FixedAssetsConsuming { get; set; } = null!;
    public DbSet<FixedAssetsStorage> FixedAssetsStorage { get; set; } = null!;
    public DbSet<FreeAssets> FreeAssets { get; set; } = null!;
    public DbSet<FreeAssetsConsuming> FreeAssetsConsuming { get; set; } = null!;
    public DbSet<FreeAssetsStorage> FreeAssetsStorage { get; set; } = null!;
    public DbSet<MaintenanceRecord> MaintenanceRecord { get; set; } = null!;
    public DbSet<AssetsCategory> AssetsCategory { get; set; } = null!;
    public DbSet<Supplier> Supplier { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new ApprovalConfiguration());
        modelBuilder.ApplyConfiguration(new ApprovalDetailConfiguration());

        modelBuilder.ApplyConfiguration(new ApproverConfigConfiguration());
        modelBuilder.ApplyConfiguration(new ApproverConfigDetailConfiguration());

        modelBuilder.ApplyConfiguration(new LossConfiguration());
        modelBuilder.ApplyConfiguration(new LossDetailConfiguration());

        modelBuilder.ApplyConfiguration(new PurchaseConfiguration());
        modelBuilder.ApplyConfiguration(new PurchaseDetailConfiguration());

        modelBuilder.ApplyConfiguration(new FixedAssetsRepoConfiguration());
        modelBuilder.ApplyConfiguration(new FreeAssetsRepoConfiguration());

        modelBuilder.ApplyConfiguration(new FixedAssetsConsumingConfiguration());
        modelBuilder.ApplyConfiguration(new FreeAssetsConsumingConfiguration());

        modelBuilder.ApplyConfiguration(new FixedAssetsStorageConfiguration());
        modelBuilder.ApplyConfiguration(new FreeAssetsStorageConfiguration());
        
        modelBuilder.ApplyConfiguration(new FixedAssetsConfiguration());
        modelBuilder.ApplyConfiguration(new FreeAssetsConfiguration());

        base.OnModelCreating(modelBuilder);
    }
}