using AMS.EFCore.Data;
using OA.Infrastructure.Logger;
using OA.Infrastructure.Logger.Abstractions;

namespace Microsoft.Extensions.DependencyInjection;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDataContext(
        this IServiceCollection services, IConfiguration configuration)
    {
        _ = services.AddDbContext<DbContext, DataContext>(options =>
        {
            _ = options.EnableSensitiveDataLogging().EnableDetailedErrors().UseNpgsql(configuration.GetConnectionString("DataContext"));
        });

        _ = services.AddSingleton<ILoggerOptions, LoggerOptions>();
        return services;
    }
}