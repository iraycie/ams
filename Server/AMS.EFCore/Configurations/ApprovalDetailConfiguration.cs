﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class ApprovalDetailConfiguration : IEntityTypeConfiguration<ApprovalDetail>
{
    public void Configure(EntityTypeBuilder<ApprovalDetail> builder)
    {
        builder
            .HasOne(p => p.Approver)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Property(p => p.Status).HasConversion<string>();
    }
}