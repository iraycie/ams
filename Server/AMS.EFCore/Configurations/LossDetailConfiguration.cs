﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class LossDetailConfiguration : IEntityTypeConfiguration<FixedAssetsLossDetail>
{
    public void Configure(EntityTypeBuilder<FixedAssetsLossDetail> builder)
    {
        builder
            .HasOne(p => p.Assets)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);
    }
}