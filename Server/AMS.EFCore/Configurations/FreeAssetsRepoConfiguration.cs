﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class FreeAssetsRepoConfiguration : IEntityTypeConfiguration<FreeAssetsRepo>
{
    public void Configure(EntityTypeBuilder<FreeAssetsRepo> builder)
    {
        builder
            .HasOne(p => p.Assets)
            .WithMany()
            .OnDelete(DeleteBehavior.Cascade);


        builder.Property(p => p.Status).HasConversion<string>();
    }
}