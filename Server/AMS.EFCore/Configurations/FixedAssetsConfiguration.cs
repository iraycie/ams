﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class FixedAssetsConfiguration : IEntityTypeConfiguration<FixedAssets>
{
    public void Configure(EntityTypeBuilder<FixedAssets> builder)
    {
        builder
            .HasOne(p => p.Category)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasOne(p => p.Supplier)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Property(p => p.Univalent).HasPrecision(9, 3);
    }
}