﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class PurchaseConfiguration : IEntityTypeConfiguration<Purchase>
{
    public void Configure(EntityTypeBuilder<Purchase> builder)
    {
        builder
            .HasOne(p => p.Commiter)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasMany(p => p.PurchaseDetails)
            .WithOne()
            .OnDelete(DeleteBehavior.Cascade);

        builder.Property(p => p.Status).HasConversion<string>();
    }
}