﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class ApprovalConfiguration : IEntityTypeConfiguration<Approval>
{
    public void Configure(EntityTypeBuilder<Approval> builder)
    {
        builder
            .HasMany(p => p.ApprovalDetails)
            .WithOne()
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasOne(p => p.Committer)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Property(p => p.Type).HasConversion<string>();
        builder.Property(p => p.AssetsType).HasConversion<string>();
        builder.Property(p => p.Status).HasConversion<string>();
    }
}