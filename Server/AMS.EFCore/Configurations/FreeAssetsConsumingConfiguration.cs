﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class FreeAssetsConsumingConfiguration : IEntityTypeConfiguration<FreeAssetsConsuming>
{
    public void Configure(EntityTypeBuilder<FreeAssetsConsuming> builder)
    {
        builder
            .HasOne(p => p.Assets)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasOne(p => p.Consumer)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Property(p => p.Status).HasConversion<string>();
    }
}