﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class ApproverConfigConfiguration : IEntityTypeConfiguration<ApproverConfig>
{
    public void Configure(EntityTypeBuilder<ApproverConfig> builder)
    {
        builder
            .HasMany(p => p.ApproverDetails)
            .WithOne()            
            .OnDelete(DeleteBehavior.NoAction);
    }
}