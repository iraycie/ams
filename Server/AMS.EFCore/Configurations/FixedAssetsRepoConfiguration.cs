﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class FixedAssetsRepoConfiguration : IEntityTypeConfiguration<FixedAssetsRepo>
{
    public void Configure(EntityTypeBuilder<FixedAssetsRepo> builder)
    {
        builder
            .HasOne(p => p.Assets)
            .WithMany()
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasOne(p => p.Owner)
            .WithMany()
            .OnDelete(DeleteBehavior.SetNull);

        builder.Property(p => p.Status).HasConversion<string>();
    }
}