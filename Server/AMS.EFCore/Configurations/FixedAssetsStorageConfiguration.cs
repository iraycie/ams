﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class FixedAssetsStorageConfiguration : IEntityTypeConfiguration<FixedAssetsStorage>
{
    public void Configure(EntityTypeBuilder<FixedAssetsStorage> builder)
    {
        builder
            .HasOne(p => p.Commiter)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasOne(p => p.Category)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasOne(p => p.Supplier)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Property(p => p.Univalent).HasPrecision(9, 3);
        builder.Property(p => p.Status).HasConversion<string>();
    }
}