﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class LossConfiguration : IEntityTypeConfiguration<FixedAssetsLoss>
{
    public void Configure(EntityTypeBuilder<FixedAssetsLoss> builder)
    {
        builder
            .HasOne(p => p.Reporter)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasMany(p => p.LossDetails)
            .WithOne()
            .OnDelete(DeleteBehavior.Cascade);

        builder.Property(p => p.Status).HasConversion<string>();
    }
}