﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class PurchaseDetailConfiguration : IEntityTypeConfiguration<PurchaseDetail>
{
    public void Configure(EntityTypeBuilder<PurchaseDetail> builder)
    {
        builder
            .HasOne(p => p.Supplier)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Property(p => p.Univalent).HasPrecision(9, 3);
    }
}