﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class ApproverConfigDetailConfiguration : IEntityTypeConfiguration<ApproverConfigDetail>
{
    public void Configure(EntityTypeBuilder<ApproverConfigDetail> builder)
    {
        builder
            .HasOne(p => p.Approver)
            .WithMany()
            .OnDelete(DeleteBehavior.Cascade);
    }
}