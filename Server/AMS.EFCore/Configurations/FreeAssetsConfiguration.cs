﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.EFCore.Configurations;

public class FreeAssetsConfiguration : IEntityTypeConfiguration<FreeAssets>
{
    public void Configure(EntityTypeBuilder<FreeAssets> builder)
    {
        builder
            .HasOne(p => p.Category)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasOne(p => p.Supplier)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Property(p => p.Univalent).HasPrecision(9, 3);
    }
}