﻿using System.Text.Json;

namespace AMS.WebApi.Converters;

public class DateTimeJsonConverter : JsonConverter<DateTime>
{
    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var value = reader.GetString();

        return DateTime.Parse(value!);
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(string.Format("{0:yyyy-MM-ddThh:mm:ss}", value));
    }
}