using AMS.Services.Authorization;
using AMS.Services.Job;
using AMS.WebApi.Converters;
using DynamicAuthorization.Sdk.Extensions;
using OA.Infrastructure.Authentication;
using Quartz;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.ComponentModel;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddServices();
builder.Services.AddDataContext(builder.Configuration);

builder.Services.Configure<JsonOptions>(options =>
{
    options.JsonSerializerOptions.Converters.Add(new DateTimeJsonConverter());
    options.JsonSerializerOptions.Converters.Add(new TimeOnlyJsonConverter());
    options.JsonSerializerOptions.Converters.Add(new DateOnlyJsonConverter());
});

TypeDescriptor.AddAttributes(typeof(DateTime), new TypeConverterAttribute(typeof(DateTimeTypeConverter)));
TypeDescriptor.AddAttributes(typeof(TimeOnly), new TypeConverterAttribute(typeof(TimeOnlyTypeConverter)));
TypeDescriptor.AddAttributes(typeof(DateOnly), new TypeConverterAttribute(typeof(DateOnlyTypeConverter)));

builder.Services.AddControllers()
    .AddJsonOptions(options => options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles); ;

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.CustomSchemaIds(type => type.ToString().Replace('+', '.'));
    options.MapType<DateOnly>(() => new Microsoft.OpenApi.Models.OpenApiSchema { Type = "string", Format = "date" });
    options.MapType<TimeOnly>(() => new Microsoft.OpenApi.Models.OpenApiSchema
    {
        Type = "string",
        Format = "date",
        Example = OpenApiAnyFactory.CreateFromJson($"\"{TimeOnly.FromDateTime(DateTime.UtcNow)}\"")
    });
});

builder.Services.AddAuthorization(opts =>
{
    opts.AddPolicy("ams", policyBuilder =>
    {
        policyBuilder.RequireAuthenticatedUser();
    });
});

builder.Services.AddInfrastructure().AddQuartzCore(cfg =>
{
    cfg.ScheduleJob<InitializeInfrastructure>(trigger => trigger
               .WithIdentity(nameof(InitializeInfrastructure))
               .StartNow()
               .WithDescription("初始化基础设施"));
});
//builder.Services.AddCloudIdentityServer<UserValidator>(builder.Configuration);
builder.Services.AddJwt(builder.Configuration);
builder.Services.AddDynamicAuthorization<UserGrantHandler>();
builder.Services.AddIntegrateCors();
builder.Services.RegisterWeComEventHandlers();

var app = builder.Build();

app.Services.Initialize(typeof(Program).Assembly);

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseOfficeAutomation("ams");

app.MapControllers();

app.Run();