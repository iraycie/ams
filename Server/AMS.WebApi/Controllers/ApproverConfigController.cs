﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("审批流程管理", Route = "/settings/approver")]
public class ApproverConfigController : ControllerBase
{
    private readonly IApproverConfigService _service;

    public ApproverConfigController(IApproverConfigService service)
    {
        _service = service;
    }

    // GET api/approver-config
    [HttpGet("api/approver-config")]
    public Task<ListResult<ApproverConfig.QueryModel>> GetListAsync()
    {
        return _service.GetListAsync(HttpContext.RequestAborted);
    }

    // GET api/approver-config
    [HttpGet("api/approver-config/approver")]
    public Task<ListResult<object>> GetApproverListAsync()
    {
        return _service.GetApproverListAsync(HttpContext.RequestAborted);
    }

    // PUT api/approver-config/5
    [HttpPut("api/approver-config")]
    [DynamicAuthorize]
    [PermisstionDefinition("更新审批流程")]
    public Task<OperationalResult> UpdateAsync(IEnumerable<ApproverConfig.UpdateModel> approverConfigs)
    {
        return _service.UpdateAsync(approverConfigs, HttpContext.RequestAborted);
    }
}