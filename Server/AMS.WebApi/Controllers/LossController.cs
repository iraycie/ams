﻿using DynamicAuthorization.Sdk.Attributes;

namespace AMS.WebApi.Controllers;

[ApiController]
[PermisstionDefinition("资产报损管理", Route = "/fixed-assets/loss")]
public class LossController : ControllerBase
{
    private readonly ILossService _service;

    public LossController(ILossService service)
    {
        _service = service;
    }

    // GET api/loss/condition-selector
    [HttpGet("api/loss/condition-selector")]
    public Task<PageResult<FixedAssetsLoss.QueryModel>> GetPageAsync([FromQuery] FixedAssetsLoss.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // POST api/loss
    [HttpPost("api/loss")]
    public Task<OperationalResult> CreateAsync(FixedAssetsLoss.CreateModel loss)
    {
        return _service.CreateAsync(loss, HttpContext.RequestAborted);
    }
}