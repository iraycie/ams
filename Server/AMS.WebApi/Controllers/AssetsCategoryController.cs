﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("资产类别管理", Route = "/settings/assets-category")]
public class AssetsCategoryController : ControllerBase
{
    private readonly IAssetsCategoryService _service;

    public AssetsCategoryController(IAssetsCategoryService service)
    {
        _service = service;
    }

    // GET api/assets-category
    [HttpGet("api/assets-category")]
    public Task<ListResult<AssetsCategory.QueryModel>> GetListAsync()
    {
        return _service.GetListAsync(HttpContext.RequestAborted);
    }

    // GET api/assets-category/page
    [HttpGet("api/assets-category/page")]
    public Task<PageResult<AssetsCategory.QueryModel>> GetPageAsync([FromQuery] PageParameter input)
    {
        return _service.GetPageAsync(input, HttpContext.RequestAborted);
    }

    // POST api/assets-category
    [HttpPost("api/assets-category")]
    [DynamicAuthorize]
    [PermisstionDefinition("添加资产类别")]
    public Task<OperationalResult> CreateAsync(AssetsCategory.CreateModel assetsCategory)
    {
        return _service.CreateAsync(assetsCategory, HttpContext.RequestAborted);
    }

    // PUT api/assets-category/5
    [HttpPut("api/assets-category/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("更新资产类别")]
    public Task<OperationalResult> UpdateAsync(int id, AssetsCategory.UpdateModel assetsCategory)
    {
        return _service.UpdateAsync(id, assetsCategory, HttpContext.RequestAborted);
    }

    // DELETE api/assets-category/5
    [HttpDelete("api/assets-category/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("删除资产类别")]
    public Task<OperationalResult> DeleteAsync(int id)
    {
        return _service.DeleteAsync(id, HttpContext.RequestAborted);
    }
}