﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("维修记录管理", Route = "/maintenance-record")]
public class MaintenanceRecordController : ControllerBase
{
    private readonly IMaintenanceRecordService _service;

    public MaintenanceRecordController(IMaintenanceRecordService service)
    {
        _service = service;
    }

    // GET api/maintenance-record
    [HttpGet("api/maintenance-record")]
    public Task<ListResult<MaintenanceRecord.QueryModel>> GetListAsync()
    {
        return _service.GetListAsync(HttpContext.RequestAborted);
    }

    // GET api/maintenance-record/page
    [HttpGet("api/maintenance-record/page")]
    public Task<PageResult<MaintenanceRecord.QueryModel>> GetPageAsync([FromQuery] PageParameter input)
    {
        return _service.GetPageAsync(input, HttpContext.RequestAborted);
    }

    // POST api/maintenance-record
    [HttpPost("api/maintenance-record")]
    [DynamicAuthorize]
    [PermisstionDefinition("添加维修记录")]
    public Task<OperationalResult> CreateAsync(MaintenanceRecord.CreateModel maintenanceRecord)
    {
        return _service.CreateAsync(maintenanceRecord, HttpContext.RequestAborted);
    }

    // PUT api/maintenance-record/5
    [HttpPut("api/maintenance-record/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("更新维修记录")]
    public Task<OperationalResult> UpdateAsync(int id, MaintenanceRecord.UpdateModel maintenanceRecord)
    {
        return _service.UpdateAsync(id, maintenanceRecord, HttpContext.RequestAborted);
    }
}