﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("审批管理", Route = "/approval")]
public class ApprovalController : ControllerBase
{
    private readonly IApprovalService _service;

    public ApprovalController(IApprovalService service)
    {
        _service = service;
    }

    // GET api/approval/condition-selector
    [HttpGet("api/approval/condition-selector")]
    public Task<PageResult<Approval.QueryModel>> GetPageAsync([FromQuery] Approval.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // POST api/approval/pass
    [HttpPost("api/approval/pass")]
    [DynamicAuthorize]
    [PermisstionDefinition("审批通过")]
    public Task<OperationalResult> PassAsync([FromQuery] int id)
    {
        return _service.PassAsync(id, HttpContext.RequestAborted);
    }

    // POST api/approval/reject
    [HttpPost("api/approval/reject")]
    [DynamicAuthorize]
    [PermisstionDefinition("审批拒绝")]
    public Task<OperationalResult> RejectAsync([FromQuery] int id, string remark)
    {
        return _service.RejectAsync(id, remark, HttpContext.RequestAborted);
    }
}
