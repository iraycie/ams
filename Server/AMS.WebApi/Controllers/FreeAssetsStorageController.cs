﻿using DynamicAuthorization.Sdk.Attributes;

namespace AMS.WebApi.Controllers;

[ApiController]
[PermisstionDefinition("易耗品资产入库管理", Route = "/free-assets/storage")]
public class FreeAssetsStorageController : ControllerBase
{
    private readonly IFreeAssetsStorageService _service;

    public FreeAssetsStorageController(IFreeAssetsStorageService service)
    {
        _service = service;
    }

    // GET api/free-assets-storage/condition-selector
    [HttpGet("api/free-assets-storage/condition-selector")]
    public Task<PageResult<FreeAssetsStorage.QueryModel>> GetPageAsync([FromQuery] FreeAssetsStorage.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // POST api/free-assets-storage
    [HttpPost("api/free-assets-storage")]
    [DynamicAuthorize]
    [PermisstionDefinition("易耗品资产入库")]
    public Task<OperationalResult> CreateAsync(FreeAssetsStorage.CreateModel freeAssetsStorage)
    {
        return _service.CreateAsync(freeAssetsStorage, HttpContext.RequestAborted);
    }
}