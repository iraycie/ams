﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("易耗品资产领用管理", Route = "/free-assets/consuming")]
public class FreeAssetsConsumingController : ControllerBase
{
    private readonly IFreeAssetsConsumingService _service;

    public FreeAssetsConsumingController(IFreeAssetsConsumingService service)
    {
        _service = service;
    }

    // GET api/free-assets-consuming/condition-selector
    [HttpGet("api/free-assets-consuming/condition-selector")]
    public Task<PageResult<FreeAssetsConsuming.QueryModel>> GetPageAsync([FromQuery] FreeAssetsConsuming.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // POST api/free-assets-consuming
    [HttpPost("api/free-assets-consuming")]
    [DynamicAuthorize]
    [PermisstionDefinition("易耗品资产领用")]
    public Task<OperationalResult> CreateAsync(FreeAssetsConsuming.CreateModel freeAssetsConsuming)
    {
        return _service.CreateAsync(freeAssetsConsuming, HttpContext.RequestAborted);
    }
}