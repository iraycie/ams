﻿using DynamicAuthorization.Sdk.Attributes;

namespace AMS.WebApi.Controllers;

[ApiController]
[PermisstionDefinition("资产采购管理", Route = "/purchase")]
public class PurchaseController : ControllerBase
{
    private readonly IPurchaseService _service;

    public PurchaseController(IPurchaseService service)
    {
        _service = service;
    }

    // GET api/purchase/condition-selector
    [HttpGet("api/purchase/condition-selector")]
    public Task<PageResult<Purchase.QueryModel>> GetPageAsync([FromQuery] Purchase.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // POST api/purchase
    [HttpPost("api/purchase")]
    [DynamicAuthorize]
    [PermisstionDefinition("固定资产采购")]
    public Task<OperationalResult> CreateAsync(Purchase.CreateModel purchase)
    {
        return _service.CreateAsync(purchase, HttpContext.RequestAborted);
    }
}