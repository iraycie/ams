﻿using DynamicAuthorization.Sdk.Attributes;

namespace AMS.WebApi.Controllers;

[ApiController]
[PermisstionDefinition("固定资产入库管理", Route = "/fixed-assets/storage")]
public class FixedAssetsStorageController : ControllerBase
{
    private readonly IFixedAssetsStorageService _service;

    public FixedAssetsStorageController(IFixedAssetsStorageService service)
    {
        _service = service;
    }

    // GET api/fixed-assets-storage/condition-selector
    [HttpGet("api/fixed-assets-storage/condition-selector")]
    public Task<PageResult<FixedAssetsStorage.QueryModel>> GetPageAsync([FromQuery] FixedAssetsStorage.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }


    // POST api/fixed-assets-storage
    [HttpPost("api/fixed-assets-storage")]
    [DynamicAuthorize]
    [PermisstionDefinition("固定资产入库")]
    public Task<OperationalResult> CreateAsync(FixedAssetsStorage.CreateModel fixedAssetsStorage)
    {
        return _service.CreateAsync(fixedAssetsStorage, HttpContext.RequestAborted);
    }
}