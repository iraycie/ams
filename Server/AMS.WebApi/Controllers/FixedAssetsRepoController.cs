﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("固定资产库存管理", Route = "/fixed-assets/repo")]
public class FixedAssetsRepoController : ControllerBase
{
    private readonly IFixedAssetsRepoService _service;

    public FixedAssetsRepoController(IFixedAssetsRepoService service)
    {
        _service = service;
    }

    // GET api/fixed-assets-repo/condition-selector
    [HttpGet("api/fixed-assets-repo/condition-selector")]
    public Task<PageResult<FixedAssetsRepo.QueryModel>> GetPageAsync([FromQuery] FixedAssetsRepo.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // PUT api/fixed-assets-repo/5    
    [HttpPut("api/fixed-assets-repo/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("更新固定资产库存")]
    public Task<OperationalResult> UpdateAsync(int id, FixedAssetsRepo.UpdateModel fixedAssetsRepo)
    {
        return _service.UpdateAsync(id, fixedAssetsRepo, HttpContext.RequestAborted);
    }

    // GET api/fixed-assets-repo/approved
    [HttpGet("api/fixed-assets-repo/approved")]
    public Task<ListResult<FixedAssetsRepo.QueryModel>> GetApprovedAsync()
    {
        return _service.GetApprovedAsync(HttpContext.RequestAborted);
    }

    // GET api/fixed-assets-repo/owned
    [HttpGet("api/fixed-assets-repo/owned")]
    public Task<ListResult<FixedAssetsRepo.QueryModel>> GetOwnedAsync()
    {
        return _service.GetOwnedAsync(HttpContext.RequestAborted);
    }
}