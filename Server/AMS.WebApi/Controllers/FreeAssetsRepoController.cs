﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("易耗品资产库存管理", Route = "/free-assets/repo")]
public class FreeAssetsRepoController : ControllerBase
{
    private readonly IFreeAssetsRepoService _service;

    public FreeAssetsRepoController(IFreeAssetsRepoService service)
    {
        _service = service;
    }

    // GET api/free-assets-repo/condition-selector
    [HttpGet("api/free-assets-repo/condition-selector")]
    public Task<PageResult<FreeAssetsRepo.QueryModel>> GetPageAsync([FromQuery] FreeAssetsRepo.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // GET api/free-assets-repo/approved
    [HttpGet("api/free-assets-repo/approved")]
    public Task<ListResult<FreeAssetsRepo.QueryModel>> GetApprovedAsync()
    {
        return _service.GetApprovedAsync(HttpContext.RequestAborted);
    }
}