﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("固定资产领用管理", Route = "/fixed-assets/consuming")]
public class FixedAssetsConsumingController : ControllerBase
{
    private readonly IFixedAssetsConsumingService _service;

    public FixedAssetsConsumingController(IFixedAssetsConsumingService service)
    {
        _service = service;
    }

    // GET api/fixed-assets-consuming/condition-selector
    [HttpGet("api/fixed-assets-consuming/condition-selector")]
    public Task<PageResult<FixedAssetsConsuming.QueryModel>> GetPageAsync([FromQuery] FixedAssetsConsuming.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // POST api/fixed-assets-consuming
    [HttpPost("api/fixed-assets-consuming")]
    [DynamicAuthorize]
    [PermisstionDefinition("固定资产领用")]
    public Task<OperationalResult> CreateAsync(FixedAssetsConsuming.CreateModel fixedAssetsConsuming)
    {
        return _service.CreateAsync(fixedAssetsConsuming, HttpContext.RequestAborted);
    }

    // GET api/fixed-assets-consuming
    [HttpGet("api/fixed-assets-consuming/return")]
    public Task<OperationalResult> ReturnAsync([FromQuery] int id)
    {
        return _service.ReturnAsync(id, HttpContext.RequestAborted);
    }
}