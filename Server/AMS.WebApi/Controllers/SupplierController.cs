﻿using DynamicAuthorization.Sdk.Attributes;
using Microsoft.AspNetCore.Authorization;

namespace AMS.WebApi.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
[PermisstionDefinition("供应商管理", Route = "/settings/supplier")]
public class SupplierController : ControllerBase
{
    private readonly ISupplierService _service;

    public SupplierController(ISupplierService service)
    {
        _service = service;
    }

    // GET api/supplier
    [HttpGet("api/supplier")]
    public Task<ListResult<Supplier.QueryModel>> GetListAsync()
    {
        return _service.GetListAsync(HttpContext.RequestAborted);
    }

    // GET api/supplier/page
    [HttpGet("api/supplier/page")]
    public Task<PageResult<Supplier.QueryModel>> GetPageAsync([FromQuery] PageParameter input)
    {
        return _service.GetPageAsync(input, HttpContext.RequestAborted);
    }

    // POST api/supplier
    [HttpPost("api/supplier")]
    [DynamicAuthorize]
    [PermisstionDefinition("添加供应商")]
    public Task<OperationalResult> CreateAsync(Supplier.CreateModel supplier)
    {
        return _service.CreateAsync(supplier, HttpContext.RequestAborted);
    }

    // PUT api/supplier/5
    [HttpPut("api/supplier/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("更新供应商")]
    public Task<OperationalResult> UpdateAsync(int id, Supplier.UpdateModel supplier)
    {
        return _service.UpdateAsync(id, supplier, HttpContext.RequestAborted);
    }

    // DELETE api/supplier/5
    [HttpDelete("api/supplier/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("删除供应商")]
    public Task<OperationalResult> DeleteAsync(int id)
    {
        return _service.DeleteAsync(id, HttpContext.RequestAborted);
    }
}