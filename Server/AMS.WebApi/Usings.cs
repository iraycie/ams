global using AMS.Common;
global using AMS.Services.Abstractions;
global using AMS.Services.Models;

global using Microsoft.AspNetCore.Mvc;
global using System.Text.Json.Serialization;