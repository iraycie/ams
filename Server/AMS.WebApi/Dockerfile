FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Server/AMS.WebApi/AMS.WebApi.csproj", "Server/AMS.WebApi/"]
COPY ["Server/AMS.EFCore/AMS.EFCore.csproj", "Server/AMS.EFCore/"]
COPY ["Server/AMS.Services/AMS.Services.csproj", "Server/AMS.Services/"]
COPY ["Server/OA.Infrastructure/OA.Infrastructure.csproj", "Server/OA.Infrastructure/"]
COPY ["Server/AMS.Common/AMS.Common.csproj", "Server/AMS.Common/"]
COPY ["Server/DynamicAuthorization.Sdk/DynamicAuthorization.Sdk.csproj", "Server/DynamicAuthorization.Sdk/"]
ENV ASPNETCORE_ENVIRONMENT Production
ENV ASPNETCORE_URLS https://*:80
RUN dotnet restore "Server/AMS.WebApi/AMS.WebApi.csproj"
COPY . .
WORKDIR "/src/Server/AMS.WebApi"
RUN dotnet build "AMS.WebApi.csproj" -c Debug -o /app/build

FROM build AS publish
RUN dotnet publish "AMS.WebApi.csproj" -c Debug -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AMS.WebApi.dll"]