﻿namespace System;

public static class StringExtensions
{
    public static string LimitLength(this string source, int limit)
    {
        if (limit < 4)
        {
            throw new ArgumentException("限制长度的最小值为4", nameof(limit));
        }

        if (source.Length > limit)
        {
            var bound = limit - 3;

            source = $"{source[..bound]}...";
        }

        return source;
    }
}