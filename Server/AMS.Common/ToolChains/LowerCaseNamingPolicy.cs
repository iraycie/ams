﻿using System.Text.Json;

namespace AMS.Common.ToolChains;

public sealed class LowerCaseNamingPolicy : JsonNamingPolicy
{
    public static LowerCaseNamingPolicy LowerCase { get; }

    static LowerCaseNamingPolicy()
    {
        LowerCase = new LowerCaseNamingPolicy();
    }

    private LowerCaseNamingPolicy()
    {

    }

    public override string ConvertName(string name)
    {
        return name.ToLower();
    }
}