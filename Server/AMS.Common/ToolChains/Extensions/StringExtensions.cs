﻿namespace AMS.Common.ToolChains.Extensions;

public static class StringExtensions
{
    public static string Disorder(this string source)
    {
        return string.Concat(source.Select(n => new { n, id = Guid.NewGuid() }).OrderBy(x => x.id).Select(m => m.n));
    }
}