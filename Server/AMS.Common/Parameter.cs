﻿namespace AMS.Common;

public class PageParameter
{
    public int PageNum { get; set; } = 1;

    public int PageSize { get; set; } = 15;
}