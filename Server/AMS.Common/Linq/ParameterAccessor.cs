namespace AMS.Common.Linq;

internal class ParameterAccessor : ExpressionVisitor
{
    private readonly ParameterExpression _parameter;

    public ParameterAccessor(ParameterExpression parameter)
    {
        _parameter = parameter;
    }

    protected override Expression VisitParameter(ParameterExpression node)
    {
        return _parameter;
    }
}