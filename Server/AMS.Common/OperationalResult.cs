﻿namespace AMS.Common;

public class OperationalResult
{
    public const int ERROR = -1;
    public const int SUCCESS = 0;
    public const int FAILURE = 1;

    public int Status { get; set; }

    public string Message { get; set; } = string.Empty;

    public static OperationalResult Ok(string? message = null)
    {
        var result = new OperationalResult
        {
            Status = SUCCESS,
            Message = string.IsNullOrEmpty(message) ? string.Empty : message
        };

        return result;
    }

    public static OperationalResult Fail(string? message = null)
    {
        var result = new OperationalResult
        {
            Status = FAILURE,
            Message = string.IsNullOrEmpty(message) ? string.Empty : message
        };

        return result;
    }

    public bool EnsureSucceed() => Status == SUCCESS;
}

public class OperationalResult<TModel> : OperationalResult
{
    public TModel Data { get; set; }

    public static new OperationalResult<TModel> Fail(string? message = null)
    {
        var result = new OperationalResult<TModel>
        {
            Status = FAILURE,
            Message = string.IsNullOrEmpty(message) ? string.Empty : message
        };

        return result;
    }
}

public class ListResult<TModel> : OperationalResult<IEnumerable<TModel>>
{

}

public class PageResult<TModel> : ListResult<TModel>
{
    public int PageNum { get; set; }

    public int PageSize { get; set; }

    public int Total { get; set; }
}