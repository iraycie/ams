using DynamicAuthorization.Sdk.Abstarctions;
using DynamicAuthorization.Sdk.Impl;
using Microsoft.Extensions.DependencyInjection;

namespace DynamicAuthorization.Sdk.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDynamicAuthorization<THandler>(this IServiceCollection services)
        where THandler : class, IUserGrantHandler
    {
        services.AddScoped<IUserGrantHandler, THandler>();
        services.AddScoped<IGrantValidator, GrantValidator>();
        services.AddSingleton<IGrantService, GrantService>();

        services.AddMemoryCache();
        services.AddHttpContextAccessor();

        return services;
    }
}