﻿namespace DynamicAuthorization.Sdk.Model;

public class RoutePermissionInfo
{
    public string Controller { get; set; } = null!;

    public string? ControllerDisplayName { get; set; }

    public string Name { get; set; } = null!;
}