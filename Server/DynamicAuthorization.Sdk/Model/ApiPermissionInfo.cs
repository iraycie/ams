﻿namespace DynamicAuthorization.Sdk.Model;

public class ApiPermissionInfo
{
    public string Controller { get; set; } = null!;

    public string? ControllerDisplayName { get; set; }

    public IDictionary<string, string> ApiInfo { get; set; } = new Dictionary<string, string>();
}