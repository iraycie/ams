﻿namespace DynamicAuthorization.Sdk.Exceptions;

public class RejectedAccessException : Exception
{
	public new string? Message { get; set; }

	public RejectedAccessException()
	{
		Message = base.Message;
	}

	public RejectedAccessException(string message)
	{
		Message = message;
	}
}