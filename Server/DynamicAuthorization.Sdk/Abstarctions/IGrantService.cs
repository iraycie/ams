using System.Reflection;

namespace DynamicAuthorization.Sdk.Abstarctions;

public interface IGrantService
{
    IGrantTypeMetadata Analyze(string grant);

    bool Check(string grant, Type controller, MethodInfo action);
}