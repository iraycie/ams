namespace DynamicAuthorization.Sdk.Abstarctions;

public interface IUserGrantHandler
{
    /// <summary>
    /// 获取用户的授权信息，权限之间用空格隔开
    /// </summary>
    /// <param name="account"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>例如"UserController Temp:TempAsync(Int32)"</returns>
    Task<string?> GetUserGrantsAsync(string account, CancellationToken cancellationToken = default);
}