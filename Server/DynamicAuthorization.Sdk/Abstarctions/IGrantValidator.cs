using System.Reflection;

namespace DynamicAuthorization.Sdk.Abstarctions;

internal interface IGrantValidator
{
    Task<bool> CheckAsync(string account, Type controller, MethodInfo action, CancellationToken cancellationToken = default);
}