using System.Reflection;

namespace DynamicAuthorization.Sdk.Abstarctions;

public interface IGrantTypeMetadata
{
    bool IsAdmin { get; }

    ICollection<Type> GrantClasses { get; }

    ICollection<Type> RejectClasses { get; }

    IDictionary<Type, List<MethodInfo>> GrantMethods { get; }

    IDictionary<Type, List<MethodInfo>> RejectMethods { get; }

    Type? MapClass(Type controller);

    MethodInfo? MapMethod(Type controller, MethodInfo action);
}