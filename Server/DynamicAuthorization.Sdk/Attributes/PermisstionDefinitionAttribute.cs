﻿namespace DynamicAuthorization.Sdk.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
public class PermisstionDefinitionAttribute : Attribute
{
	public string DisplayName { get; set; } = string.Empty;

	public string Route { get; set; } = string.Empty;

	public PermisstionDefinitionAttribute()
	{

	}

	public PermisstionDefinitionAttribute(string displayName)
	{
		DisplayName = displayName;
	}
}