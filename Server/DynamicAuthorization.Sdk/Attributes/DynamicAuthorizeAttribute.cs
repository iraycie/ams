using DynamicAuthorization.Sdk.Filters;
using Microsoft.AspNetCore.Mvc;

namespace DynamicAuthorization.Sdk.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class DynamicAuthorizeAttribute : TypeFilterAttribute
{
    public DynamicAuthorizeAttribute() : base(typeof(DynamicAuthorizationFilter))
    {

    }
}