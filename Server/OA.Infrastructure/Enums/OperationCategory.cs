﻿namespace OA.Infrastructure.Enums;

/// <summary>
/// 操作类别
/// </summary>
public enum OperationCategory
{
    /// <summary>
    /// 查询
    /// </summary>
    Query = 1,

    /// <summary>
    /// 新增
    /// </summary>
    Add = 2,

    /// <summary>
    /// 更新
    /// </summary>
    Update = 3,

    /// <summary>
    /// 删除
    /// </summary>
    Delete = 4,
}