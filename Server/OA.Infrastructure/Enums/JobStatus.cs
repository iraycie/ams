﻿namespace OA.Infrastructure.Enums;

/// <summary>
/// 任务状态
/// </summary>
public enum JobStatus
{
    /// <summary>
    /// 禁用
    /// </summary>
    Disable = -1,

    /// <summary>
    /// 激活
    /// </summary>
    Active = 1,

    /// <summary>
    /// 已结束
    /// </summary>
    Closed = 2,
}