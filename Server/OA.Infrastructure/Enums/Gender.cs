﻿namespace OA.Infrastructure.Enums;

/// <summary>
/// 性别
/// </summary>
public enum Gender
{
    /// <summary>
    /// 未定义
    /// </summary>
    Undefined = 0,

    /// <summary>
    /// 男性
    /// </summary>
    Male = 1,

    /// <summary>
    /// 女性
    /// </summary>
    Female = 2,
}