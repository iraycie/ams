﻿global using AMS;
global using AMS.Common;
global using AMS.Common.Linq;
global using OA.Infrastructure.Abstractions;
global using OA.Infrastructure.DomainModels;
global using OA.Infrastructure.Models;

global using Microsoft.AspNetCore.Mvc;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.EntityFrameworkCore.ChangeTracking;
global using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
global using Microsoft.Extensions.Configuration;
global using Microsoft.Extensions.DependencyInjection;
global using System.Collections;
global using System.ComponentModel;
global using System.ComponentModel.DataAnnotations;
global using System.Linq.Expressions;
global using System.Text.Json;