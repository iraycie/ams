﻿using OA.Infrastructure.Comparers;
using OA.Infrastructure.Converters;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OA.Infrastructure.Configurations;

public class RoleConfiguration : IEntityTypeConfiguration<Role>
{
    public void Configure(EntityTypeBuilder<Role> builder)
    {
        builder
            .Property(p => p.Types)
            .HasConversion(new CollectionToStringConverter<List<string>>(), new CollectionValueComparer<List<string>, string>()).IsRequired(false);

        builder
            .Property(p => p.ViewRoutes)
            .HasConversion(new CollectionToStringConverter<List<string>>(), new CollectionValueComparer<List<string>, string>()).IsRequired(false);
    }
}