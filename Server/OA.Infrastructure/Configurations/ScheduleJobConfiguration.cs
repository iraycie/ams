﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OA.Infrastructure.Configurations;

public class ScheduleJobConfiguration : IEntityTypeConfiguration<ScheduleJob>
{
    public void Configure(EntityTypeBuilder<ScheduleJob> builder)
    {
        builder.Property(p => p.Status).HasConversion<string>();
    }
}