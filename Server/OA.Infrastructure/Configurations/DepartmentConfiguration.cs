﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OA.Infrastructure.Configurations;

public class DepartmentConfiguration : IEntityTypeConfiguration<Department>
{
    public void Configure(EntityTypeBuilder<Department> builder)
    {
        builder
            .HasOne(p => p.Leader)
            .WithOne()
            .HasForeignKey<Department>(p => p.LeaderId)
            .OnDelete(DeleteBehavior.SetNull);

        builder
            .HasOne(p => p.Parent)
            .WithOne()
            .OnDelete(DeleteBehavior.SetNull);
    }
}