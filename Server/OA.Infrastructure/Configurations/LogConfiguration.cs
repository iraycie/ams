﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OA.Infrastructure.Configurations;

public class LogConfiguration : IEntityTypeConfiguration<Log>
{
    public void Configure(EntityTypeBuilder<Log> builder)
    {
        builder
            .HasOne(p => p.Operator)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Property(p => p.Operation).HasConversion<string>();
    }
}