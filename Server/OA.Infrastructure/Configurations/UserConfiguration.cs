﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OA.Infrastructure.Configurations;

public class UserConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder
            .HasOne(p => p.Department)
            .WithMany()
            .HasForeignKey("DepartmentId")
            .OnDelete(DeleteBehavior.SetNull);

        builder
            .HasOne(p => p.DirectLeader)
            .WithMany()
            .OnDelete(DeleteBehavior.SetNull);

        builder.Property(p => p.Gender).HasConversion<string>();
    }
}