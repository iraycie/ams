﻿using OA.Infrastructure.WeCom.Serializers;
using OA.Infrastructure.WeCom.Serializers.Abstractions;

namespace Microsoft.Extensions.DependencyInjection;

internal static class WeComEventSerializerExtensions
{
    public static IServiceCollection RegisterWeComEventSerializers(this IServiceCollection services)
    {
        services.AddSingleton<IWeComMessageSerializable, WeComTemplateCardEventSerializer>();
        services.AddSingleton<IWeComMessageSerializable, WeComTemplateCardMenuEventSerializer>();
        services.AddSingleton<IWeComMessageSerializable, WeComChangeContactEventSerializer>();

        return services;
    }
}