﻿namespace OA.Infrastructure.WeCom.Serializers.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class EventTypeAttribute : Attribute
{
    private readonly string _eventType;

    public string EventType => _eventType;

    public EventTypeAttribute(string eventType)
    {
        _eventType = eventType;
    }
}