﻿using OA.Infrastructure.DomainModels.Callback;
using OA.Infrastructure.WeCom.Serializers.Abstractions;
using OA.Infrastructure.WeCom.Serializers.Attributes;
using System.Xml.Serialization;

namespace OA.Infrastructure.WeCom.Serializers;

[EventType(WeComCallbackEvents.ChangeContactEvent)]
public class WeComChangeContactEventSerializer : XmlSerializer, IWeComMessageSerializable
{
	public WeComChangeContactEventSerializer()
		: base(typeof(WeComChangeContractEvent))
	{

	}
}