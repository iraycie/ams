﻿namespace OA.Infrastructure.WeCom;

public static class WeComCallbackEvents
{
    public const string ChangeContactEvent = "change_contact";
    public const string TemplateCardEvent = "template_card_event";
    public const string TemplateCardMenuEvent = "template_card_menu_event";

    /// <summary>
    /// Event Key
    /// </summary>
    public const string TemplateCardApprovePassEventKey = "pass";
    public const string TemplateCardApproveRejectEventKey = "reject";
    public const string TemplateCardApproveNoticeSelfEventKey = "notice_self";

    public const string TemplateCardUserTemporaryAuthGrantEventKey = "user_temp_auth_grant";
    public const string TemplateCardUserTemporaryAuthRejectEventKey = "user_temp_auth_reject";

    public const string TemplateCardPermissionTemporaryAuthGrantEventKey = "permission_temp_auth_grant";
    public const string TemplateCardPermissionTemporaryAuthRejectEventKey = "permission_temp_auth_reject";
}