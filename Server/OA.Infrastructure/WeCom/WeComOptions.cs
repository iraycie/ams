﻿namespace OA.Infrastructure.WeCom;

public class WeComOptions
{
    public const string Position = "WeCom";

    public string CorpId { get; set; } = null!;

    public string Secret { get; set; } = null!;

    public int AgentId { get; set; }

    public string RedirectUri { get; set; } = null!;

    public string? Token { get; set; }

    public string? AesKey { get; set; }
}