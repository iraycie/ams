﻿using OA.Infrastructure.DomainModels.Message;
using OA.Infrastructure.WeCom.Abstractions;

namespace OA.Infrastructure.WeCom.Generator;

public class WeComButtonTemplateCardEngine : WeComTemplateCardEngine<WeComButtonTemplateCard>
{
    private readonly WeComButtonTemplateCard _template = null!;

    private WeComButtonTemplateCardEngine()
        : base(false, false)
    {
        throw new NotImplementedException();
    }

    internal WeComButtonTemplateCardEngine(bool enableIdTranslate, bool enableDuplicateCheck)
        : base(enableIdTranslate, enableDuplicateCheck)
    {
        _template = new()
        {
            Template_card = new WeComButtonTemplateCardDetail()
        };
    }

    public override WeComButtonTemplateCard Generate()
    {
        if (_template.Template_card.Action_menu is not null)
        {
            if (!_template.Template_card.Action_menu.Action_list.Any())
            {
                throw new InvalidOperationException("带操作菜单的卡片，操作菜单项至少得存在一个，请调用<AddActionMenuItem>添加菜单项");
            }
        }

        if (_template.Template_card.Button_selection is not null)
        {
            if (!_template.Template_card.Button_selection.Option_list.Any())
            {
                throw new InvalidOperationException("带下拉选择器的卡片，下拉项至少得存在一个，请调用<AddButtonSelecionOptionItem>添加菜单项");
            }
        }

        if (EnableIdTranslate)
        {
            _template.Enable_id_trans = 1;
        }

        if (EnableDuplicateCheck)
        {
            _template.Enable_Duplicate_Check = 1;
        }

        return _template;
    }

    public override WeComButtonTemplateCardEngine SetAgentId(int agentId)
    {
        _template.AgentId = agentId;

        return this;
    }

    public override WeComButtonTemplateCardEngine ToParty(string toParty)
    {
        _template.ToParty = toParty;

        return this;
    }

    public override WeComButtonTemplateCardEngine ToTag(string toTag)
    {
        _template.ToTag = toTag;

        return this;
    }

    public override WeComButtonTemplateCardEngine ToUser(string toUser)
    {
        _template.ToUser = toUser;

        return this;
    }

    public WeComButtonTemplateCardEngine WithSource(TemplateCardSource source)
    {
        _template.Template_card.Source = source;

        return this;
    }

    public WeComButtonTemplateCardEngine WithActionMenu(string menuDescription)
    {
        _template.Template_card.Action_menu = new TemplateCardActionMenu
        {
            Desc = menuDescription
        };

        return this;
    }

    public WeComButtonTemplateCardEngine AddActionMenuItem(TemplateCardActionMenuItem item)
    {
        if (_template.Template_card.Action_menu is null)
        {
            throw new InvalidOperationException("请先调用<WithActionMenu>方法构建菜单");
        }

        if (_template.Template_card.Action_menu.Action_list.Count == 3)
        {
            throw new InvalidOperationException("菜单项最多为3个");
        }

        _template.Template_card.Action_menu.Action_list.Add(item);

        return this;
    }

    public WeComButtonTemplateCardEngine WithCardActionForUrl(string url)
    {
        _template.Template_card.Card_action = new TemplateCardAction
        {
            Type = 1,
            Url = url
        };

        return this;
    }

    public WeComButtonTemplateCardEngine WithCardActionForApp(string appId, string pagePath)
    {
        _template.Template_card.Card_action = new TemplateCardAction
        {
            Type = 2,
            AppId = appId,
            PagePath = pagePath
        };

        return this;
    }

    public WeComButtonTemplateCardEngine WithTitle(string title, string description)
    {
        _template.Template_card.Main_title = new TemplateCardMainTitle
        {
            Title = title,
            Desc = description
        };

        return this;
    }

    public WeComButtonTemplateCardEngine WithSubTitle(string subTitle)
    {
        _template.Template_card.Sub_title_text = subTitle;

        return this;
    }

    public WeComButtonTemplateCardEngine AddHoriztontalContentItemForText(string keyName, string value)
    {
        AddHoriztontalContentItem(new TemplateCardHorizontalContentItem
        {
            Type = 0,
            Keyname = keyName,
            Value = value
        });

        return this;
    }

    public WeComButtonTemplateCardEngine AddHoriztontalContentItemForUrl(string keyName, string value, string url)
    {
        AddHoriztontalContentItem(new TemplateCardHorizontalContentItem
        {
            Type = 1,
            Keyname = keyName,
            Value = value,
            Url = url
        });

        return this;
    }

    public WeComButtonTemplateCardEngine AddHoriztontalContentItemForMedia(string keyName, string value, string mediaId)
    {
        AddHoriztontalContentItem(new TemplateCardHorizontalContentItem
        {
            Type = 2,
            Keyname = keyName,
            Value = value,
            Media_id = mediaId
        });

        return this;
    }

    public WeComButtonTemplateCardEngine AddHoriztontalContentItemForUserDetail(string keyName, string value, string userId)
    {
        AddHoriztontalContentItem(new TemplateCardHorizontalContentItem
        {
            Type = 3,
            Keyname = keyName,
            Value = value,
            UserId = userId
        });

        return this;
    }

    public WeComButtonTemplateCardEngine AddButtonForCallback(string text, string key, WeComTemplateCardButtonStyle style = WeComTemplateCardButtonStyle.Primary)
    {
        AddButton(new TemplateCardButtonItem
        {
            Type = 0,
            Text = text,
            Style = (int)style,
            Key = key
        });

        return this;
    }

    public WeComButtonTemplateCardEngine AddButtonForUrl(string text, string url, WeComTemplateCardButtonStyle style = WeComTemplateCardButtonStyle.Primary)
    {
        AddButton(new TemplateCardButtonItem
        {
            Type = 1,
            Text = text,
            Style = (int)style,
            Url = url
        });

        return this;
    }

    public WeComButtonTemplateCardEngine WithButtonSelection(string questionKey, string title, string? selectedId)
    {
        _template.Template_card.Button_selection = new TemplateCardButtonSelection()
        {
            Question_key = questionKey,
            Title = title,
            Selected_id = selectedId
        };

        return this;
    }

    public WeComButtonTemplateCardEngine AddButtonSelectionOptionItem(TemplateCardButtonSelectionOptionItem item)
    {
        if (_template.Template_card.Button_selection is null)
        {
            throw new InvalidOperationException("请先调用<WithButtonSelection>方法构建下拉选择器");
        }

        if (_template.Template_card.Button_selection.Option_list.Count == 10)
        {
            throw new InvalidOperationException("下拉选择项最多为10个");
        }

        if (_template.Template_card.Button_selection.Option_list.Any(x => x.Id == item.Id))
        {
            throw new ArgumentException("下拉选择项的事件ID不可重复", nameof(item));
        }

        _template.Template_card.Button_selection.Option_list.Add(item);

        return this;
    }

    public WeComButtonTemplateCardEngine WithQuoteArea(string title, string quoteText)
    {
        _template.Template_card.Quote_area = new TemplateCardQuoteArea
        {
            Title = title,
            Quote_text = quoteText
        };

        return this;
    }

    public WeComButtonTemplateCardEngine AddQuoteAreaJumpForUrl(string url)
    {
        if (_template.Template_card.Quote_area is null)
        {
            throw new InvalidOperationException("请先调用<WithQuoteArea>方法构建引用文献区域");
        }

        _template.Template_card.Quote_area.Type = 1;
        _template.Template_card.Quote_area.Url = url;

        return this;
    }

    public WeComButtonTemplateCardEngine AddQuoteAreaJumpForApp(string appId, string? pagePath)
    {
        if (_template.Template_card.Quote_area is null)
        {
            throw new InvalidOperationException("请先调用<WithQuoteArea>方法构建引用文献区域");
        }

        _template.Template_card.Quote_area.Type = 2;
        _template.Template_card.Quote_area.AppId = appId;
        _template.Template_card.Quote_area.PagePath = pagePath;

        return this;
    }

    private void AddHoriztontalContentItem(TemplateCardHorizontalContentItem item)
    {
        if (_template.Template_card.Horizontal_content_list.Count == 6)
        {
            throw new InvalidOperationException("菜单项最多为6个");
        }

        _template.Template_card.Horizontal_content_list.Add(item);
    }

    private void AddButton(TemplateCardButtonItem item)
    {
        if (_template.Template_card.Button_list.Count == 6)
        {
            throw new InvalidOperationException("菜单项最多为6个");
        }

        _template.Template_card.Button_list.Add(item);
    }
}

public enum WeComTemplateCardButtonStyle : int
{
    Primary = 1,
    Secondary = 2,
    Dangerous = 3,
    Normal = 4
}