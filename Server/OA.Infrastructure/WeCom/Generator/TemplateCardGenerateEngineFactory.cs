﻿namespace OA.Infrastructure.WeCom.Generator;

public static class TemplateCardGenerateEngineFactory
{
    public static WeComButtonTemplateCardEngine GetButtonTemplateCardEngine(bool enableIdTranslate, bool enableDuplicateCheck)
    {
        return new WeComButtonTemplateCardEngine(enableIdTranslate, enableDuplicateCheck);
    }
}