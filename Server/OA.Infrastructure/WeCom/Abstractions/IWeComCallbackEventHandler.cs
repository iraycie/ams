﻿using OA.Infrastructure.DomainModels.Callback;

namespace OA.Infrastructure.WeCom.Abstractions;

public interface IWeComCallbackEventHandler<TEvent>
    where TEvent : WeComCallbackEventBase
{
    Task<OperationalResult> HandleAsync(TEvent args, CancellationToken cancellationToken);
}