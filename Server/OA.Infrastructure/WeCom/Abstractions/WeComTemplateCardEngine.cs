﻿using OA.Infrastructure.DomainModels.Message;

namespace OA.Infrastructure.WeCom.Abstractions;

public abstract class WeComTemplateCardEngine<TMessage>
    where TMessage : WeComMessage
{
    protected bool EnableIdTranslate { get; private set; }

    protected bool EnableDuplicateCheck { get; private set; }

    protected int DuplicateCheckInterval { get; private set; } = 1800;

    protected WeComTemplateCardEngine(bool enableIdTranslate, bool enableDuplicateCheck)
    {
        EnableIdTranslate = enableIdTranslate;
        EnableDuplicateCheck = enableDuplicateCheck;
    }

    public void SetDuplicateCheckInterval(int interval)
    {
        if (interval > 14400)
        {
            throw new InvalidOperationException("重复消息检查的时间间隔最大不超过4小时");
        }

        DuplicateCheckInterval = interval;
    }

    public abstract WeComTemplateCardEngine<TMessage> ToUser(string toUser);

    public abstract WeComTemplateCardEngine<TMessage> ToParty(string toParty);

    public abstract WeComTemplateCardEngine<TMessage> ToTag(string toTag);

    public abstract WeComTemplateCardEngine<TMessage> SetAgentId(int agentId);

    public abstract TMessage Generate();
}