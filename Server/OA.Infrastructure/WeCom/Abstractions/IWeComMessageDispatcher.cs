﻿using System.Xml.Serialization;

namespace OA.Infrastructure.WeCom.Abstractions;

public interface IWeComMessageDispatcher
{
    WeComMessageDispatcher Dispatch(string msg);

    bool TryGetSerializer(out XmlSerializer serializer);
}