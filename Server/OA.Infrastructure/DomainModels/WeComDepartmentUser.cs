﻿namespace OA.Infrastructure.DomainModels;

public class WeComDepartmentUser
{
    public string UserId { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Telephone { get; set; } = string.Empty;

    public int IsLeader { get; set; }

    public int[] Is_leader_in_dept { get; set; } = Array.Empty<int>();

    public string[] Direct_leader { get; set; } = Array.Empty<string>();
}

public class WeComDepartmentUsers : WeComResponseBase
{
    public ICollection<WeComDepartmentUser> UserList { get; set; } = new HashSet<WeComDepartmentUser>();
}