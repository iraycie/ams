﻿namespace OA.Infrastructure.DomainModels;

public class WeComResponseBase
{
    public int Errcode { get; set; }

    public string ErrMsg { get; set; } = string.Empty;
}