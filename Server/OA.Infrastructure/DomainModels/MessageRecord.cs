﻿namespace OA.Infrastructure.DomainModels;

public record MessageRecord
{
    public string Sender { get; set; } = string.Empty;

    public MessageLevel Level { get; set; } = MessageLevel.Information;
}

public record MessageRecord<TData> : MessageRecord
{
    public TData? Data { get; set; }
}

public enum MessageLevel
{
    Information = 0,
    Warning = 1,
    Error = 2,
    Dangerous = 3
}