﻿namespace OA.Infrastructure.DomainModels;

public class WeComCorpTicket : WeComResponseBase
{
    public string Access_token { get; set; } = null!;

    public int Expires_in { get; set; }
}