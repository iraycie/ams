﻿namespace OA.Infrastructure.DomainModels;

public class WeComDepartment
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Name_en { get; set; } = string.Empty;

    public string[] Department_leader { get; set; } = Array.Empty<string>();

    public int ParentId { get; set; }

    public int Order { get; set; }
}

public class WeComDepartments : WeComResponseBase
{
    public ICollection<WeComDepartment> Department { get; set; } = new HashSet<WeComDepartment>();
}