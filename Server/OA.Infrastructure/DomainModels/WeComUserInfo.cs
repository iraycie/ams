﻿namespace OA.Infrastructure.DomainModels;

public class WeComUserInfo : WeComResponseBase
{
    public string UserId { get; set; } = null!;

    public string User_ticket { get; set; } = string.Empty;
}