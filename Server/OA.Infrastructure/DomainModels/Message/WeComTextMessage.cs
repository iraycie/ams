﻿namespace OA.Infrastructure.DomainModels.Message;

public class WeComTextMessage : WeComMessage
{
    public override string MsgType { get; } = "text";

    public WeComTextMessageDetail Text { get; set; } = null!;

    public int Safe { get; set; } = 0;
}

public class WeComTextMessageDetail
{
    [StringLength(2048)]
    public string Content { get; set; } = string.Empty;
}