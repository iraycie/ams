﻿namespace OA.Infrastructure.DomainModels.Message;

public abstract class WeComOperationMenuTemplateCard : WeComTemplateCard
{
    public TemplateCardActionMenu? Action_menu { get; set; }

    public TemplateCardAction? Card_action { get; set; }
}

public class TemplateCardActionMenu
{
    public string Desc { get; set; } = string.Empty;

    public ICollection<TemplateCardActionMenuItem> Action_list { get; set; } = new List<TemplateCardActionMenuItem>(3);
}

public class TemplateCardActionMenuItem
{
    public string Text { get; set; } = null!;

    [StringLength(512)]
    public string Key { get; set; } = null!;
}

public class TemplateCardAction
{
    public int Type { get; set; } = 0;

    public string? Url { get; set; }

    public string? AppId { get; set; }

    public string? PagePath { get; set; }
}