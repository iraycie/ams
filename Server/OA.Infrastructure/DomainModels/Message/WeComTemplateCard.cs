﻿namespace OA.Infrastructure.DomainModels.Message;

public abstract class WeComTemplateCard
{
    public abstract string Card_type { get; protected set; }

    public TemplateCardSource? Source { get; set; }

    public TemplateCardMainTitle Main_title { get; set; } = null!;

    public string? Sub_title_text { get; set; }

    public string Task_id { get; set; } = null!;
}

public class TemplateCardSource
{
    public string Icon_url { get; set; } = null!;

    public string Desc { get; set; } = string.Empty;

    public int Desc_color { get; set; } = 0;
}

public class TemplateCardMainTitle
{
    [StringLength(36)]
    public string Title { get; set; } = null!;

    [StringLength(44)]
    public string Desc { get; set; } = null!;
}

public class TemplateCardQuoteArea
{
    public int Type { get; set; } = 0;

    public string? Url { get; set; }

    public string? AppId { get; set; }

    public string? PagePath { get; set; }

    public string Title { get; set; } = string.Empty;

    public string Quote_text { get; set; } = string.Empty;
}

public class TemplateCardHorizontalContentItem
{
    public int Type { get; set; } = 0;

    [StringLength(5)]
    public string Keyname { get; set; } = string.Empty;

    [StringLength(30)]
    public string Value { get; set; } = null!;

    public string? Url { get; set; }

    public string? Media_id { get; set; }

    public string? UserId { get; set; }
}