﻿namespace OA.Infrastructure.DomainModels.Message;

public abstract class WeComMessage
{
    public string ToUser { get; set; } = string.Empty;
    public string ToParty { get; set; } = string.Empty;
    public string ToTag { get; set; } = string.Empty;
    public abstract string MsgType { get; }
    public int AgentId { get; set; }
    public int Enable_id_trans { get; set; } = 0;
    public int Enable_Duplicate_Check { get; set; } = 0;
    public int Duplicate_Check_Interval { get; set; } = 1800;
}