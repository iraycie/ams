﻿namespace OA.Infrastructure.DomainModels.Message;

public class WeComButtonTemplateCard : WeComMessage
{
    public override string MsgType { get; } = "template_card";

    public WeComButtonTemplateCardDetail Template_card { get; set; } = null!;
}

public class WeComButtonTemplateCardDetail : WeComOperationMenuTemplateCard
{
    public override string Card_type { get; protected set; } = "button_interaction";

    public TemplateCardQuoteArea? Quote_area { get; set; }

    public ICollection<TemplateCardHorizontalContentItem> Horizontal_content_list { get; set; } = new List<TemplateCardHorizontalContentItem>(6);

    public TemplateCardButtonSelection? Button_selection { get; set; }

    public ICollection<TemplateCardButtonItem> Button_list { get; set; } = new List<TemplateCardButtonItem>(6);
}

public class TemplateCardButtonSelection
{
    [StringLength(512)]
    public string Question_key { get; set; } = null!;

    public string Title { get; set; } = string.Empty;

    public ICollection<TemplateCardButtonSelectionOptionItem> Option_list { get; set; } = new List<TemplateCardButtonSelectionOptionItem>(10);

    public string? Selected_id { get; set; }
}

public class TemplateCardButtonSelectionOptionItem
{
    public string Id { get; set; } = null!;

    [StringLength(16)]
    public string Text { get; set; } = null!;
}

public class TemplateCardButtonItem
{
    public int? Type { get; set; }

    [StringLength(10)]
    public string Text { get; set; } = null!;

    public int Style { get; set; } = 1;

    [StringLength(512)]
    public string? Key { get; set; }

    public string? Url { get; set; }
}