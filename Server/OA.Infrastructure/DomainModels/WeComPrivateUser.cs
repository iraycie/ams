﻿namespace OA.Infrastructure.DomainModels;

public class WeComPrivateUser : WeComResponseBase
{
    public string UserId { get; set; } = null!;
    public string Gender { get; set; } = null!;
    public string Avatar { get; set; } = string.Empty;
    public string Qr_code { get; set; } = string.Empty;
    public string? Mobile { get; set; }
    public string? Email { get; set; }
    public string Biz_mail { get; set; } = string.Empty;
    public string? Address { get; set; }
}