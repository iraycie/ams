﻿namespace OA.Infrastructure.DomainModels;

public class LoginInfo
{
    public string Account { get; set; } = null!;

    public string Password { get; set; } = null!;
}