﻿namespace OA.Infrastructure.DomainModels;

public class WeComPublicUser : WeComResponseBase
{
    public string UserId { get; set; } = null!;
    public string Name { get; set; } = null!;
    public int[] Department { get; set; } = Array.Empty<int>();
    public string Position { get; set; } = null!;
    public int[] Is_leader_in_dept { get; set; } = Array.Empty<int>();
    public string[] Direct_leader { get; set; } = Array.Empty<string>();
    public string Alias { get; set; } = null!;
    public int Status { get; set; }
}