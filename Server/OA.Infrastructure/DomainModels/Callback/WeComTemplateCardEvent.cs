﻿using OA.Infrastructure.WeCom.Abstractions;
using System.Xml.Serialization;

namespace OA.Infrastructure.DomainModels.Callback;

[XmlRoot("xml")]
public class WeComTemplateCardEvent : WeComCallbackEventBase
{
    public string EventKey { get; set; } = null!;

    public string TaskId { get; set; } = null!;

    public string CardType { get; set; } = null!;

    public string ResponseCode { get; set; } = null!;

    [XmlArrayItem("SelectedItem", IsNullable = false)]
    public XmlSelectedItem[] SelectedItems { get; set; } = Array.Empty<XmlSelectedItem>();

    public override async Task<OperationalResult> CallbackAsync()
    {
        var httpContext = App.HttpContext!;

        var handler = httpContext.RequestServices.GetService<IWeComCallbackEventHandler<WeComTemplateCardEvent>>();

        if (handler is not null)
        {
            _ = await handler.HandleAsync(this, httpContext.RequestAborted);
        }

        return _executeResult;
    }
}


[Serializable()]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
public partial class XmlSelectedItem
{
    public string QuestionKey { get; set; } = null!;

    [XmlArrayItem("OptionId", IsNullable = false)]
    public string[] OptionIds { get; set; } = Array.Empty<string>();
}