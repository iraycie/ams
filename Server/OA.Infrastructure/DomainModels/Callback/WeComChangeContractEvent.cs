﻿using System.Xml.Serialization;

namespace OA.Infrastructure.DomainModels.Callback;

[XmlRoot("xml")]
public class WeComChangeContractEvent : WeComCallbackEventBase
{
    public string? ChangeType { get; set; }
    public string UserID { get; set; } = null!;
    public string Department { get; set; } = null!;

    public override Task<OperationalResult> CallbackAsync()
    {
        throw new NotImplementedException("未注册相应的事件处理程序");
    }
}