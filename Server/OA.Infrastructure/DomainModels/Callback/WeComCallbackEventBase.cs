﻿using System.Xml.Serialization;

namespace OA.Infrastructure.DomainModels.Callback;

public abstract class WeComCallbackEventBase : Xml
{
    protected OperationalResult _executeResult;

    protected WeComCallbackEventBase()
    {
        _executeResult = new OperationalResult();
    }

    public virtual Task<OperationalResult> CallbackAsync()
    {
        return Task.FromResult(_executeResult);
    }
}

[Serializable()]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public partial class Xml
{
    private string _operatorUserId = null!;

    public string ToUserName { get; set; } = null!;

    public string FromUserName
    {
        get
        {
            return _operatorUserId;
        }
        set
        {
            _operatorUserId = value;
            SetOperator(_operatorUserId);
        }
    }

    public uint CreateTime { get; set; }

    public string MsgType { get; set; } = null!;

    public string Event { get; set; } = null!;

    public int AgentID { get; set; }

    private static void SetOperator(string value)
    {
        if (App.Metadata.TryGetValue("operator", out _))
        {
            App.Metadata["operator"] = value;
        }
        else
        {
            App.Metadata.Add("operator", value);
        }
    }
}