﻿using System.Xml.Serialization;

namespace OA.Infrastructure.DomainModels.Callback;

[XmlRoot("xml")]
public class WeComTemplateCardMenuEvent : WeComCallbackEventBase
{
    public string EventKey { get; set; } = null!;

    public string TaskId { get; set; } = null!;

    public string CardType { get; set; } = null!;

    public string ResponseCode { get; set; } = null!;

    public override Task<OperationalResult> CallbackAsync()
    {
        throw new NotImplementedException("未注册相应的事件处理程序");
    }
}