﻿namespace OA.Infrastructure.DomainModels.TemporaryAuthorization;

public class TemporaryAuthorizationCodeModel : TemporaryAuthorizationBase
{
    public string Target { get; set; } = null!;

    public string RequireContent { get; set; } = string.Empty;

    public string? Code { get; set; }
}