﻿namespace OA.Infrastructure.DomainModels.TemporaryAuthorization;

public abstract class TemporaryAuthorizationBase
{
    public User Commiter { get; set; } = null!;

    public int Step { get; set; } = 5;

    public TimeUnit Unit { get; set; } = TimeUnit.Minutes;

    public DateTime? GrantDate { get; set; }

    public DateTime? Expiration => CalculateExpiration();

    private DateTime? CalculateExpiration()
    {
        return GrantDate + Unit switch
        {
            TimeUnit.Minutes => TimeSpan.FromMinutes(Step),
            TimeUnit.Hours => TimeSpan.FromHours(Step),
            TimeUnit.Days => TimeSpan.FromDays(Step),
            _ => throw new NotImplementedException(),
        };
    }
}

public enum TimeUnit
{
    Minutes = 1,
    Hours = 2,
    Days = 3,
}