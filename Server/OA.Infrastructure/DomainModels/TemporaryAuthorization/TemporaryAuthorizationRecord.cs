﻿namespace OA.Infrastructure.DomainModels.TemporaryAuthorization;

public record TemporaryAuthorizationRecord
{
    public int Status { get; set; } = 0;

    public string Commiter { get; set; } = null!;

    public string Description { get; set; } = string.Empty;

    public string? Prescription { get; set; }

    public string Key { get; set; } = null!;
}