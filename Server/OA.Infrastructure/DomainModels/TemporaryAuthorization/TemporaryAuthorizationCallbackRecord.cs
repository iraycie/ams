﻿namespace OA.Infrastructure.DomainModels.TemporaryAuthorization;

public class TemporaryAuthorizationCallbackRecord
{
    public int Status { get; set; } = 0;

    public string? Code { get; set; }

    public string Description { get; set; } = string.Empty;
}