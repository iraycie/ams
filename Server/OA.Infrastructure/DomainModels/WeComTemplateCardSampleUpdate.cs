﻿namespace OA.Infrastructure.DomainModels;

public class WeComTemplateCardSampleUpdate
{
    public string[] UserIds { get; set; } = Array.Empty<string>();
    public int[] PartyIds { get; set; } = Array.Empty<int>();
    public int[] TagIds { get; set; } = Array.Empty<int>();
    public int? AtAll { get; set; }
    public int AgentId { get; set; }
    public string Response_code { get; set; } = null!;
    public WeComTemplateCardButtonUpdate Button { get; set; } = null!;
    public int Enable_id_trans { get; set; } = 1;
}

public class WeComTemplateCardButtonUpdate
{
    public string Replace_name { get; set; } = string.Empty;
}