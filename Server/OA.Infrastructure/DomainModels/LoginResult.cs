﻿namespace OA.Infrastructure.DomainModels;

public class LoginResult
{
    public bool Success { get; set; } = false;
    public string AccessToken { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string? Avatar { get; set; }
    public string? QrCode { get; set; }
}