﻿using Microsoft.AspNetCore.Authorization;
using OA.Infrastructure.SignalR.Abstractions;

namespace OA.Infrastructure.Controllers;

[ApiController]
[Authorize(Policy = "ams")]
public class SignalRController : ControllerBase
{
    private readonly ISignalRService _service;

    public SignalRController(ISignalRService service)
    {
        _service = service;
    }

    [HttpPost("api/signalr")]
    public async Task SendAsync(string message)
    {
        await _service.SendAsync(message, HttpContext.RequestAborted);
    }

    [HttpPost("api/signalr/to")]
    public async Task SendToUserAsync(string message, string account)
    {
        await _service.SendToUserAsync(message, account, HttpContext.RequestAborted);
    }

    [HttpPost("api/signalr/togroup")]
    public async Task SendToGroupAsync(string message, string groupName)
    {
        await _service.SendToGroupAsync(message, groupName, HttpContext.RequestAborted);
    }
}