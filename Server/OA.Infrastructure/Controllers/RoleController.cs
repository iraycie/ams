﻿using DynamicAuthorization.Sdk.Attributes;

namespace OA.Infrastructure.Controllers;

[ApiController]
[PermisstionDefinition("角色管理", Route = "/organization/role")]
public class RoleController : ControllerBase
{
    private readonly IRoleService _service;

    public RoleController(IRoleService service)
    {
        _service = service;
    }

    // GET api/role
    [HttpGet("api/role")]
    public Task<ListResult<Role.QueryModel>> GetListAsync()
    {
        return _service.GetListAsync(HttpContext.RequestAborted);
    }

    // GET api/role/page
    [HttpGet("api/role/page")]
    public Task<PageResult<Role.QueryModel>> GetPageAsync([FromQuery] PageParameter input)
    {
        return _service.GetPageAsync(input, HttpContext.RequestAborted);
    }

    // POST api/role
    [HttpPost("api/role")]
    public Task<OperationalResult> CreateAsync(Role.CreateModel role)
    {
        return _service.CreateAsync(role, HttpContext.RequestAborted);
    }

    // PUT api/role/5
    [HttpPut("api/role/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("更新角色")]
    public Task<OperationalResult> UpdateAsync(int id, Role.UpdateModel role)
    {
        return _service.UpdateAsync(id, role, HttpContext.RequestAborted);
    }

    // DELETE api/role/1
    [HttpDelete("api/role/{id}")]
    public Task<OperationalResult> DeleteAsync(int id)
    {
        return _service.DeleteAsync(id, HttpContext.RequestAborted);
    }

    [HttpGet("api/role/api")]
    public async Task<IActionResult> GetApiPermissions()
    {
        return Ok(await _service.GetApiPermisstionsAsync(HttpContext.RequestAborted));
    }

    [HttpGet("api/role/route")]
    public async Task<IActionResult> GetRoutePermissions()
    {
        return Ok(await _service.GetRoutePermissionsAsync(HttpContext.RequestAborted));
    }
}