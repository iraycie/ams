﻿using DynamicAuthorization.Sdk.Attributes;

namespace OA.Infrastructure.Controllers;

[ApiController]
[PermisstionDefinition("系统设置", Route = "/settings/system")]
public class SystemSettingsController : ControllerBase
{
    private readonly ISystemSettingService _service;

    public SystemSettingsController(ISystemSettingService service)
    {
        _service = service;
    }

    // GET api/system-settings
    [HttpGet("api/system-settings")]
    public Task<OperationalResult<SystemSettings.QueryModel>> GetAsync()
    {
        return _service.GetAsync(HttpContext.RequestAborted);
    }

    // PUT api/system-settings
    [HttpPut("api/system-settings")]
    [DynamicAuthorize]
    [PermisstionDefinition("更新系统设置")]
    public Task<OperationalResult> UpdateAsync(SystemSettings.UpdateModel systemSetting)
    {
        return _service.UpdateAsync(systemSetting, HttpContext.RequestAborted);
    }
}