﻿using DynamicAuthorization.Sdk.Attributes;

namespace OA.Infrastructure.Controllers;

[ApiController]
[DynamicAuthorize]
[PermisstionDefinition("系统日志", Route = "/log")]
public class LogController : ControllerBase
{
    private readonly ILogService _service;

    public LogController(ILogService service)
    {
        _service = service;
    }

    // GET api/log/condition-selector
    [HttpGet("api/log/condition-selector")]
    public Task<PageResult<Log.QueryModel>> GetPageAsync([FromQuery] Log.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }
}