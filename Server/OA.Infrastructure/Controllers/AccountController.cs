﻿namespace OA.Infrastructure.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AccountController : ControllerBase
{
    private readonly IAccountService _accountService;

    public AccountController(IAccountService accountService)
    {
        _accountService = accountService;
    }

    [HttpPost]
    public async Task<IActionResult> LoginAsync([FromBody] LoginInfo loginInfo)
    {
        try
        {
            var result = await _accountService.LoginAsync(loginInfo, HttpContext.RequestAborted);

            return result.Status == OperationalResult.SUCCESS ? Ok(result.Data) : Unauthorized();
        }
        catch (Exception ex)
        {
            return Ok($"{ex.Message}");
        }
    }
}