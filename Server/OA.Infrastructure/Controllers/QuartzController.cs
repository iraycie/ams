﻿using DynamicAuthorization.Sdk.Attributes;
using OA.Infrastructure.Job;
using Quartz;

namespace OA.Infrastructure.Controllers;

[ApiController]
[PermisstionDefinition("调度器设置", Route = "/settings/quartz")]
public class QuartzController : ControllerBase
{
    public QuartzController(ISchedulerFactory schedulerFactory)
    {
        var sched = schedulerFactory.GetScheduler().Result;

        var job = JobBuilder.Create<SendNotificationWithData>()
            .WithDescription("")
            .WithIdentity(Guid.NewGuid().ToString("N"))
            .UsingJobData("description", "记得吃早餐哦~")
            .Build();

        var trigger = TriggerBuilder.Create()
            .WithIdentity(Guid.NewGuid().ToString("N"))
            .UsingJobData(new JobDataMap()
            {
                { "TriggedDate", DateTime.UtcNow }
            })
            .StartNow()
            .Build();

        sched.ScheduleJob(job, trigger);
    }

    [HttpGet("api/quartz")]
    public IActionResult Get()
    {
        return Ok();
    }
}