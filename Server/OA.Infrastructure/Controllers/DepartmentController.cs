﻿using DynamicAuthorization.Sdk.Attributes;

namespace OA.Infrastructure.Controllers;

[ApiController]
[PermisstionDefinition("部门管理", Route = "/organization/department")]
public class DepartmentController : ControllerBase
{
    private readonly IDepartmentService _service;

    public DepartmentController(IDepartmentService service)
    {
        _service = service;
    }

    // GET api/department
    [HttpGet("api/department")]
    public Task<ListResult<Department.QueryModel>> GetListAsync()
    {
        return _service.GetListAsync(HttpContext.RequestAborted);
    }

    // GET api/department/condition-selector
    [HttpGet("api/department/condition-selector")]
    public Task<PageResult<Department.QueryModel>> GetPageAsync([FromQuery] Department.ConditionSelectorCondition condition)
    {
        return _service.GetPageAsync(condition, HttpContext.RequestAborted);
    }

    // POST api/department
    [HttpPost("api/department")]
    [DynamicAuthorize]
    [PermisstionDefinition("新增部门")]
    public Task<OperationalResult> CreateAsync(Department.CreateModel department)
    {
        return _service.CreateAsync(department, HttpContext.RequestAborted);
    }

    // PUT api/department/5
    [HttpPut("api/department/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("更新部门")]
    public Task<OperationalResult> UpdateAsync(int id, Department.UpdateModel department)
    {
        return _service.UpdateAsync(id, department, HttpContext.RequestAborted);
    }

    // DELETE api/department/5
    [HttpDelete("api/department/{id}")]
    [DynamicAuthorize]
    [PermisstionDefinition("删除部门")]
    public Task<OperationalResult> DeleteAsync(int id)
    {
        return _service.DeleteAsync(id, HttpContext.RequestAborted);
    }
}