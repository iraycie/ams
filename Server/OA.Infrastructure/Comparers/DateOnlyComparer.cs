﻿namespace OA.Infrastructure.Comparers;

public class DateOnlyComparer : ValueComparer<DateOnly>
{
    public DateOnlyComparer() : base(
        (t1, t2) => t1.Equals(t2),
        t => t.GetHashCode())
    {

    }
}