﻿namespace OA.Infrastructure.Comparers;

public class TimeOnlyComparer : ValueComparer<DateOnly>
{
    public TimeOnlyComparer() : base(
        (t1, t2) => t1.Equals(t2),
        t => t.GetHashCode())
    {

    }
}