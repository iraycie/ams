﻿using System.Security.Claims;

namespace OA.Infrastructure.Authentication.Abstractions;

public interface ITokenHandler
{
    Token CreateToken(ICollection<Claim> claims);
}