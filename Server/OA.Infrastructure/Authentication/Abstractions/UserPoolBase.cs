﻿using System.Security.Claims;

namespace OA.Infrastructure.Authentication.Abstractions;

public abstract class UserPoolBase
{
    public abstract Task<ICollection<Claim>?> ValidateAsync(string account, string password);
}