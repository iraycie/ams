﻿using Microsoft.Extensions.Options;
using System.Net;
using System.Text;

namespace OA.Infrastructure.Authentication;

internal class OAuthEndpointService
{
    private readonly HttpClient _httpClient;

    public OAuthEndpointService(HttpClient client, IOptions<CredentialsOptions> options)
    {
        client.DefaultRequestHeaders.Add("Accept", "*/*");
        client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
        client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

        client.DefaultRequestVersion = HttpVersion.Version10;

        client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(options.Value.ClientID + ":" + options.Value.ClientSecret)));

        _httpClient = client;
    }

    public HttpClient GetHttpClient() => _httpClient;
}