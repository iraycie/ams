﻿namespace OA.Infrastructure.Authentication.Redis;

internal class RedisOptions
{
    public const string Position = "Redis";

    public string Connection { get; set; } = string.Empty;

    public int DefaultDB { get; set; }
}