﻿using Newtonsoft.Json;

namespace OA.Infrastructure.Authentication;

public class Token
{
    [JsonProperty("token_type")]
    public string TokenType { get; set; } = "Bearer";

    [JsonProperty("access_token")]
    public string AccessToken { get; set; } = null!;

    [JsonProperty("refresh_token")]
    public string? RefreshToken { get; set; }

    [JsonProperty("expires_in")]
    public int ExpireIn { get; set; } = 86400;
}