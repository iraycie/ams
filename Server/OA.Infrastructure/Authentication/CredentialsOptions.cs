﻿namespace OA.Infrastructure.Authentication;

internal class CredentialsOptions
{
    public const string Position = "IdentityServer";

    public string ClientID { get; set; } = string.Empty;

    public string ClientSecret { get; set; } = string.Empty;

    public string Scope { get; set; } = string.Empty;

    public string Authority { get; set; } = string.Empty;
}