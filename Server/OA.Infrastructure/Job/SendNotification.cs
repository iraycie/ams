﻿using Microsoft.AspNetCore.SignalR;
using OA.Infrastructure.SignalR.Abstractions;
using OA.Infrastructure.SignalR.Hubs;
using Quartz;

namespace OA.Infrastructure.Job;

[DisallowConcurrentExecution]
public class SendNotification : IJob
{
    private readonly IServiceProvider _serviceProvider;

    public SendNotification(IServiceProvider sp)
    {
        _serviceProvider = sp;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        var service = _serviceProvider.GetRequiredService<ISignalRService>();

        await service.Factory
            .CreateGenericFactory<NotifyHub>()
            .CreateAllProxy()
            .SendAsync("ReceiveNotification", new MessageRecord<string>
            {
                Sender = "System",
                Data = $"当前时间：{DateTime.Now}，休息一会儿吧，看看窗外~"
            }, context.CancellationToken);

        Console.WriteLine($"发送通知 - {DateTime.Now}");
    }
}