﻿using Microsoft.AspNetCore.SignalR;
using OA.Infrastructure.SignalR.Abstractions;
using OA.Infrastructure.SignalR.Hubs;
using Quartz;

namespace OA.Infrastructure.Job;

[DisallowConcurrentExecution]
public class AskForAdminNotification : IJob
{
    private readonly IServiceProvider _serviceProvider;

    public AskForAdminNotification(IServiceProvider sp)
    {
        _serviceProvider = sp;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        var service = _serviceProvider.GetRequiredService<ISignalRService>();

        foreach (var role in App.AdminRoleCodes)
        {
            await service.Factory
            .CreateGenericFactory<NotifyHub>()
            .CreateGroupProxy(role)
            .SendAsync("ReceiveNotification", new MessageRecord<string>
            {
                Sender = "System",
                Data = $"请及时阅读审批条目~"
            }, context.CancellationToken);
        }

        Console.WriteLine($"发送通知 - {DateTime.Now}");
    }
}