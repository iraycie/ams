﻿namespace OA.Infrastructure.Abstractions;

public interface ILogService
{
    Task<PageResult<Log.QueryModel>> GetPageAsync(Log.ConditionSelectorCondition condition, CancellationToken cancellationToken);
}