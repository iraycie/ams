﻿namespace OA.Infrastructure.Abstractions;

public interface IAccountService
{
    Task<OperationalResult<LoginResult>> LoginAsync(LoginInfo loginInfo, CancellationToken cancellationToken = default);
}