﻿using OA.Infrastructure.DomainModels.Message;

namespace OA.Infrastructure.Abstractions;

public interface IWeComService
{
    string AppEntryUrl { get; }

    Task<OperationalResult<string>> GetAccessTokenAsync();

    Task<OperationalResult<WeComUserInfo>> GetUserInfoAsync(string code);

    Task<OperationalResult<WeComPublicUser>> GetUserAsync(string userId);

    Task<OperationalResult<WeComPrivateUser>> SyncUserPrivateInfoAsync(string ticket, CancellationToken cancellationToken);

    Task<ListResult<Department>> GetDepartmentsAsync();

    Task<ListResult<User>> GetDepartmentUsersAsync(Department departmentId);

    Task<OperationalResult<WeComResponseBase>> SendTextMessageAsync(string userId, string content);

    Task<OperationalResult<string>> SendButtonInteractionMessageAsync(string userId, WeComButtonTemplateCard card, string? callbackKey = null);

    Task UpdateSampleTemplateCardAsync(WeComTemplateCardSampleUpdate model);
}