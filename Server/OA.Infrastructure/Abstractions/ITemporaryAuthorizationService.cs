﻿using OA.Infrastructure.DomainModels.TemporaryAuthorization;
using System.Collections.Concurrent;

namespace OA.Infrastructure.Abstractions;

public interface ITemporaryAuthorizationService<TModel>
    where TModel : TemporaryAuthorizationBase
{
    protected static readonly ConcurrentDictionary<string, TModel> s_request = new();

    Task<string> TryAcquireAsync(TModel model, CancellationToken cancellationToken);

    bool TryConsume(string key, string code, out TModel model);

    Task<OperationalResult> GrantAsync(string key, CancellationToken cancellationToken);

    Task<OperationalResult> RejectAsync(string key, CancellationToken cancellationToken);
}