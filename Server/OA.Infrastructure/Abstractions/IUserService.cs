﻿namespace OA.Infrastructure.Abstractions;

public interface IUserService
{
    Task<ListResult<User.QueryModel>> GetListAsync(CancellationToken cancellationToken);

    Task<PageResult<User.QueryModel>> GetPageAsync(User.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(int id, User.UpdateModel user, CancellationToken cancellationToken);

    Task<string?> GetUserGrantsAsync(string account, CancellationToken cancellationToken);

    Task<OperationalResult<User.QueryModel>> GetUserInfoAsync(string account, CancellationToken cancellationToken);

    Task<IEnumerable<string>> GetUserRoutesAsync(string account, CancellationToken cancellationToken);

    Task<IEnumerable<string>> GetRoleCodesAsync(string account, CancellationToken cancellationToken);

    Task<IEnumerable<string>> GetRolesNameAsync(string account, CancellationToken cancellationToken);

    Task LoginAsync(string account, CancellationToken cancellationToken);
}