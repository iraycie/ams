﻿using DynamicAuthorization.Sdk.Model;

namespace OA.Infrastructure.Abstractions;

public interface IRoleService
{
    Task<ListResult<Role.QueryModel>> GetListAsync(CancellationToken cancellationToken);

    Task<PageResult<Role.QueryModel>> GetPageAsync(PageParameter input, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(Role.CreateModel role, CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(int id, Role.UpdateModel role, CancellationToken cancellationToken);

    Task<OperationalResult> DeleteAsync(int id, CancellationToken cancellationToken);

    Task<ListResult<ApiPermissionInfo>> GetApiPermisstionsAsync(CancellationToken cancellationToken);

    Task<ListResult<RoutePermissionInfo>> GetRoutePermissionsAsync(CancellationToken cancellationToken);
}