﻿namespace OA.Infrastructure.Abstractions;

public interface ISystemSettingService
{
    Task<OperationalResult<SystemSettings.QueryModel>> GetAsync(CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(SystemSettings.UpdateModel systemSetting, CancellationToken cancellationToken);
}