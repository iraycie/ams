﻿namespace OA.Infrastructure.Abstractions;

public interface IDepartmentService
{
    Task<ListResult<Department.QueryModel>> GetListAsync(CancellationToken cancellationToken);

    Task<PageResult<Department.QueryModel>> GetPageAsync(Department.ConditionSelectorCondition condition, CancellationToken cancellationToken);

    Task<OperationalResult> CreateAsync(Department.CreateModel department, CancellationToken cancellationToken);

    Task<OperationalResult> UpdateAsync(int id, Department.UpdateModel department, CancellationToken cancellationToken);

    Task<OperationalResult> DeleteAsync(int id, CancellationToken cancellationToken);
}