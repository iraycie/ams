﻿namespace OA.Infrastructure.Logger.Abstractions;

public interface ILoggerOptions
{
    bool Enable { get; set; }

    bool SaveUnChangedProperties { get; set; }

    IReadOnlyCollection<Func<EntityEntry, bool>> EntityFilters { get; set; }

    IReadOnlyCollection<Func<EntityEntry, PropertyEntry, bool>> PropertyFilters { get; set; }
}