﻿using OA.Infrastructure.Enums;

namespace OA.Infrastructure.Logger.Abstractions;

public interface ITrackerEntry
{
    Dictionary<string, string> Tables { get; }

    OperationCategory OperationCategory { get; }

    Dictionary<string, Dictionary<string, object?>?> OriginalValues { get; }

    Dictionary<string, Dictionary<string, object?>?> CurrentValues { get; }

    Dictionary<string, Dictionary<string, object?>> KeyValues { get; }

    Dictionary<string, Dictionary<string, object?>> Fields { get; }

    Dictionary<string, List<PropertyEntry>?> TemporaryProperties { get; set; }
}