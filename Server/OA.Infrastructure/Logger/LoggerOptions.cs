﻿using OA.Infrastructure.Logger.Abstractions;

namespace OA.Infrastructure.Logger;

public sealed class LoggerOptions : ILoggerOptions
{
    public bool Enable { get; set; } = true;
    public bool SaveUnChangedProperties { get; set; } = false;
    public IReadOnlyCollection<Func<EntityEntry, bool>> EntityFilters { get; set; } = null!;
    public IReadOnlyCollection<Func<EntityEntry, PropertyEntry, bool>> PropertyFilters { get; set; } = null!;

    public LoggerOptions()
    {
        EntityFilters = new HashSet<Func<EntityEntry, bool>>()
        {
            n => n.Metadata.GetTableName()! != "Log"
        };

        PropertyFilters = new HashSet<Func<EntityEntry, PropertyEntry, bool>>()
        {
            (x, y) =>
            {
                if (x.Metadata.GetTableName()! == "User")
                {
                    var t = y.GetColumnName();
                    if (y.GetColumnName() == "LastVisitedDate")
                    {
                        return false;
                    }
                }

                return true;
            }
        };
    }
}