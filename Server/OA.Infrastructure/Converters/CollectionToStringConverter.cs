﻿namespace OA.Infrastructure.Converters;

public class CollectionToStringConverter<TModel> : ValueConverter<TModel, string>
    where TModel : IEnumerable
{
    public CollectionToStringConverter() : base(
        v => JsonSerializer.Serialize(v, (JsonSerializerOptions?)null),
        v => string.IsNullOrWhiteSpace(v) ? default! : JsonSerializer.Deserialize<TModel>(v, (JsonSerializerOptions?)null)!)
    {

    }
}