﻿namespace OA.Infrastructure.Converters;

public class ObjectToStringConverter<TModel> : ValueConverter<TModel, string>
{
    public ObjectToStringConverter() : base(
        v => JsonSerializer.Serialize(v, (JsonSerializerOptions?)null),
        v => string.IsNullOrWhiteSpace(v) ? default! : JsonSerializer.Deserialize<TModel>(v, (JsonSerializerOptions?)null)!)
    {

    }
}