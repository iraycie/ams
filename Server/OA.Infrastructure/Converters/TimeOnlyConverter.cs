﻿namespace OA.Infrastructure.Converters;

public class TimeOnlyConverter : ValueConverter<TimeOnly, TimeSpan>
{
    public TimeOnlyConverter() : base(
            timeonly => timeonly.ToTimeSpan(),
            timespan => TimeOnly.FromTimeSpan(timespan))
    {

    }
}