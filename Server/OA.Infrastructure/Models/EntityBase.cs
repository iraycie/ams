﻿namespace OA.Infrastructure.Models;

/// <summary>
/// 实体类的基类
/// </summary>
[DisplayName("实体类的基类")]
public partial class EntityBase
{
    /// <summary>
    /// 编号
    /// </summary>
    [DisplayName("编号")]
    [Key]
    public int Id { get; set; }

    /// <summary>
    /// 创建日期
    /// </summary>
    [DisplayName("创建日期")]
    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;

    /// <summary>
    /// 最近修改日期
    /// </summary>
    [DisplayName("最近修改日期")]
    public DateTime LastModifiedDate { get; set; } = DateTime.UtcNow;

    /// <summary>
    /// 是否软删除
    /// </summary>
    [DisplayName("是否软删除")]
    public bool IsDeleted { get; set; } = false;
}