﻿using OA.Infrastructure.Enums;

namespace OA.Infrastructure.Models;

/// <summary>
/// 日志
/// </summary>
[DisplayName("日志")]
public partial class Log : EntityBase

{
    /// <summary>
    /// 操作者
    /// </summary>
    [DisplayName("操作者")]
    public User Operator { get; set; } = null!;

    /// <summary>
    /// 操作类型
    /// </summary>
    [DisplayName("操作类型")]
    public OperationCategory Operation { get; set; }

    /// <summary>
    /// 表单ID
    /// </summary>
    [DisplayName("表单ID")]
    public int FormId { get; set; }

    /// <summary>
    /// 表单ID
    /// </summary>
    [DisplayName("表单类型")]
    public string FormType { get; set; } = string.Empty;

    /// <summary>
    /// 表单ID
    /// </summary>
    [DisplayName("字段")]
    [MaxLength(32)]
    public string Field { get; set; } = string.Empty;

    /// <summary>
    /// 旧值
    /// </summary>
    [DisplayName("旧值")]
    public string Old { get; set; } = string.Empty;

    /// <summary>
    /// 新值
    /// </summary>
    [DisplayName("新值")]
    public string New { get; set; } = string.Empty;
}