﻿namespace OA.Infrastructure.Models;

public partial class Department
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        [DisplayName("部门名称")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 负责人
        /// </summary>
        [DisplayName("负责人")]
        public User.QueryModel? Leader { get; set; }

        /// <summary>
        /// 上级部门
        /// </summary>
        [DisplayName("上级部门")]
        public QueryModel? Parent { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        [DisplayName("角色")]
        public IEnumerable<Role.QueryModel> Roles { get; set; } = Enumerable.Empty<Role.QueryModel>();
    }
}