﻿using OA.Infrastructure.Enums;

namespace OA.Infrastructure.Models;

public partial class User
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 帐号
        /// </summary>
        [DisplayName("帐号")]
        public string Account { get; set; } = string.Empty;

        /// <summary>
        /// 工号
        /// </summary>
        [DisplayName("工号")]
        public string JobNumber { get; set; } = string.Empty;

        /// <summary>
        /// 企业微信用户Id
        /// </summary>
        [DisplayName("企业微信用户Id")]
        public string UserId { get; set; } = string.Empty;

        /// <summary>
        /// 姓名
        /// </summary>
        [DisplayName("姓名")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 手机号
        /// </summary>
        [DisplayName("手机号")]
        public string? Mobile { get; set; }

        /// <summary>
        /// 部门
        /// </summary>
        [DisplayName("部门")]
        public Department.QueryModel? Department { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [DisplayName("性别")]
        public Gender Gender { get; set; } = Gender.Undefined;

        /// <summary>
        /// 邮箱地址
        /// </summary>
        [DisplayName("邮箱地址")]
        public string? Email { get; set; }

        /// <summary>
        /// 是否为负责人
        /// </summary>
        [DisplayName("是否为负责人")]
        public bool IsLeader { get; set; } = false;

        /// <summary>
        /// 直属上级
        /// </summary>
        [DisplayName("直属上级")]
        public QueryModel? DirectLeader { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [DisplayName("头像")]
        public string? Avatar { get; set; }

        /// <summary>
        /// 个人二维码
        /// </summary>
        [DisplayName("个人二维码")]
        public string? QrCode { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        [DisplayName("地址")]
        public string? Address { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        [DisplayName("角色")]
        public IEnumerable<Role.QueryModel> Roles { get; set; } = Enumerable.Empty<Role.QueryModel>();
    }
}
