﻿namespace OA.Infrastructure.Models;

public partial class EntityBase
{
    public class QueryModel
    {
        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName("编号")]
        public int Id { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DisplayName("创建日期")]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 最近修改日期
        /// </summary>
        [DisplayName("最近修改日期")]
        public DateTime LastModifiedDate { get; set; }
    }
}