﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OA.Infrastructure.Models;

/// <summary>
/// 角色
/// </summary>
[DisplayName("角色")]
public partial class Role : EntityBase
{
    /// <summary>
    /// 角色码
    /// </summary>
    [DisplayName("角色码")]
    [MaxLength(32)]
    public string RoleCode { get; set; } = string.Empty;

    /// <summary>
    /// 角色名称
    /// </summary>
    [DisplayName("角色名称")]
    [MaxLength(48)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// API权限
    /// </summary>
    [DisplayName("API权限")]
    public List<string> Types { get; set; } = new();

    /// <summary>
    /// 路由权限
    /// </summary>
    [DisplayName("路由权限")]
    public List<string> ViewRoutes { get; set; } = new();

    /// <summary>
    /// 拥有该角色的用户
    /// </summary>
    [DisplayName("拥有该角色的用户")]
    [InverseProperty(nameof(User.Roles))]
    public ICollection<User> OwnUsers { get; set; } = new HashSet<User>();

    /// <summary>
    /// 拥有该角色的部门
    /// </summary>
    [DisplayName("拥有该角色的部门")]
    [InverseProperty(nameof(Department.Roles))]
    public ICollection<Department> OwnDepartments { get; set; } = new HashSet<Department>();
}