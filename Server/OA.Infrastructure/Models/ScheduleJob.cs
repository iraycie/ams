﻿using OA.Infrastructure.Enums;

namespace OA.Infrastructure.Models;

/// <summary>
/// 计划作业
/// </summary>
[DisplayName("计划作业")]
public partial class ScheduleJob : EntityBase
{
    /// <summary>
    /// 作业Id
    /// </summary>
    [DisplayName("作业编号")]
    [MaxLength(32)]
    public string JobId { get; set; } = null!;

    /// <summary>
    /// 任务表单的主键
    /// </summary>
    [DisplayName("Key")]
    [MaxLength(32)]
    public string Key { get; set; } = string.Empty;

    /// <summary>
    /// 任务状态
    /// </summary>
    [DisplayName("状态")]
    public JobStatus Status { get; set; } = JobStatus.Active;
}