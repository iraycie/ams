﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OA.Infrastructure.Models;

/// <summary>
/// 部门
/// </summary>
[DisplayName("部门")]
public partial class Department : EntityBase

{
    /// <summary>
    /// 部门名称
    /// </summary>
    [DisplayName("部门名称")]
    [MaxLength(32)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 负责人
    /// </summary>
    [DisplayName("负责人")]
    public User? Leader { get; set; }

    /// <summary>
    /// 上级部门
    /// </summary>
    [DisplayName("上级部门")]
    public Department? Parent { get; set; }

    /// <summary>
    /// 角色
    /// </summary>
    [DisplayName("角色")]
    [InverseProperty(nameof(Role.OwnDepartments))]
    public ICollection<Role> Roles { get; set; } = new HashSet<Role>();

    [DisplayName("负责人Id")]
    public int? LeaderId { get; set; }
}