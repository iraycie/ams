﻿namespace OA.Infrastructure.Models;

public partial class SystemSettings
{
    public class UpdateModel
    {
        /// <summary>
        /// 勾选该设置，则相应的业务处理流会通知到企业微信终端用户。
        /// </summary>
        [DisplayName("发送消息")]
        public bool SendMessage { get; set; } = false;

        /// <summary>
        /// 勾选该设置，则拟态控制台将被启用
        /// </summary>
        [DisplayName("显示控制台")]
        public bool ShowConsole { get; set; } = false;

        /// <summary>
        /// 系统管理员能否审批
        /// </summary>
        [DisplayName("系统管理员能否审批")]
        public bool SystemAdminCanApprove { get; set; } = false;
    }
}