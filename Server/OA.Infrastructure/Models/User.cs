﻿using OA.Infrastructure.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace OA.Infrastructure.Models;

/// <summary>
/// 用户
/// </summary>
[DisplayName("用户")]
public partial class User : EntityBase
{
    /// <summary>
    /// 帐号
    /// </summary>
    [DisplayName("帐号")]
    [MaxLength(8)]
    public string Account { get; set; } = string.Empty;

    /// <summary>
    /// 工号
    /// </summary>
    [DisplayName("工号")]
    [MaxLength(8)]
    public string JobNumber { get; set; } = string.Empty;

    /// <summary>
    /// 企业微信用户Id
    /// </summary>
    [DisplayName("企业微信用户Id")]
    public string UserId { get; set; } = string.Empty;

    /// <summary>
    /// 企业微信用户票据
    /// </summary>
    [DisplayName("企业微信用户票据")]
    public string? UserTicket { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    [DisplayName("姓名")]
    [MaxLength(32)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 密码
    /// </summary>
    [DisplayName("密码")]
    [MaxLength(128)]
    public string Password { get; set; } = string.Empty;

    /// <summary>
    /// 手机号
    /// </summary>
    [DisplayName("手机号")]
    [RegularExpression(@"^((\+?86)|(\(\+86\)))?((((13[^4]{1})|(14[5-9]{1})|147|(15[^4]{1})|166|(17\d{1})|(18\d{1})|(19[12589]{1}))\d{8})|((134[^9]{1}|1410|1440)\d{7}))$", ErrorMessage = "请输入正确的手机号。")]
    [MaxLength(11, ErrorMessage = "手机号长度错误。")]
    public string? Mobile { get; set; }

    /// <summary>
    /// 部门
    /// </summary>
    [DisplayName("部门")]
    public Department? Department { get; set; }

    /// <summary>
    /// 性别
    /// </summary>
    [DisplayName("性别")]
    public Gender Gender { get; set; } = Gender.Undefined;

    /// <summary>
    /// 邮箱地址
    /// </summary>
    [DisplayName("邮箱地址")]
    [RegularExpression(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "邮箱格式不正确。")]
    public string? Email { get; set; }

    /// <summary>
    /// 企业邮箱
    /// </summary>
    [DisplayName("企业邮箱")]
    [RegularExpression(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "邮箱格式不正确。")]
    public string? BizMail { get; set; }

    /// <summary>
    /// 是否为负责人
    /// </summary>
    [DisplayName("是否为负责人")]
    public bool IsLeader { get; set; } = false;

    /// <summary>
    /// 直属上级
    /// </summary>
    [DisplayName("直属上级")]
    public User? DirectLeader { get; set; }

    /// <summary>
    /// 头像
    /// </summary>
    [DisplayName("头像")]
    public string? Avatar { get; set; }

    /// <summary>
    /// 个人二维码
    /// </summary>
    [DisplayName("个人二维码")]
    public string? QrCode { get; set; }

    /// <summary>
    /// 地址
    /// </summary>
    [DisplayName("地址")]
    public string? Address { get; set; }

    /// <summary>
    /// 角色
    /// </summary>
    [DisplayName("角色")]
    [InverseProperty(nameof(Role.OwnUsers))]
    public ICollection<Role> Roles { get; set; } = new HashSet<Role>();

    [DisplayName("所属部门Id")]
    public int? DepartmentId { get; set; }

    /// <summary>
    /// 上一次访问日期
    /// </summary>
    [DisplayName("上一次访问日期")]
    public DateTime LastVisitedDate { get; set; } = DateTime.UtcNow;
}