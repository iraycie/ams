﻿namespace OA.Infrastructure.Models;

public partial class Department
{
    public class UpdateModel
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        [DisplayName("部门名称")]
        [MaxLength(32)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 负责人
        /// </summary>
        [DisplayName("负责人")]
        public int? LeaderId { get; set; }

        /// <summary>
        /// 上级部门
        /// </summary>
        [DisplayName("上级部门")]
        public int? ParentId { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        [DisplayName("角色")]
        public List<int> RolesId { get; set; } = new();
    }
}