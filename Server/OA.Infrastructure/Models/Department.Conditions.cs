﻿namespace OA.Infrastructure.Models;

public partial class Department
{
    [DisplayName("用户组查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("角色")]
        public int? SelectedRoleId { get; set; }
    }
}