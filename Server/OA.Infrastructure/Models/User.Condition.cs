﻿using OA.Infrastructure.Enums;

namespace OA.Infrastructure.Models;

public partial class User
{
    [DisplayName("条件查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("关键词")]
        public string? Keywords { get; set; }

        [DisplayName("部门")]
        public int? DeptId { get; set; }

        [DisplayName("性别")]
        public Gender? Gender { get; set; }
    }
}