﻿namespace OA.Infrastructure.Models;

public partial class Role
{
    public new class QueryModel : EntityBase.QueryModel
    {
        /// <summary>
        /// 角色码
        /// </summary>
        [DisplayName("角色码")]
        public string RoleCode { get; set; } = string.Empty;

        /// <summary>
        /// 角色名称
        /// </summary>
        [DisplayName("角色名称")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// API权限
        /// </summary>
        [DisplayName("API权限")]
        public IEnumerable<string> Types { get; set; } = Enumerable.Empty<string>();

        /// <summary>
        /// 路由权限
        /// </summary>
        [DisplayName("路由权限")]
        public IEnumerable<string> ViewRoutes { get; set; } = Enumerable.Empty<string>();

        public IEnumerable<string> ApiPermissions { get; set; } = Enumerable.Empty<string>();

        public IEnumerable<string> RoutePermissions { get; set; } = Enumerable.Empty<string>();
    }
}