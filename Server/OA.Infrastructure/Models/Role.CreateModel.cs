﻿namespace OA.Infrastructure.Models;

public partial class Role
{
    public class CreateModel
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        [DisplayName("角色名称")]
        [MaxLength(32)]
        public string Name { get; set; } = string.Empty;

        public IEnumerable<string> ApiPermissions { get; set; } = Enumerable.Empty<string>();

        public IEnumerable<string> RoutePermissions { get; set; } = Enumerable.Empty<string>();
    }
}