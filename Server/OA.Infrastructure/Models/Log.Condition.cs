﻿using OA.Infrastructure.Enums;

namespace OA.Infrastructure.Models;

public partial class Log
{
    [DisplayName("条件查询")]
    public class ConditionSelectorCondition : PageParameter
    {
        [DisplayName("操作类型")]
        public OperationCategory? LogOperation { get; set; }

        [DisplayName("起始日期")]
        public DateTime? CreateDateSelectorBeginDate { get; set; }

        [DisplayName("截止日期")]
        public DateTime? CreateDateSelectorEndDate { get; set; }
    }

    [DisplayName("日期范围")]
    public class CreateDateSelectorCondition : PageParameter
    {
        [DisplayName("起始日期")]
        public DateTime? BeginDate { get; set; }

        [DisplayName("截止日期")]
        public DateTime? EndDate { get; set; }
    }
}