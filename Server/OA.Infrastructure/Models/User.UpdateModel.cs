﻿using OA.Infrastructure.Enums;

namespace OA.Infrastructure.Models;

public partial class User
{
    public class UpdateModel
    {
        /// <summary>
        /// 姓名
        /// </summary>
        [DisplayName("姓名")]
        [MaxLength(32)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 手机号
        /// </summary>
        [DisplayName("手机号")]
        [MaxLength(11, ErrorMessage = "手机号长度错误。")]
        public string? Mobile { get; set; }

        /// <summary>
        /// 部门
        /// </summary>
        [DisplayName("部门")]
        public int? DepartmentId { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [DisplayName("性别")]
        public Gender Gender { get; set; } = Gender.Undefined;

        /// <summary>
        /// 邮箱地址
        /// </summary>
        [DisplayName("邮箱地址")]
        public string? Email { get; set; }

        /// <summary>
        /// 是否为负责人
        /// </summary>
        [DisplayName("是否为负责人")]
        public bool IsLeader { get; set; } = false;

        /// <summary>
        /// 直属上级
        /// </summary>
        [DisplayName("直属上级")]
        public int? DirectLeaderId { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [DisplayName("头像")]
        public string? Avatar { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        [DisplayName("地址")]
        public string? Address { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        [DisplayName("角色")]
        public List<int> RolesId { get; set; } = new();
    }
}