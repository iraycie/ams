﻿using DynamicAuthorization.Sdk.Utils;
using System.Reflection;

namespace System;

public static class ServiceProviderExtensions
{
    public static IServiceProvider Initialize(this IServiceProvider provider, Assembly assembly)
    {
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        App.Services = provider;
        App.Assembly = assembly;

        var managedByDocker = Environment.GetEnvironmentVariable("MANAGED_BY_DOCKER");

        if (!string.IsNullOrEmpty(managedByDocker) && managedByDocker == "TRUE")
        {
            App.ManagedByDocker = true;
        }

        PermissionSniffer.Initialize(App.Assembly);

        return provider;
    }
}