﻿using OA.Infrastructure.Impl;

namespace OA.Infrastructure.Extensions;

public static class DefinitionServiceExtensions
{
    public static User.QueryModel GetUserQueryModel(this User user)
    {
        return UserService.GetQueryModel(user);
    }

    public static Department.QueryModel GetDepartmentQueryModel(this Department department)
    {
        return DepartmentService.GetQueryModel(department);
    }

    public static Role.QueryModel GetRoleQueryModel(this Role role)
    {
        return RoleService.GetQueryModel(role);
    }
}