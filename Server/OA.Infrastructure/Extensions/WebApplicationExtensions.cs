﻿using OA.Infrastructure.SignalR;
using OA.Infrastructure.SignalR.Hubs;

namespace Microsoft.AspNetCore.Builder;

public static class WebApplicationExtensions
{
    public static WebApplication UseOfficeAutomation(this WebApplication app, string authenticatePolicyNames)
    {
        app.UseCors("__signalRCors");
        //app.UseCloudIdentityServer();
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseSignalR(authenticatePolicyNames);

        return app;
    }

    internal static WebApplication UseSignalR(this WebApplication app, string authenticatePolicyNames)
    {
        app.MapHub<NotifyHub>(SignalRConstants.Path, opts =>
        {
            opts.Transports = Http.Connections.HttpTransportType.WebSockets;
            opts.CloseOnAuthenticationExpiration = true;
        })
        .RequireCors("__signalRCors")
        .RequireAuthorization(authenticatePolicyNames);

        return app;
    }
}