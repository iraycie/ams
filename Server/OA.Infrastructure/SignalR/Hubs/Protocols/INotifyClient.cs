﻿namespace OA.Infrastructure.SignalR.Hubs.Protocols;

internal interface INotifyClient
{
    Task SendNotificationAsync(string methodName, string message, CancellationToken cancellationToken = default);
}