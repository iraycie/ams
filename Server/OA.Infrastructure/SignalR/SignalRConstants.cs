﻿namespace OA.Infrastructure.SignalR;

internal static class SignalRConstants
{
    public const string Path = "/api/notify";
}