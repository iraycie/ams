﻿using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;

namespace OA.Infrastructure.SignalR;

internal class UserIdentityProvider : IUserIdProvider
{
    public virtual string? GetUserId(HubConnectionContext connection)
    {
        return connection.User.FindFirstValue(ClaimTypes.Name);
    }
}