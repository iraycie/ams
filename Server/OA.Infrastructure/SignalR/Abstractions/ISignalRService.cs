﻿namespace OA.Infrastructure.SignalR.Abstractions;

public interface ISignalRService
{
    IClientProxyFactory Factory { get; }

    Task SendAsync(string message, CancellationToken cancellationToken);

    Task SendToUserAsync(string message, string username, CancellationToken cancellationToken);

    Task SendToGroupAsync(string message, string groupName, CancellationToken cancellationToken);

    Task SendAsync(object data, string method, MessageLevel messageLevel = MessageLevel.Information, CancellationToken cancellationToken = default);
}