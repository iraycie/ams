﻿using Microsoft.AspNetCore.SignalR;

namespace OA.Infrastructure.SignalR.Abstractions;

public interface IClientProxyFactory
{
    IClientProxyFactory<THub> CreateGenericFactory<THub>() where THub : Hub;
}

public interface IClientProxyFactory<THub>
    where THub : Hub
{
    IGroupManager GroupManager();

    IClientProxy CreateAllProxy();

    IClientProxy CreateGroupProxy(string groupName);

    IClientProxy CreateUserProxy(string account);
}