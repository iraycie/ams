import { AppRouteRecordRaw } from '../../types'

import { LAYOUT } from '@/router/constant';

const organizationRouter: AppRouteRecordRaw = {
  path: '/organization',
  name: 'Organization',
  component: LAYOUT,
  redirect: '/organization/department',
  meta: {
    icon: 'CopyOutlined',
    title: '组织架构管理',
    rank: 11,
    tag: 'perms'
  },
  children: [
    {
      path: '/organization/department',
      name: 'department',
      component: () => import('@/views/department/index.vue'),
      meta: {
        title: '部门',
        icon: 'ApartmentOutlined',
        showParent: false,
        tag: 'perms'
      }
    },
    {
      path: '/organization/user',
      name: 'user',
      component: () => import('@/views/user/index.vue'),
      meta: {
        title: '用户',
        icon: 'UserOutlined',
        tag: 'perms'
      }
    },
    {
      path: '/organization/role',
      name: 'role',
      component: () => import('@/views/role/index.vue'),
      meta: {
        title: '角色',
        icon: 'ApiOutlined',
        tag: 'perms'
      }
    },
  ]
}

export default organizationRouter