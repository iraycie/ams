﻿import type { AppRouteRecordRaw } from '../../types'
import { LAYOUT } from '@/router/constant';

const approvalRouter: AppRouteRecordRaw = {
  path: '/approval',
  name: 'Approval',
  component: LAYOUT,
  redirect: '/approval/list',
  meta: {
    icon: 'BellOutlined',
    title: "审批",
    rank: 4,
    tag: 'perms'
  },
  children: [
    {
      path: '/approval/list',
      name: 'approval',
      component: () => import('@/views/approval/index.vue'),
      meta: {
        icon: 'BellOutlined',
        title: '审批',
        tag: 'perms'
      }
    }
  ]
};

export default approvalRouter;