﻿import type { AppRouteRecordRaw } from '../../types'
const Layout = () => import('@/layout/index.vue')

const maintenanceRecordRouter: AppRouteRecordRaw = {
  path: '/maintenance-record',
  name: 'MaintenanceRecord',
  component: Layout,
  redirect: '/maintenance-record/list',
  meta: {
    icon: 'home-filled',
    title: "维修记录",
    rank: 10
  },
  children: [
    {
      path: '/maintenance-record/list',
      name: 'maintenance-record',
      component: () => import('@/views/maintenance-record/index.vue'),
      meta: {
        icon: 'ToolOutlined',
        title: '维修记录'
      }
    }
  ]
};

export default maintenanceRecordRouter;