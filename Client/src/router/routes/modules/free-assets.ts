import { AppRouteRecordRaw } from '../../types'

import { LAYOUT } from '@/router/constant';

const freeAssetsRouter: AppRouteRecordRaw = {
  path: '/free-assets',
  name: 'FreeAssets',
  component: LAYOUT,
  redirect: '/free-assets/repo',
  meta: {
    icon: 'CoffeeOutlined',
    title: '易耗品资产管理',
    rank: 3,
    tag: 'perms'
  },
  children: [
    {
      path: '/free-assets/storage',
      name: 'free-assets-storage',
      component: () => import('@/views/free-assets-storage/index.vue'),
      meta: {
        title: '易耗品资产入库',
        icon: 'PlusOutlined',
        tag: 'perms'
      }
    },
    {
      path: '/free-assets/repo',
      name: 'free-assets-repo',
      component: () => import('@/views/free-assets-repo/index.vue'),
      meta: {
        title: '易耗品资产库存',
        icon: 'ShopOutlined',
        showParent: false,
        tag: 'perms'
      }
    },
    {
      path: '/free-assets/consuming',
      name: 'free-assets-consuming',
      component: () => import('@/views/free-assets-consuming/index.vue'),
      meta: {
        title: '易耗品资产领用',
        icon: 'BlockOutlined',
        tag: 'perms'
      }
    },    
  ]
}

export default freeAssetsRouter