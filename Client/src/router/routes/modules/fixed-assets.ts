import { AppRouteRecordRaw } from '../../types'

import { LAYOUT } from '@/router/constant';

const freeAssetsRouter: AppRouteRecordRaw = {
  path: '/fixed-assets',
  name: 'FixedAssets',
  component: LAYOUT,
  redirect: '/fixed-assets/repo',
  meta: {
    icon: 'DesktopOutlined',
    title: '固定资产管理',
    rank: 2,
    tag: 'perms'
  },
  children: [
    {
      path: '/fixed-assets/storage',
      name: 'fixed-assets-storage',
      component: () => import('@/views/fixed-assets-storage/index.vue'),
      meta: {
        title: '固定资产入库',
        icon: 'PlusOutlined',
        tag: 'perms'
      }
    },
    {
      path: '/fixed-assets/repo',
      name: 'fixed-assets-repo',
      component: () => import('@/views/fixed-assets-repo/index.vue'),
      meta: {
        title: '固定资产库存',
        icon: 'ShopOutlined',
        showParent: false,
        tag: 'perms'
      }
    },
    {
      path: '/fixed-assets/consuming',
      name: 'fixed-assets-consuming',
      component: () => import('@/views/fixed-assets-consuming/index.vue'),
      meta: {
        title: '固定资产领用',
        icon: 'BlockOutlined',
        tag: 'perms'
      }
    },
    {
      path: '/fixed-assets/loss',
      name: 'fixed-assets-loss',
      component: () => import('@/views/fixed-assets-loss/index.vue'),
      meta: {
        title: '固定资产报损',
        icon: 'ToolOutlined',
        tag: 'perms'
      }
    }    
  ]
}

export default freeAssetsRouter