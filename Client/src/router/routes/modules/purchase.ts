﻿import type { AppRouteRecordRaw } from '../../types'
import { LAYOUT } from '@/router/constant';

const purchaseRouter: AppRouteRecordRaw = {
  path: '/purchase',
  name: 'Purchase',
  component: LAYOUT,
  redirect: '/purchase/list',
  meta: {
    icon: 'home-filled',
    title: "资产采购",
    rank: 1,
    tag: 'perms'
  },
  children: [
    {
      path: '/purchase/list',
      name: 'purchase',
      component: () => import('@/views/purchase/index.vue'),
      meta: {
        icon: 'ShoppingOutlined',
        title: '资产采购',
        tag: 'perms'
      }
    }
  ]
};

export default purchaseRouter;