﻿import type { AppRouteRecordRaw } from '../../types'
import { LAYOUT } from '@/router/constant';

const logRouter: AppRouteRecordRaw = {
  path: '/log',
  name: 'Log',
  component: LAYOUT,
  redirect: '/log/list',
  meta: {
    icon: 'home-filled',
    title: "日志",
    rank: 12,
    tag: 'perms'
  },
  children: [
    {
      path: '/log/list',
      name: 'log',
      component: () => import('@/views/log/index.vue'),
      meta: {
        icon: 'BookOutlined',
        title: '日志',
        tag: 'perms',
        refreshRedirect: '/log'
      }
    }
  ]
};

export default logRouter;