import type { AppRouteRecordRaw } from '../../types'
import { LAYOUT } from '@/router/constant';

const settingsRouter: AppRouteRecordRaw = {
  path: '/settings',
  name: 'Settings',
  component: LAYOUT,
  redirect: '/settings/system',
  meta: {
    icon: 'DeploymentUnitOutlined',
    title: "基础设置",
    rank: 99,
    tag: 'perms'
  },
  children: [
    {
      path: '/settings/system',
      name: 'settings-system',
      component: () => import('@/views/system-settings/index.vue'),
      meta: {
        icon: 'SettingOutlined',
        title: '系统设置',
        tag: 'perms'
      }
    },
    {
      path: '/settings/assets-category',
      name: 'settings-assets-category',
      component: () => import('@/views/assets-category/index.vue'),
      meta: {
        icon: 'CalculatorOutlined',
        title: '资产类别管理',
        tag: 'perms'
      }
    },
    {
      path: '/settings/supplier',
      name: 'settings-supplier',
      component: () => import('@/views/supplier/index.vue'),
      meta: {
        icon: 'ShopOutlined',
        title: '供应商管理',
        tag: 'perms'
      }
    },
    {
      path: '/settings/approver',
      name: 'settings-approver',
      component: () => import('@/views/approver-config/index.vue'),
      meta: {
        icon: 'UserSwitchOutlined',
        title: '审批流程管理',
        tag: 'perms'
      }
    }
  ]
};

export default settingsRouter;