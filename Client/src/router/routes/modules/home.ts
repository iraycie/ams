import { AppRouteRecordRaw } from '../../types'

import { LAYOUT } from '@/router/constant';

const homeRouter: AppRouteRecordRaw = {
  path: '/home',
  name: 'Home',
  component: LAYOUT,
  redirect: '/home/dashboard',
  meta: {
    icon: 'home-filled',
    title: 'home',
    rank: 0
  },
  children: [
    {
      path: '/home/dashboard',
      name: 'Welcome',
      component: () => import('@/views/home.vue'),
      meta: {
        title: 'Home',
        icon: 'home-filled',
        refreshRedirect: '/home'
      }
    }
  ]
}

export default homeRouter
