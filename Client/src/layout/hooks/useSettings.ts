import { computed } from 'vue'
import { useSettingsStoreWithOut } from '@/store/modules/layout'
import { ISettingsInfo } from '@/types/settings'

export function useSettings() {
  const settingsStore = useSettingsStoreWithOut()

  const setSettings = (info: Partial<ISettingsInfo>) => {
    settingsStore.setSettings(info)
  }

  const getLayoutMode = computed(() => settingsStore.getLayoutMode)
  const getDarkMode = computed(() => settingsStore.getDarkMode)
  const getSystemThemeColor = computed(() => settingsStore.getSystemThemeColor)
  const getHeaderColor = computed(() => settingsStore.getHeaderColor)
  const getSiderColor = computed(() => settingsStore.getSiderColor)

  const getIsShowFooter = computed(() => settingsStore.getIsShowFooter)
  const getIsShowHeader = computed(() => settingsStore.getIsShowHeader)
  const getIsShowSider = computed(() => settingsStore.getIsShowSider)
  const getIsShowLogo = computed(() => settingsStore.getIsShowLogo)

  const getContentWidthMode = computed(() => settingsStore.getContentWidthMode)
  const getFixedHeader = computed(() => settingsStore.getFixedHeader)
  const getFixedSider = computed(() => settingsStore.getFixedSider)
  const getAutoSplice = computed(() => settingsStore.getAutoSplice)
  const getGrayMode = computed(() => settingsStore.getGrayMode)
  const getColorWeak = computed(() => settingsStore.getColorWeak)
  const getCollapsed = computed(() => settingsStore.getCollapsed)

  return {
    setSettings,
    getLayoutMode,
    getDarkMode,
    getSystemThemeColor,
    getHeaderColor,
    getSiderColor,
    getIsShowFooter,
    getIsShowHeader,
    getIsShowSider,
    getIsShowLogo,
    getContentWidthMode,
    getFixedHeader,
    getFixedSider,
    getAutoSplice,
    getGrayMode,
    getColorWeak,
    getCollapsed
  }
}
