import type { Ref } from 'vue'
import { useRoute, useRouter } from 'vue-router'
import { menus } from '@/router'
import { getParentPaths } from '@/router/menus'
import { AppRouteRecordRaw } from '@/router/types'

export interface IOnlyOneChildMenu extends Partial<AppRouteRecordRaw> {
  noShowingChildren?: boolean
}

export function useMenus() {
  const route = useRoute()

  const { currentRoute, options } = useRouter()

  const isOnlyChildrenMenu = (
    menu: AppRouteRecordRaw,
    onlyOneChild: Ref<Nullable<IOnlyOneChildMenu>>
  ): boolean => {
    const children = menu?.children || []
    const showingChildren = children.filter((item: any) => {
      onlyOneChild.value = item
      return true
    })

    if (showingChildren.length === 0) {
      onlyOneChild.value = { ...menu, noShowingChildren: true }
      return true
    }

    if (showingChildren[0]?.meta?.showParent) {
      onlyOneChild.value = menu
      return false
    }

    if (showingChildren.length === 1) {
      return true
    }

    onlyOneChild.value = { ...menu, noShowingChildren: false }
    return false
  }

  return {
    route,
    currentRoute,
    options,
    menus,
    getParentPaths,
    isOnlyChildrenMenu
  }
}