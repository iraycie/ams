import { ref, unref } from 'vue'
import { useI18n } from '../web/useI18n'
import { message } from 'ant-design-vue'
import { cloneDeep } from 'lodash-es'
import { useModal as useBasicModal } from '@/components/Modal'

const { t } = useI18n()

export function useModal<T = any>(bind = {}) {
  // bind = bind || {}
  const [register, { openModal, closeModal }] = useBasicModal()

  const bindValue = {
    destroyOnClose: true,
    width: '45%',
    bodyStyle: {
      overflow: 'auto',
      maxHeight: '78vh'
      /* margin: '24px 0',
      paddingTop: 0 */
    },
    okText: t('system.modal.confirm'),
    cancelText: t('system.modal.cancel'),
    closeFunc: () => {
      visible.value = false
      return Promise.resolve(true)
    },
    ...bind
  }
  const visible = ref<boolean>(false)
  const currentItem = ref<T>()

  const onChange = function (row?: T) {
    if (row) {
      currentItem.value = cloneDeep(row)
    }
    visible.value = !visible.value
    unref(visible) ? openModal() : closeModal()
  }

  const onConfirm = function <T = any>(error: string, success?: string) {
    /* const handlerResult = useHttp(error, success)

    return async function () {
      if (!isFunction(api)) {
        throw new Error('"api" is not a function!')
      }
      if (!formRef) {
        throw new Error('"formRef" is undefined')
      }
      const result: T = await formRef.value.handleOk()
      if (!result) {
        message.warning(t('system.form.warning'))
        return
      }
      const res = await api(result, currentItem.value?.id)
      handlerResult(res, successCallback)
    } */
  }

  const getInnerForm = async function <T = any>(formRef: any) {
    // const handlerResult = useHttp(error, success)
    const result: T = await unref(formRef).handleOk()
    if (!result) {
      message.warning(t('system.form.warning'))
      return
    }
    return result
  }

  const onClose = function () {
    visible.value = false
    closeModal()
  }

  const onShow = function (row?: T) {
    if (row) {
      currentItem.value = cloneDeep(row)
    }
    visible.value = true
    openModal()
  }

  return {
    visible,
    bindValue,
    register,
    currentItem,
    onChange,
    onClose,
    onConfirm,
    onShow,
    getInnerForm
  }
}

export default useModal
