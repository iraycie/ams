/**
 * @description login types and enums
 * */
export interface LoginInfoType {
  account: string
  password: string
  rememberMe?: boolean
}
export interface PhoneLoginInfoType {
  phone: string
  verifyCode: string
}
export interface RegisterInfoType {
  account: string
  // phone: string
  verifyCode?: string
  password: string
  confirmPassword?: string
  consent?: Boolean
}
export interface ResetInfoType {
  account: string
  // phone: string
  verifyCode?: string
  password: string
  confirmPassword?: string
}

export interface AccountInfoType {
  account: string
  token: string
  avatar: string
  phone: string
}
export interface SendVerifyCodeInfoType {
  account: string
  accountType: string
  operationType: string
}
export interface AccountInformation {
  account?: string
  username?: string
  userId?: string
  avatar?: string
  qrcode?: string
  accessToken: string
  tokenType?: string
  expireTime: number
}

export const accountInformation: AccountInformation = {
  account: '',
  username: '',
  userId: '',
  // avatar: '',
  accessToken: '',
  tokenType: '',
  expireTime: 0
}
