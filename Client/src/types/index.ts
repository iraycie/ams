export type Direction = 'left' | 'center' | 'right'

export interface EnumType {
  value: string | number
  displayName: string | number
}

export interface BasicPageParams {
  pageSize: number
  pageNum: number
}

export interface TimeRange {
  time?: string[]
}

export interface DateTimeRange {
  dateTime?: string[]
}

type DisplayType =
  | 'tag'
  | 'tags'
  | 'check'
  | 'checks'
  | 'rate'
  | 'slider'
  | 'link'
  | 'switch'
  | 'action'

interface EventFn<T = any> {
  (data: T): any
}

export interface ActionType {
  name: string
  event: EventFn
}

export interface RenderColumnsFn<T = any> {
  (data: {
    text?: any
    record: T
    index?: number
    column?: BasicTableColumn
  }): any
}

export interface BasicTableColumn<T = any> {
  title: string
  dataIndex: string
  width?: number
  fixed?: boolean | Direction
  align?: Direction
  ellipsis?: boolean
  checked?: boolean
  rank?: number
  name?: string
  type?: DisplayType
  color?: string
  isCheck?: boolean
  btns?: ActionType[]
  customRender?: RenderColumnsFn<T>
}

export enum Handler {
  Edit,
  Detail,
  Delete
}
