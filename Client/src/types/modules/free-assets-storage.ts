﻿import { AssetsCategory } from './assets-category'
import { Supplier } from './supplier'
import { User } from './user'
import { ApprovalStatus } from './approval-status'

export interface FreeAssetsStorage {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  name?: string
  category?: CategorySelectOptionType
  supplier?: SupplierSelectOptionType
  batch?: string
  count?: number
  univalent?: number
  description?: string
  remark?: string
  commiter?: CommiterSelectOptionType
  status?: ApprovalStatus
}

export interface CategorySelectOptionType {
  id: number
  name: AssetsCategory
}

export interface SupplierSelectOptionType {
  id: number
  name: Supplier
}

export interface CommiterSelectOptionType {
  id: number
  name: User
}

export interface FreeAssetsStorageAddType {
  name?: string
  categoryId?: string | number
  supplierId?: string | number
  batch?: string
  count?: number
  univalent?: number
  description?: string
  remark?: string
}

export interface ConditionSelectorParams {
  keywords?: string
  statusSelector?: ApprovalStatus
}
