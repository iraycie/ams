
export interface AssetsCategory {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  name?: string
}

export interface AssetsCategoryAddType {
  name?: string
}

export type AssetsCategoryEditType = {
  name?: string
}
