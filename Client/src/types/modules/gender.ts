import { EnumType } from "@/types";

export enum Gender {
  Undefined = 0,
  Male = 1,
  Female = 2,
}

export const genderOptions: EnumType[] = [
  {
    value: Gender.Undefined,
    displayName: "未定义"
  },
  {
    value: Gender.Male,
    displayName: "男性"
  },
  {
    value: Gender.Female,
    displayName: "女性"
  },
];