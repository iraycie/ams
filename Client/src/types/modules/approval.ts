﻿import { User } from './user'
import { ApprovalDetail } from './approval-detail'
import { ApprovalType } from './approval-type'
import { AssetsType } from './assets-type'
import { ApprovalStatus } from './approval-status'

export interface Approval {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  type?: ApprovalType
  formId?: number
  assetsType?: AssetsType
  approvalDetails?: ApprovalDetail[]
  remark?: string
  committer?: CommitterSelectOptionType
  status?: ApprovalStatus
}

export interface CommitterSelectOptionType {
  id: number
  name: User
}

export interface ConditionSelectorParams  {
  selectedApprovalStatus: ApprovalStatus
  selectedApprovalType: ApprovalType
  selectedAssetsType: AssetsType
  createDateSelectorBeginDate: Date
  createDateSelectorEndDate: Date
}

export interface CreateDateSelectorParams  {
  beginDate: Date
  endDate: Date
}