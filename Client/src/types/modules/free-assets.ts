﻿import { AssetsCategory } from './assets-category'
import { Supplier } from './supplier'

export interface FreeAssets {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  name?: string
  category?: CategorySelectOptionType
  supplier?: SupplierSelectOptionType
  batch?: string
  description?: string
}

export interface CategorySelectOptionType {
  id: number
  name: AssetsCategory
}

export interface SupplierSelectOptionType {
  id: number
  name: Supplier
}
