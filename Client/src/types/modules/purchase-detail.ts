import { Supplier } from './supplier'

export interface PurchaseDetail {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  name?: string
  count?: number
  univalent?: number
  supplier?: SupplierSelectOptionType
}

export interface SupplierSelectOptionType {
  id: number
  name: Supplier
}

export interface PurchaseDetailAddType {
  name?: string
  count?: number
  univalent?: number
  supplierId?: string | number
}
