﻿import { Department } from './department'
import { Role } from './role'
import { Gender } from './gender'

export interface User {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  account?: string
  jobNumber?: string
  userId?: string
  name?: string
  password?: string
  mobile?: string
  department?: DepartmentSelectOptionType
  gender?: Gender
  email?: string
  isLeader?: boolean
  directLeader?: DirectLeaderSelectOptionType
  avatar?: string
  qrCode?: string
  address?: string
  roles?: RolesSelectOptionType[]
  departmentId?: number
}

export interface DepartmentSelectOptionType {
  id: number
  name: Department
}

export interface DirectLeaderSelectOptionType {
  id: number
  username: User
}

export interface RolesSelectOptionType {
  id: number
  name: Role
}

export type UserEditType = {
  name?: string
  department?: DepartmentSelectOptionType
  departmentId?: string | number
  gender?: Gender
  email?: string
  isLeader?: boolean
  directLeader?: DirectLeaderSelectOptionType
  directLeaderId?: string | number
  address?: string
  roles?: RolesSelectOptionType[]
  rolesId?: string[] | number[]
}

export interface ConditionSelectorParams {
  keywords?: string
  deptId?: number
  gender?: Gender
}
