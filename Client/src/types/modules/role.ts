﻿import { User } from './user'
import { Department } from './department'

export interface Role {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  roleCode?: string
  name?: string
  types?: string[]
  viewRoutes?: string[],
  apiPermissions?: string[],
  routePermissions?: string[],
  ownUsers?: OwnUsersSelectOptionType[]
  ownDepartments?: OwnDepartmentsSelectOptionType[]
}

export interface ApiPermissionInfo {
  controller: string,
  controllerDisplayName?: string,
  apiInfo: any[]
}

export interface RoutePermissionInfo {
  controller: string,
  controllerDisplayName?: string,
  name: string
}

export interface OwnUsersSelectOptionType {
  id: number
  username: User
}

export interface OwnDepartmentsSelectOptionType {
  id: number
  name: Department
}

export interface RoleAddType {
  name?: string
}

export type RoleEditType = {
  name?: string
}