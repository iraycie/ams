﻿import { User } from './user'
import { ApprovalStatus } from './approval-status'

export interface ApprovalDetail {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  sort?: number
  approver?: ApproverSelectOptionType
  remark?: string
  status?: ApprovalStatus
}

export interface ApproverSelectOptionType {
  id: number
  name: User
}
