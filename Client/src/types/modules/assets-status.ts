﻿import { EnumType } from "@/types";

export enum AssetsStatus {
  Rejected = -1,
  Normal = 0,
  Approving = 1,
  Withdrawn = 2,
  Lossing = 3,
  OutOfStock = 4,
}

export const assetsStatusOptions: EnumType[] = [
  {
    value: AssetsStatus.Rejected,
    displayName: "审批遭拒"
  },
  {
    value: AssetsStatus.Normal,
    displayName: "正常"
  },
  {
    value: AssetsStatus.Approving,
    displayName: "待审批"
  },
  {
    value: AssetsStatus.Withdrawn,
    displayName: "已下架"
  },
  {
    value: AssetsStatus.Lossing,
    displayName: "报损中"
  },
  {
    value: AssetsStatus.OutOfStock,
    displayName: "库存缺失"
  },
];