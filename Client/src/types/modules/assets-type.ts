﻿import { EnumType } from "@/types";

export enum AssetsType {
  Consumable = 1,
  Durable = 2,
}

export const assetsTypeOptions: EnumType[] = [
  {
    value: AssetsType.Consumable,
    displayName: "易耗品"
  },
  {
    value: AssetsType.Durable,
    displayName: "耐用品"
  },
];