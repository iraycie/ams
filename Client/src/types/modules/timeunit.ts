import { EnumType } from "@/types";

export enum TimeUnit {
  Minutes = 1,
  Hours = 2,
  Days = 3,
}

export const timeUintOptions: EnumType[] = [
  {
    value: TimeUnit.Minutes,
    displayName: "分钟"
  },
  {
    value: TimeUnit.Hours,
    displayName: "小时"
  },
  {
    value: TimeUnit.Days,
    displayName: "天"
  },
];