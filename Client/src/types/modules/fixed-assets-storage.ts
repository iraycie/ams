﻿import { User } from './user'
import { ApprovalStatus } from './approval-status'
import { AssetsCategory } from './assets-category'
import { Supplier } from './supplier'

export interface FixedAssetsStorage {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  name?: string
  category?: CategorySelectOptionType
  specs?: string
  univalent?: number
  supplier?: SupplierSelectOptionType
  storageLocation?: string
  purchaseDate?: string
  description?: string
  remark?: string
  commiter?: CommiterSelectOptionType
  status?: ApprovalStatus
}

export interface CategorySelectOptionType {
  id: number
  name: AssetsCategory
}

export interface SupplierSelectOptionType {
  id: number
  name: Supplier
}

export interface CommiterSelectOptionType {
  id: number
  name: User
}

export interface FixedAssetsStorageAddType {
  name?: string
  categoryId?: string | number
  specs?: string
  univalent?: number
  supplierId?: string | number
  storageLocation?: string
  purchaseDate?: string
  description?: string
  remark?: string
}

export interface ConditionSelectorParams {
  keywords?: string
  statusSelector?: ApprovalStatus
  supplierSelectorId?: number
  categorySelectorId?: number
}
