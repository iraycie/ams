﻿import { EnumType } from "@/types";

export enum LossStatus {
  Rejected = -1,
  Pending = 0,
  Fixing = 1,
  Succeed = 2,
  Failed = 3,
}

export const lossStatusOptions: EnumType[] = [
  {
    value: LossStatus.Rejected,
    displayName: "审批遭拒"
  },
  {
    value: LossStatus.Pending,
    displayName: "待审批"
  },
  {
    value: LossStatus.Fixing,
    displayName: "维修中"
  },
  {
    value: LossStatus.Succeed,
    displayName: "维修成功"
  },
  {
    value: LossStatus.Failed,
    displayName: "维修失败"
  },
];