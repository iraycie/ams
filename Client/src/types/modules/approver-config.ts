import { ApproverConfigDetail } from './approver-config-detail'

export interface ApproverConfig {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  key?: string
  name?: string
  approverDetails?: ApproverConfigDetail[]
}

export interface Approver{
  id?: number
  name?: string  
}