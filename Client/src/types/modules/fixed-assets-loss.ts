﻿import { FixedAssetsLossDetail } from './fixed-assets-loss-detail'
import { User } from './user'
import { LossStatus } from './loss-status'


export interface FixedAssetsLoss {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  lossDetails?: FixedAssetsLossDetail[]
  remark?: string
  reporter?: ReporterSelectOptionType
  status?: LossStatus
}

export interface ReporterSelectOptionType {
  id: number
  name: User
}

export interface FixedAssetsLossAddType {
  lossDetails?: FixedAssetsLossDetail[]
  remark?: string
}

export interface ConditionSelectorParams {
  lossStatus?: LossStatus
  createDateSelectorBeginDate?: Date
  createDateSelectorEndDate?: Date
}

export interface CreateDateSelectorParams {
  beginDate?: Date
  endDate?: Date
}