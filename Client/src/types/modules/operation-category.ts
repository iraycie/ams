﻿import { EnumType } from "@/types";

export enum OperationCategory {
  Query = 1,
  Add = 2,
  Update = 3,
  Delete = 4,
}

export const operationCategoryOptions: EnumType[] = [
  {
    value: OperationCategory.Query,
    displayName: "查询"
  },
  {
    value: OperationCategory.Add,
    displayName: "新增"
  },
  {
    value: OperationCategory.Update,
    displayName: "更新"
  },
  {
    value: OperationCategory.Delete,
    displayName: "删除"
  },
];