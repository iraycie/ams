﻿import { FixedAssets } from './fixed-assets'
import { User } from './user'
import { ApprovalStatus } from './approval-status'

export interface FixedAssetsConsuming {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  assets?: AssetsSelectOptionType
  useLocation?: string
  remark?: string
  consumer?: ConsumerSelectOptionType
  returning?: boolean
  status?: ApprovalStatus
}

export interface AssetsSelectOptionType {
  id: number
  name: FixedAssets
}

export interface ConsumerSelectOptionType {
  id: number
  name: User
}

export interface FixedAssetsConsumingAddType {
  assetsId?: string | number
  useLocation?: string
  remark?: string
}

export interface ConditionSelectorParams {
  keywords?: string
  statusSelector?: ApprovalStatus
}
