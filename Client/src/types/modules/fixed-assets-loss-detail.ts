import { FixedAssets } from './fixed-assets'

export interface FixedAssetsLossDetail {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  assets?: AssetsSelectOptionType
  remark?: string
}

export interface AssetsSelectOptionType {
  id: number
  name: FixedAssets
}

export interface FixedAssetsLossDetailAddType {
  assetsId?: string | number
  remark?: string
}
