﻿import { User } from './user'
import { Role } from './role'

export interface Department {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  name?: string
  leader?: LeaderSelectOptionType
  parent?: ParentSelectOptionType
  roles?: RolesSelectOptionType[]
  leaderId?: number
}

export interface LeaderSelectOptionType {
  id: number
  username: User
}

export interface ParentSelectOptionType {
  id: number
  name: Department
}

export interface RolesSelectOptionType {
  id: number
  name: Role
}

export interface DepartmentAddType {
  name?: string
  leaderId?: string | number
  parentId?: string | number
  rolesId?: string[] | number[]
}

export type DepartmentEditType = {
  name?: string
  leader?: LeaderSelectOptionType
  leaderId?: string | number
  parent?: ParentSelectOptionType
  parentId?: string | number
  roles?: RolesSelectOptionType[]
  rolesId?: string[] | number[]
}

export interface ConditionSelectorParams {
  selectedRoleId?: number
}
