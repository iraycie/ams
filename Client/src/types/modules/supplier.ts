
export interface Supplier {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  name?: string
  contactor?: string
  phone?: string
}

export interface SupplierAddType {
  name?: string
  contactor?: string
  phone?: string
}

export type SupplierEditType = {
  name?: string
  contactor?: string
  phone?: string
}
