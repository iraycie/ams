﻿import { User } from './user'
import { OperationCategory } from './operation-category'

export interface Log {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  operator?: OperatorSelectOptionType
  operation?: OperationCategory
  formId?: number
  formType?: string
  field?: string
  old?: string
  new?: string
}

export interface OperatorSelectOptionType {
  id: number
  name: User
}

export interface LogSearchParams {
  createdDate: string
  operator: User
  operation: OperationCategory
  formId: number
  formType: string
  field: string
  old: string
  new: string
}

export interface ConditionSelectorParams {
  logOperation?: OperationCategory
  createDateSelectorBeginDate?: Date
  createDateSelectorEndDate?: Date
}

export interface CreateDateSelectorParams {
  beginDate?: Date
  endDate?: Date
}