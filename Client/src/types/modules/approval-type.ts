﻿import { EnumType } from "@/types";

export enum ApprovalType {
  Storage = 1,
  Consuming = 2,
  Return = 3,
  Requisition = 4,
  Loss = 5,
}

export const approvalTypeOptions: EnumType[] = [
  {
    value: ApprovalType.Storage,
    displayName: "资产入库"
  },
  {
    value: ApprovalType.Consuming,
    displayName: "资产领用"
  },
  {
    value: ApprovalType.Return,
    displayName: "资产归还"
  },
  {
    value: ApprovalType.Requisition,
    displayName: "采购申请"
  },
  {
    value: ApprovalType.Loss,
    displayName: "资产报损"
  },
];