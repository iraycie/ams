import { User } from './user'

export interface ApproverConfigDetail {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  approver?: ApproverSelectOptionType
  sort?: number
}

export interface ApproverSelectOptionType {
  id: number
  account: User
}
