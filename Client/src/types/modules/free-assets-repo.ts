﻿import { FreeAssets } from './free-assets'
import { AssetsStatus } from './assets-status'

export interface FreeAssetsRepo {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  assets?: AssetsSelectOptionType
  stock?: number
  status?: AssetsStatus
}

export interface AssetsSelectOptionType {
  id: number
  name: FreeAssets
}

export interface ConditionSelectorParams {
  keywords?: string
  statusSelector?: AssetsStatus
}
