﻿
export interface MaintenanceRecord {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  description?: string
  overhauler?: string
  phone?: string
  repairDate?: string
}

export interface MaintenanceRecordAddType {
  description?: string
  overhauler?: string
  phone?: string
  repairDate?: string
}

export type MaintenanceRecordEditType = {
  description?: string
  overhauler?: string
  phone?: string
  repairDate?: string
}