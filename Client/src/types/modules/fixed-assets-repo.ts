﻿import { FixedAssets } from './fixed-assets'
import { User } from './user'
import { AssetsStatus } from './assets-status'

export interface FixedAssetsRepo {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  assets?: AssetsSelectOptionType
  owner?: OwnerSelectOptionType
  storageLocation?: string
  useLocation?: string
  purchaseDate?: string
  entryDate?: string
  status?: AssetsStatus
}

export interface AssetsSelectOptionType {
  id: number
  name: FixedAssets
}

export interface OwnerSelectOptionType {
  id: number
  name: User
}

export type FixedAssetsRepoEditType = {
  storageLocation?: string
}

export interface ConditionSelectorParams {
  keywords?: string
  statusSelector?: AssetsStatus
}
