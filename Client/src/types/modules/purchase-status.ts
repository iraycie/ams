﻿import { EnumType } from "@/types";

export enum PurchaseStatus {
  Rejected = -1,
  Pending = 0,
  Appropriation = 1,
  Completed = 2,
}

export const purchaseStatusOptions: EnumType[] = [
  {
    value: PurchaseStatus.Rejected,
    displayName: "审批遭拒"
  },
  {
    value: PurchaseStatus.Pending,
    displayName: "等待审批"
  },
  {
    value: PurchaseStatus.Appropriation,
    displayName: "拨款中"
  },
  {
    value: PurchaseStatus.Completed,
    displayName: "采购完成"
  },
];