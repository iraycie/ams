import { BasicPageParams, BasicTableColumn } from '@/types'

export interface LoginInfo {
  account: string
  password: string
}

export interface LoginResult {
  success: boolean
  accessToken: string  
  name: string
}