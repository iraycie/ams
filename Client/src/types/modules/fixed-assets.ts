﻿
export interface FixedAssets {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  serialNo?: string
  name?: string
  specs?: string
  univalent?: number
  supplier?: string
  description?: string
}
