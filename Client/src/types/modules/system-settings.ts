export interface SystemSettings {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  sendMessage?: boolean
  showConsole?: boolean
  systemAdminCanApprove?: boolean
}