﻿import { FreeAssets } from './free-assets'
import { User } from './user'
import { ApprovalStatus } from './approval-status'

export interface FreeAssetsConsuming {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  assets?: AssetsSelectOptionType
  count?: number
  remark?: string
  consumer?: ConsumerSelectOptionType
  status?: ApprovalStatus
}

export interface AssetsSelectOptionType {
  id: number
  name: FreeAssets
}

export interface ConsumerSelectOptionType {
  id: number
  name: User
}

export interface FreeAssetsConsumingAddType {
  assetsId?: string | number
  count?: number
  remark?: string
}

export interface ConditionSelectorParams {
  keywords?: string
  statusSelector?: ApprovalStatus
}
