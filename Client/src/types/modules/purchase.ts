﻿import { PurchaseDetail } from './purchase-detail'
import { User } from './user'
import { PurchaseStatus } from './purchase-status'

export interface Purchase {
  id?: number
  createdDate?: string
  lastModifiedDate?: string
  isDeleted?: boolean
  purchaseDetails?: PurchaseDetail[]
  remark?: string
  commiter?: CommiterSelectOptionType
  status?: PurchaseStatus
}

export interface CommiterSelectOptionType {
  id: number
  name: User
}

export interface PurchaseAddType {
  purchaseDetails?: PurchaseDetail[]
  remark?: string
}

export interface ConditionSelectorParams {
  purchaseStatus?: PurchaseStatus
  createDateSelectorBeginDate?: Date
  createDateSelectorEndDate?: Date
}

export interface CreateDateSelectorParams {
  beginDate?: Date
  endDate?: Date
}