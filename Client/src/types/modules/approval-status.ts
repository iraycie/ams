﻿import { EnumType } from "@/types";

export enum ApprovalStatus {
  Pending = 0,
  Passed = 1,
  Rejected = 2,
  WaitingForLast = 3,
}

export const approvalStatusOptions: EnumType[] = [
  {
    value: ApprovalStatus.Pending,
    displayName: "待审批"
  },
  {
    value: ApprovalStatus.Passed,
    displayName: "通过"
  },
  {
    value: ApprovalStatus.Rejected,
    displayName: "驳回"
  },
  {
    value: ApprovalStatus.WaitingForLast,
    displayName: "等待上一位审批"
  },
];