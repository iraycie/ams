import { SystemSettings } from "./modules/system-settings"

/**
 * @description app setting emun and types
 */
export const prefixCls = 'ap'
export const headerHeight = 48
export const siderWidth = 210

/*
---------------------------------------
--------- layout mode ----------------------
---------------------------------------
*/
export enum LayoutModeEnum {
  DEFAULT = 'DEFAULT',
  TOP_MIXIN = 'TOP_MIXIN',
  TOP = 'TOP',
  LEFT_MIXIN = 'LEFT_MIXIN'
}

export interface ILayoutModeInfo {
  name: string
  type: LayoutModeEnum
  className1?: string
  className2: string
}

export const layoutModesInfo: ILayoutModeInfo[] = [
  {
    name: 'layout.setting.default',
    type: LayoutModeEnum.DEFAULT,
    className1: 'black-left',
    className2: 'gray-left'
  },
  {
    name: 'layout.setting.top',
    type: LayoutModeEnum.TOP,
    className2: 'gray-top'
  },
  {
    name: 'layout.setting.topMixin',
    type: LayoutModeEnum.TOP_MIXIN,
    className1: 'black-top-mixin',
    className2: 'gray-top-mixin'
  },
  // {
  //   name: 'layout.setting.leftMixin',
  //   type: LayoutModeEnum.LEFT_MIXIN,
  //   className1: 'black-left-mixin',
  //   className2: 'gray-left-mixin'
  // }
]

/*
---------------------------------------
--------- I18n ----------------------
---------------------------------------
*/
export type LocaleType = 'zh_CN' | 'en'

export enum LocaleEnum {
  ZH_CN = 'zh_CN',
  EN_US = 'en'
}

export interface LocaleSetting {
  // Current language
  locale: LocaleType
  // default language
  fallback: LocaleType
  // available Locales
  availableLocales: LocaleType[]
}

export const localeList = [
  {
    text: '简体中文',
    event: LocaleEnum.ZH_CN
  },
  {
    text: 'English',
    event: LocaleEnum.EN_US
  }
]
export const localeSetting: LocaleSetting = {
  locale: LocaleEnum.ZH_CN,
  fallback: LocaleEnum.EN_US,
  availableLocales: localeList.map(l => l.event)
}

/*
---------------------------------------
----- Theme ------
---------------------------------------
 */
export enum DarkModeEnum {
  DARK = 'dark',
  LIGHT = 'light'
}

export const darkMode = DarkModeEnum.LIGHT

// system color
export const APP_SYSTEM_COLOR_LIST: string[] = [
  '#0084f4',
  '#0960bd',
  '#009688',
  '#536dfe',
  '#ff5c93',
  '#ee4f12',
  '#0096c7',
  '#9c27b0',
  '#ff9800'
]

// header color
export const APP_HEADER_COLOR_LIST: string[] = [
  '#ffffff',
  '#151515',
  '#009688',
  '#5172DC',
  '#018ffb',
  '#409eff',
  '#e74c3c',
  '#394664',
  '#383f45'
]

// sider color
export const APP_SIDER_COLOR_LIST: string[] = [
  '#ffffff',
  '#151515',
  '#212121',
  '#273352',
  '#191b24',
  '#001628',
  '#28333E',
  '#344058',
  '#383f45'
]

/*
---------------------------------------
----- layout function ------
---------------------------------------
 */
export enum SwitchEnum {
  ON = 'on',
  OFF = 'off'
}

export enum ContentWidthModeEnum {
  FLUID = 'fluid',
  FIXED = 'fixed'
}

export const contentWidthModes = [
  {
    displayName: 'layout.setting.fluid',
    value: ContentWidthModeEnum.FLUID
  },
  {
    displayName: 'layout.setting.fixed',
    value: ContentWidthModeEnum.FIXED
  }
]

/*
---------------------------------------
--------- layout State ----------------------
---------------------------------------
*/
export enum SettingsHandlerEnum {
  LAYOUT_MODE = 'layoutMode',
  DARK_MODE = 'darkMode',
  SYSTEM_THEME_COLOR = 'systemThemeColor',
  HEADER_COLOR = 'headerColor',
  SIDER_COLOR = 'siderColor',
  IS_SHOW_FOOTER = 'isShowFooter',
  IS_SHOW_HEADER = 'isShowHeader',
  IS_SHOW_SIDER = 'isShowSider',
  IS_SHOW_LOGO = 'isShowLogo',
  CONTENT_WIDTH_MODE = 'contentWidthMode',
  FIXED_SIDER = 'fixedSider',
  FIXED_HEADER = 'fixedHeader',
  AUTO_SPLICE = 'autoSplice',
  GRAY_MODE = 'grayMode',
  COLOR_WRAK = 'colorWeak',
  COLLAPSED = 'collapsed',
  SYSTEM_SETTINGS = 'system-settings'
}

export interface ISettingsInfo {
  [SettingsHandlerEnum.LAYOUT_MODE]?: LayoutModeEnum
  [SettingsHandlerEnum.DARK_MODE]?: DarkModeEnum
  [SettingsHandlerEnum.SYSTEM_THEME_COLOR]?: string
  [SettingsHandlerEnum.HEADER_COLOR]?: string
  [SettingsHandlerEnum.SIDER_COLOR]?: string
  [SettingsHandlerEnum.IS_SHOW_FOOTER]?: SwitchEnum
  [SettingsHandlerEnum.IS_SHOW_HEADER]?: SwitchEnum
  [SettingsHandlerEnum.IS_SHOW_SIDER]?: SwitchEnum
  [SettingsHandlerEnum.IS_SHOW_LOGO]?: SwitchEnum
  [SettingsHandlerEnum.CONTENT_WIDTH_MODE]?: ContentWidthModeEnum
  [SettingsHandlerEnum.FIXED_SIDER]?: SwitchEnum
  [SettingsHandlerEnum.FIXED_HEADER]?: SwitchEnum
  [SettingsHandlerEnum.AUTO_SPLICE]?: SwitchEnum
  [SettingsHandlerEnum.GRAY_MODE]?: SwitchEnum
  [SettingsHandlerEnum.COLOR_WRAK]?: SwitchEnum
  [SettingsHandlerEnum.COLLAPSED]?: boolean
  [SettingsHandlerEnum.SYSTEM_SETTINGS]?: SystemSettings
}

export const settingsInfoDefault: Required<ISettingsInfo> = {
  [SettingsHandlerEnum.LAYOUT_MODE]: LayoutModeEnum.DEFAULT,
  [SettingsHandlerEnum.DARK_MODE]: DarkModeEnum.LIGHT,
  [SettingsHandlerEnum.SYSTEM_THEME_COLOR]: '#536dfe',
  [SettingsHandlerEnum.HEADER_COLOR]: APP_HEADER_COLOR_LIST[0],
  [SettingsHandlerEnum.SIDER_COLOR]: APP_SIDER_COLOR_LIST[0],
  [SettingsHandlerEnum.IS_SHOW_FOOTER]: SwitchEnum.OFF,
  [SettingsHandlerEnum.IS_SHOW_HEADER]: SwitchEnum.ON,
  [SettingsHandlerEnum.IS_SHOW_SIDER]: SwitchEnum.ON,
  [SettingsHandlerEnum.IS_SHOW_LOGO]: SwitchEnum.ON,
  [SettingsHandlerEnum.CONTENT_WIDTH_MODE]: ContentWidthModeEnum.FLUID,
  [SettingsHandlerEnum.FIXED_SIDER]: SwitchEnum.ON,
  [SettingsHandlerEnum.FIXED_HEADER]: SwitchEnum.OFF,
  [SettingsHandlerEnum.AUTO_SPLICE]: SwitchEnum.OFF,
  [SettingsHandlerEnum.GRAY_MODE]: SwitchEnum.OFF,
  [SettingsHandlerEnum.COLOR_WRAK]: SwitchEnum.OFF,
  [SettingsHandlerEnum.COLLAPSED]: false,
  [SettingsHandlerEnum.SYSTEM_SETTINGS]: { showConsole: false, sendMessage: false },
}
