export enum Bool {
  'false',
  'true'
}

export type CollectionType = 'boolean' | 'number' | 'dateTime'
