import { App } from 'vue'
import AntDesign from 'ant-design-vue'
import * as antIcons from '@ant-design/icons-vue'

export function registerGlobComp(app: App<Element>) {
  app.use(AntDesign)
  Object.keys(antIcons).forEach(key => {
    app.component(key, antIcons[key])
  })
  app.config.globalProperties.$antIcons = antIcons
}
