﻿import { http } from '../../utils/http'
import { Log, ConditionSelectorParams, CreateDateSelectorParams } from '@/types/modules/log'
import { BasicPageParams } from '@/types'
import type { LogSearchParams } from '@/types/modules/log'
import { User } from '@/types/modules/user';

export const getLogList = () => {
  return http.request<Log[]>('get', '/api/log')
}

export const getLogPage = (params: BasicPageParams & LogSearchParams) => {
  return http.request<Log>('get', '/api/log/page', {
    params
  })
}

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<Log>('get', '/api/log/condition-selector', {
    params
  })
}