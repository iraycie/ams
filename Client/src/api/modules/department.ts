﻿import { http } from '@/utils/http'
import { Department, ConditionSelectorParams } from '@/types/modules/department'
import { BasicPageParams } from '@/types'

export const getDepartmentList = () => {
  return http.request<Department[]>('get', '/api/department')
}

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<Department>('get', '/api/department/condition-selector', {
    params
  })
}

export const createDepartment = (data: Department) => {
  return http.request('post', '/api/department', {
    data
  })
}

export const updateDepartment = (data: Department, id: number) => {
  return http.request('put', `/api/department/${id}`, {
    data
  })
}

export const deleteDepartment = (id: any) => {
  return http.request('delete', `/api/department/${id}`)
}
