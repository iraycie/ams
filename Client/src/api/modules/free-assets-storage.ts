﻿import { http } from '@/utils/http'
import { FreeAssetsStorage, ConditionSelectorParams } from '@/types/modules/free-assets-storage'
import { BasicPageParams } from '@/types'

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<FreeAssetsStorage>('get', '/api/free-assets-storage/condition-selector', {
    params
  })
}

export const createFreeAssetsStorage = (data: FreeAssetsStorage) => {
  return http.request('post', '/api/free-assets-storage', {
    data
  })
}
