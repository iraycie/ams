﻿import { http } from '../../utils/http'
import { EntityBase } from '@/types/modules/entity-base'
import { BasicPageParams } from '@/types'
import type { EntityBaseSearchParams } from '@/types/modules/entity-base'
