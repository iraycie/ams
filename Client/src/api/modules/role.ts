﻿import { http } from '@/utils/http'
import { Role, ApiPermissionInfo, RoutePermissionInfo } from '@/types/modules/role'
import { BasicPageParams } from '@/types'

export const getRoleList = () => {
  return http.request<Role[]>('get', '/api/role')
}

export const getRolePage = (params: BasicPageParams) => {
  return http.request<Role>('get', '/api/role/page', {
    params
  })
}

export const createRole = (data: Role) => {
  return http.request('post', '/api/role', {
    data
  })
}

export const updateRole = (data: Role, id: number) => {
  return http.request('put', `/api/role/${id}`, {
    data
  })
}

export const deleteRole = (id: any) => {
  return http.request('delete', `/api/role/${id}`)
}


export const getApiPermissions = () => {
  return http.request<ApiPermissionInfo[]>('get', '/api/role/api')
}

export const getRoutePermissions = () => {
  return http.request<RoutePermissionInfo[]>('get', '/api/role/route')
}