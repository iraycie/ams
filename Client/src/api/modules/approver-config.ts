import { http } from '@/utils/http'
import { ApproverConfig, Approver } from '@/types/modules/approver-config'

export const getApproverConfigList = () => {
  return http.request<ApproverConfig[]>('get', '/api/approver-config')
}

export const getApproverList = () => {
  return http.request<Approver[]>('get', '/api/approver-config/approver')
}

export const updateApproverConfig = (data: ApproverConfig) => {
  return http.request('put', `/api/approver-config`, {
    data
  })
}
