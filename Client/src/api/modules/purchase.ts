﻿import { http } from '../../utils/http'
import { Purchase, ConditionSelectorParams, CreateDateSelectorParams } from '@/types/modules/purchase'
import { BasicPageParams } from '@/types'
import type { PurchaseSearchParams } from '@/types/modules/purchase'
import { PurchaseDetail } from '@/types/modules/purchase-detail';
import { User } from '@/types/modules/user';

export const getPurchaseList = () => {
  return http.request<Purchase[]>('get', '/api/purchase')
}

export const getPurchasePage = (params: BasicPageParams & PurchaseSearchParams) => {
  return http.request<Purchase>('get', '/api/purchase/page', {
    params
  })
}

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<Purchase>('get', '/api/purchase/condition-selector', {
    params
  })
}

export const createPurchase = (data: Purchase) => {
  return http.request('post', '/api/purchase', {
    data
  })
}

export const updatePurchase = (data: Purchase, id: number | string | undefined) => {
  return http.request('put', `/api/purchase/${id}`, {
    data
  })
}
