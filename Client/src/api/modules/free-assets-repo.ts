﻿import { http } from '@/utils/http'
import { FreeAssetsRepo, ConditionSelectorParams } from '@/types/modules/free-assets-repo'
import { BasicPageParams } from '@/types'

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<FreeAssetsRepo>('get', '/api/free-assets-repo/condition-selector', {
    params
  })
}

export const approvedFreeAssets = () => {
  return http.request('get', `/api/free-assets-repo/approved`)
}