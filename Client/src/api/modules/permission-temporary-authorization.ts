import { http } from '@/utils/http'
import { TimeUnit } from '@/types/modules/timeunit'

export const tryAcquireAsync = (apiPermissions: string[], requireContent: string, step: number, unit: TimeUnit) => {
  var data = { apiPermissions: apiPermissions, requireContent: requireContent, step: step, unit: unit }

  return http.request('post', `/api/permission-temp-auth/acquire`, {
    data
  })
}

export const grant = (key: string) => {
  return http.request('get', `/api/permission-temp-auth/grant?key=${key}`)
}

export const reject = (key: string) => {
  return http.request('get', `/api/permission-temp-auth/reject?key=${key}`)
}