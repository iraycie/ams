﻿import { http } from '@/utils/http'
import { FixedAssetsRepo, ConditionSelectorParams } from '@/types/modules/fixed-assets-repo'
import { BasicPageParams } from '@/types'

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<FixedAssetsRepo>('get', '/api/fixed-assets-repo/condition-selector', {
    params
  })
}

export const updateFixedAssetsRepo = (data: FixedAssetsRepo, id: number) => {
  return http.request('put', `/api/fixed-assets-repo/${id}`, {
    data
  })
}

export const approvedFixedAssets = () => {
  return http.request('get', `/api/fixed-assets-repo/approved`)
}

export const ownedFixedAssets = () => {
  return http.request('get', `/api/fixed-assets-repo/owned`)
}