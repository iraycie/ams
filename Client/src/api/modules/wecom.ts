import { http } from '../../utils/http'

export const organizationSync = () => {
  return http.request('get', `/api/wecom/sync`)
}