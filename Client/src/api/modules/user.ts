﻿import { http } from '@/utils/http'
import { User, ConditionSelectorParams } from '@/types/modules/user'
import { BasicPageParams } from '@/types'

export const getUserList = () => {
  return http.request<User[]>('get', '/api/user')
}

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<User>('get', '/api/user/condition-selector', {
    params
  })
}

export const updateUser = (data: User, id: number) => {
  return http.request('put', `/api/user/${id}`, {
    data
  })
}
