﻿import { http } from '@/utils/http'
import { FixedAssetsStorage, ConditionSelectorParams } from '@/types/modules/fixed-assets-storage'
import { BasicPageParams } from '@/types'

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<FixedAssetsStorage>('get', '/api/fixed-assets-storage/condition-selector', {
    params
  })
}

export const createFixedAssetsStorage = (data: FixedAssetsStorage) => {
  return http.request('post', '/api/fixed-assets-storage', {
    data
  })
}
