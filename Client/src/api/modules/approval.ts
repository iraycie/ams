import { http } from '@/utils/http'
import { Approval, ConditionSelectorParams } from '@/types/modules/approval'
import { BasicPageParams } from '@/types'

export const getApprovalList = () => {
  return http.request<Approval[]>('get', '/api/approval')
}

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<Approval>('get', '/api/approval/condition-selector', {
    params
  })
}

export const approvalPass = (id: number | string | undefined) => {
  return http.request('post', `/api/approval/pass?id=${id}`)
}

export const approvalReject = (id: number | string | undefined, remark: string) => {
  return http.request('post', `/api/approval/reject?id=${id}&remark=${remark}`)
}