import { http } from '@/utils/http'
import { SystemSettings } from '@/types/modules/system-settings'

export const getSystemSettings = () => {
  return http.request<SystemSettings>('get', '/api/system-settings')
}

export const updateSystemSettings = (data: SystemSettings) => {
  return http.request('put', `/api/system-settings`, {
    data
  })
}