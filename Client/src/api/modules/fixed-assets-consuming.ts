﻿import { http } from '@/utils/http'
import { FixedAssetsConsuming, ConditionSelectorParams } from '@/types/modules/fixed-assets-consuming'
import { BasicPageParams } from '@/types'

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<FixedAssetsConsuming>('get', '/api/fixed-assets-consuming/condition-selector', {
    params
  })
}

export const createFixedAssetsConsuming = (data: FixedAssetsConsuming) => {
  return http.request('post', '/api/fixed-assets-consuming', {
    data
  })
}

export const returnFixedAssetsConsuming = (id: number) => {
  return http.request('get', `/api/fixed-assets-consuming/return?id=${id}`)
}
