﻿import { http } from '@/utils/http'
import { MaintenanceRecord } from '@/types/modules/maintenance-record'
import { BasicPageParams } from '@/types'

export const getMaintenanceRecordList = () => {
  return http.request<MaintenanceRecord[]>('get', '/api/maintenance-record')
}

export const getMaintenanceRecordPage = (params: BasicPageParams) => {
  return http.request<MaintenanceRecord>('get', '/api/maintenance-record/page', {
    params
  })
}

export const createMaintenanceRecord = (data: MaintenanceRecord) => {
  return http.request('post', '/api/maintenance-record', {
    data
  })
}

export const updateMaintenanceRecord = (data: MaintenanceRecord, id: number) => {
  return http.request('put', `/api/maintenance-record/${id}`, {
    data
  })
}