﻿import { http } from '@/utils/http'
import { FreeAssetsConsuming, ConditionSelectorParams } from '@/types/modules/free-assets-consuming'
import { BasicPageParams } from '@/types'

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<FreeAssetsConsuming>('get', '/api/free-assets-consuming/condition-selector', {
    params
  })
}

export const createFreeAssetsConsuming = (data: FreeAssetsConsuming) => {
  return http.request('post', '/api/free-assets-consuming', {
    data
  })
}
