import { http } from '@/utils/http'
import { Supplier } from '@/types/modules/supplier'
import { BasicPageParams } from '@/types'

export const getSupplierList = () => {
  return http.request<Supplier[]>('get', '/api/supplier')
}

export const getSupplierPage = (params: BasicPageParams) => {
  return http.request<Supplier>('get', '/api/supplier/page', {
    params
  })
}

export const createSupplier = (data: Supplier) => {
  return http.request('post', '/api/supplier', {
    data
  })
}

export const updateSupplier = (data: Supplier, id: number) => {
  return http.request('put', `/api/supplier/${id}`, {
    data
  })
}

export const deleteSupplier = (id: any) => {
  return http.request('delete', `/api/supplier/${id}`)
}
