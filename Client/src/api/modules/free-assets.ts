﻿import { http } from '../../utils/http'
import { FreeAssets, ConditionSelectorParams } from '@/types/modules/free-assets'
import { BasicPageParams } from '@/types'
import type { FreeAssetsSearchParams } from '@/types/modules/free-assets'

export const getFreeAssetsList = () => {
  return http.request<FreeAssets[]>('get', '/api/free-assets')
}

export const getFreeAssetsPage = (params: BasicPageParams & FreeAssetsSearchParams) => {
  return http.request<FreeAssets>('get', '/api/free-assets/page', {
    params
  })
}

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<FreeAssets>('get', '/api/free-assets/condition-selector', {
    params
  })
}

export const createFreeAssets = (data: FreeAssets) => {
  return http.request('post', '/api/free-assets', {
    data
  })
}

export const updateFreeAssets = (id: number | string | undefined, data: FreeAssets) => {
  return http.request('put', `/api/free-assets/${id}`, {
    data
  })
}

export const approvedFreeAssets = () => {
  return http.request('get', `/api/free-assets/approved`)
}