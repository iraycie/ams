﻿import { http } from '@/utils/http'
import { FixedAssetsLoss, ConditionSelectorParams } from '@/types/modules/fixed-assets-loss'
import { BasicPageParams } from '@/types'

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<FixedAssetsLoss>('get', '/api/loss/condition-selector', {
    params
  })
}

export const createLoss = (data: FixedAssetsLoss) => {
  return http.request('post', '/api/loss', {
    data
  })
}
