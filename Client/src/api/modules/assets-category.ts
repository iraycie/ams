import { http } from '@/utils/http'
import { AssetsCategory } from '@/types/modules/assets-category'
import { BasicPageParams } from '@/types'

export const getAssetsCategoryList = () => {
  return http.request<AssetsCategory[]>('get', '/api/assets-category')
}

export const getAssetsCategoryPage = (params: BasicPageParams) => {
  return http.request<AssetsCategory>('get', '/api/assets-category/page', {
    params
  })
}

export const createAssetsCategory = (data: AssetsCategory) => {
  return http.request('post', '/api/assets-category', {
    data
  })
}

export const updateAssetsCategory = (data: AssetsCategory, id: number) => {
  return http.request('put', `/api/assets-category/${id}`, {
    data
  })
}

export const deleteAssetsCategory = (id: any) => {
  return http.request('delete', `/api/assets-category/${id}`)
}
