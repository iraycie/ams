import { defineStore } from 'pinia'
import {
  LayoutModeEnum,
  SwitchEnum,
  ContentWidthModeEnum,
  DarkModeEnum
} from '@/types/settings'
import { APP_SETTINGS_KEY_ } from '@/types/storage'
import { store } from '@/store'
import {
  SettingsHandlerEnum,
  ISettingsInfo,
  settingsInfoDefault
} from '@/types/settings'
import { createLocalStorage } from '@/utils/cache'
import {
  updateSystemColor,
  updateColorWeak,
  updateDarkTheme,
  updateGrayMode,
  updateHeaderBackground,
  updateSiderBackground
} from '@/utils/theme'
import { SystemSettings } from '@/types/modules/system-settings'

const local = createLocalStorage()

interface ISettingsState {
  settingsInfo: ISettingsInfo
}

const settingsInfo = (local.get(APP_SETTINGS_KEY_) ||
  settingsInfoDefault) as ISettingsInfo

export const useSettingsStore = defineStore({
  id: 'ap-layout',
  state: (): ISettingsState => ({
    settingsInfo
  }),
  getters: {
    getLayoutMode(): LayoutModeEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.LAYOUT_MODE] ??
        settingsInfoDefault[SettingsHandlerEnum.LAYOUT_MODE]
      )
    },
    getDarkMode(): DarkModeEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.DARK_MODE] ??
        settingsInfoDefault[SettingsHandlerEnum.DARK_MODE]
      )
    },
    getSystemThemeColor(): string {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.SYSTEM_THEME_COLOR] ??
        settingsInfoDefault[SettingsHandlerEnum.SYSTEM_THEME_COLOR]
      )
    },
    getHeaderColor(): string {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.HEADER_COLOR] ??
        settingsInfoDefault[SettingsHandlerEnum.HEADER_COLOR]
      )
    },
    getSiderColor(): string {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.SIDER_COLOR] ??
        settingsInfoDefault[SettingsHandlerEnum.SIDER_COLOR]
      )
    },
    getIsShowHeader(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.IS_SHOW_HEADER] ??
        settingsInfoDefault[SettingsHandlerEnum.IS_SHOW_HEADER]
      )
    },
    getIsShowFooter(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.IS_SHOW_FOOTER] ??
        settingsInfoDefault[SettingsHandlerEnum.IS_SHOW_FOOTER]
      )
    },
    getIsShowSider(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.IS_SHOW_SIDER] ??
        settingsInfoDefault[SettingsHandlerEnum.IS_SHOW_SIDER]
      )
    },
    getIsShowLogo(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.IS_SHOW_LOGO] ??
        settingsInfoDefault[SettingsHandlerEnum.IS_SHOW_LOGO]
      )
    },
    getContentWidthMode(): ContentWidthModeEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.CONTENT_WIDTH_MODE] ??
        settingsInfoDefault[SettingsHandlerEnum.CONTENT_WIDTH_MODE]
      )
    },
    getFixedSider(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.FIXED_SIDER] ??
        settingsInfoDefault[SettingsHandlerEnum.FIXED_SIDER]
      )
    },
    getFixedHeader(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.FIXED_HEADER] ??
        settingsInfoDefault[SettingsHandlerEnum.FIXED_HEADER]
      )
    },
    getAutoSplice(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.AUTO_SPLICE] ??
        settingsInfoDefault[SettingsHandlerEnum.AUTO_SPLICE]
      )
    },
    getGrayMode(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.GRAY_MODE] ??
        settingsInfoDefault[SettingsHandlerEnum.GRAY_MODE]
      )
    },
    getColorWeak(): SwitchEnum {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.COLOR_WRAK] ??
        settingsInfoDefault[SettingsHandlerEnum.COLOR_WRAK]
      )
    },
    getCollapsed(): boolean {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.COLLAPSED] ??
        settingsInfoDefault[SettingsHandlerEnum.COLLAPSED]
      )
    },
    getSystemSettings(): SystemSettings {
      return (
        this.settingsInfo?.[SettingsHandlerEnum.SYSTEM_SETTINGS] ??
        settingsInfoDefault[SettingsHandlerEnum.SYSTEM_SETTINGS]
      )
    }
  },
  actions: {
    setSettings(info: Partial<ISettingsInfo>) {
      this.settingsInfo = { ...this.settingsInfo, ...info }
      local.set(APP_SETTINGS_KEY_, this.settingsInfo)
    },
    setLayoutMode(layoutMode: LayoutModeEnum) {
      this.setSettings({ [SettingsHandlerEnum.LAYOUT_MODE]: layoutMode })
    },
    setDarkMode(darkMode: DarkModeEnum) {
      this.setSettings({ [SettingsHandlerEnum.DARK_MODE]: darkMode })
    },
    setSystemThemeColor(systemThemeColor: string) {
      this.setSettings({
        [SettingsHandlerEnum.SYSTEM_THEME_COLOR]: systemThemeColor
      })
    },
    setHeaderColor(headerColor: string) {
      this.setSettings({ [SettingsHandlerEnum.HEADER_COLOR]: headerColor })
    },
    setSiderColor(siderColor: string) {
      this.setSettings({ [SettingsHandlerEnum.SIDER_COLOR]: siderColor })
    },
    setIsShowHeader(isShowHeader: SwitchEnum): void {
      this.setSettings({ [SettingsHandlerEnum.IS_SHOW_HEADER]: isShowHeader })
    },
    setIsShowFooter(isShowFooter: SwitchEnum): void {
      this.setSettings({ [SettingsHandlerEnum.IS_SHOW_FOOTER]: isShowFooter })
    },
    setIsShowSider(isShowSider: SwitchEnum): void {
      this.setSettings({ [SettingsHandlerEnum.IS_SHOW_SIDER]: isShowSider })
    },
    setIsShowLogo(isShowLogo: SwitchEnum): void {
      this.setSettings({ [SettingsHandlerEnum.IS_SHOW_LOGO]: isShowLogo })
    },
    setContentWidthMode(contentWidthMode: ContentWidthModeEnum): void {
      this.setSettings({
        [SettingsHandlerEnum.CONTENT_WIDTH_MODE]: contentWidthMode
      })
    },
    setFixedSider(fixedSider: SwitchEnum): void {
      this.setSettings({ [SettingsHandlerEnum.FIXED_SIDER]: fixedSider })
    },
    setFixedHeader(fixedHeader: SwitchEnum): void {
      this.setSettings({ [SettingsHandlerEnum.FIXED_HEADER]: fixedHeader })
    },
    setAutoSplice(autoSplice: SwitchEnum): void {
      this.setSettings({ [SettingsHandlerEnum.AUTO_SPLICE]: autoSplice })
    },
    setGrayMode(grayMode: SwitchEnum): void {
      updateGrayMode(grayMode === SwitchEnum.ON)
      this.setSettings({ [SettingsHandlerEnum.GRAY_MODE]: grayMode })
    },
    setColorWeak(colorWeak: SwitchEnum): void {
      updateColorWeak(colorWeak === SwitchEnum.ON)
      this.setSettings({ [SettingsHandlerEnum.COLOR_WRAK]: colorWeak })
    },
    setCollapsed(collapsed: boolean): void {
      this.setSettings({ [SettingsHandlerEnum.COLLAPSED]: collapsed })
    },
    setSystemSettings(settings: SystemSettings): void {
      this.setSettings({ [SettingsHandlerEnum.SYSTEM_SETTINGS]: settings })
    },
    resetSettings(): void {
      this.setSettings(settingsInfoDefault)
      this.initSettings()
    },
    initSettings(): void {
      updateDarkTheme(this.getDarkMode)

      updateSystemColor(this.getSystemThemeColor)
      updateHeaderBackground(this.getHeaderColor)
      updateSiderBackground(this.getSiderColor)

      updateColorWeak(this.getColorWeak === SwitchEnum.ON)
      updateGrayMode(this.getGrayMode === SwitchEnum.ON)
    }
  }
})

export function useSettingsStoreWithOut() {
  return useSettingsStore(store)
}
