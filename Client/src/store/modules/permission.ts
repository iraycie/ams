import { defineStore } from 'pinia'
import { store } from '@/store'

import { createLocalStorage } from '@/utils/cache'
import { PERMISSION_KEY } from '@/types/storage'

const local = createLocalStorage()
const lsPermisstion = local.get(PERMISSION_KEY)

export const usePermissionStore = defineStore({
  id: 'permission-store',
  state: () => ({
    permissions: lsPermisstion?.permissions ?? undefined,    
  }),
  getters: {
    getPermissions(): any {
      return (this.permissions ?? {})
    }    
  },
  actions: {
    setPermissions(permissions: any) {
      this.permissions = permissions
      local.set(PERMISSION_KEY, { ...this.$state, permissions })
    }
  }
})

export function usePermissionStoreWithOut() {
  return usePermissionStore(store)
}