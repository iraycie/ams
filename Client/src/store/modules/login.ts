import { message as Message } from 'ant-design-vue'
import type { AccountInformation } from '@/types/login'
import { Status, Result } from '@/utils/http/types.d'
import { LOGINACCOUNT_KEY } from '@/types/storage'
import router from '@/router'
import { useI18n } from '@/hooks/web/useI18n'
import * as api from '@/api/login'

import { defineStore } from 'pinia'
import { store } from '@/store'
import { createLocalStorage } from '@/utils/cache'
import { accountInformation } from '@/types/login'
import { PagePathEnum } from '@/router/types'

const local = createLocalStorage()
const lsLoginAccount = local.get(LOGINACCOUNT_KEY) as AccountInformation

export const useLoginAccountStore = defineStore({
  id: 'login-account-store',
  state: (): AccountInformation => ({
    account: lsLoginAccount?.account ?? accountInformation.account,
    username: lsLoginAccount?.username ?? accountInformation.username,
    userId: lsLoginAccount?.userId ?? accountInformation.userId,
    avatar: lsLoginAccount?.avatar ?? accountInformation.avatar,
    qrcode: lsLoginAccount?.qrcode ?? accountInformation.qrcode,
    accessToken: lsLoginAccount?.accessToken ?? accountInformation.accessToken,
    expireTime: lsLoginAccount?.expireTime ?? accountInformation.expireTime
  }),
  getters: {
    getAccount(): string {
      return (this.account ?? accountInformation.account) as string
    },
    getUsername(): string {     
      let username = (this.username ? this.username : this.account ?? accountInformation.username) as string
      
      return username != '' ? username : 'Unknown';
    },
    getUserId(): string {
      return (this.userId ?? accountInformation.userId) as string
    },
    getAvatar(): string {
      return (this.avatar ?? accountInformation.avatar) as string
    },
    getQrCode(): string {
      return (this.qrcode ?? accountInformation.qrcode) as string
    },
    getToken(): string {
      return this.accessToken ?? accountInformation.accessToken
    }
  },
  actions: {
    setAccount(account: string) {
      this.account = account
      local.set(LOGINACCOUNT_KEY, { ...this.$state, account })
    },
    setUsername(username: string) {
      this.username = username
      local.set(LOGINACCOUNT_KEY, { ...this.$state, username })
    },
    setUserId(userId: string) {
      this.userId = userId
      local.set(LOGINACCOUNT_KEY, { ...this.$state, userId })
    },
    setAvatar(avatar: string) {
      this.avatar = avatar
      local.set(LOGINACCOUNT_KEY, { ...this.$state, avatar })
    },
    setQrCode(qrcode: string) {
      this.qrcode = qrcode
      local.set(LOGINACCOUNT_KEY, { ...this.$state, qrcode })
    },
    setToken(token: string, tokenType?: string) {
      this.accessToken = tokenType ? tokenType + ' ' + token : token
      local.set(LOGINACCOUNT_KEY, {
        ...this.$state,
        accessToken: this.accessToken
      })
    },
    setExpireTime(expireTime: number) {
      this.expireTime = expireTime
      local.set(LOGINACCOUNT_KEY, {
        ...this.$state,
        expireTime
      })
    },
    computeExpireTime(expireIn: number) {
      // return Date.parse(new Date().toString()) + expireIn
      return Math.round(new Date().getTime() / 1000) + expireIn
    },
    checkExpireTime() {
      const expireTime = this.expireTime ? this.expireTime : 0
      // const currentTimeStamp = Date.parse(new Date().toString())
      const currentTimeStamp = Math.round(new Date().getTime() / 1000)
      if (expireTime === 0 || expireTime < currentTimeStamp) {
        //this.removeLoginAccount()
        return true
      }
      return true
    },
    removeLoginAccount() {
      this.$state = { ...accountInformation }
      local.remove(LOGINACCOUNT_KEY)
    },
    initLoginAccount(loginAccount?: AccountInformation) {
      this.setAccount((loginAccount?.account ?? accountInformation.account) as string)
      this.setUsername(
        (loginAccount?.username ?? accountInformation.username) as string
      )
      this.setUserId(
        (loginAccount?.userId ?? accountInformation.userId) as string
      )
      this.setToken(
        loginAccount?.accessToken ?? (accountInformation.accessToken as string),
        loginAccount?.tokenType ?? (accountInformation.tokenType as string)
      )
      this.setExpireTime(
        loginAccount?.expireTime ?? (accountInformation.expireTime as number)
      )
    },

    /**
     * @description: logout
     */
    async logout(logoutParams: any, isGoLogin = false) {
      const { t } = useI18n()
      const res: Result<any> = { status: Status.SUCCESS, message: '' }      
      if (res && res.status === 0) {        
        this.removeLoginAccount()

        isGoLogin && (await router.push(PagePathEnum.BASE_LOGIN))
      } else {
        res.message && Message.error(res.message)
      }
    }
  }
})

export function useLoginAccountStoreWithOut() {
  return useLoginAccountStore(store)
}
