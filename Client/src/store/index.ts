import type { App } from 'vue'
import { createPinia, defineStore } from 'pinia'

const store = createPinia()

export function setupStore(app: App<Element>) {
  app.use(store)
}

export { store }

export const useStore = defineStore('main', {
  state: () => {
    return {
      permissions: {}
    }
  }
})