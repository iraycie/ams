import { computed } from 'vue'
import { getThemeColors, generateColors } from '@build/config/themeConfig'

import { replaceStyleVariables } from 'vite-plugin-theme/es/client'
import {
  mixLighten,
  mixDarken,
  tinycolor
} from 'vite-plugin-theme/es/colorUtils'

import { setCssVar, lighten, darken } from './util'
import { useSettingsStore } from '@/store/modules/layout'
import { SettingsHandlerEnum, DarkModeEnum } from '@/types/settings'

const blackish = ['#151515', '#394664', '#383f45']

const HEADER_BG_COLOR_VAR = '--header-bg-color'
const HEADER_BG_HOVER_COLOR_VAR = '--header-bg-hover-color'
const HEADER_MENU_ACTIVE_BG_COLOR_VAR = '--header-active-menu-bg-color'
const TOP_MENU_ACTIVE_COLOR = '--top-menu-active-color'
const LOGO_BOTTOM_BORDER_COLOR = '--logo-bottom-border-color'

const SIDER_DARK_BG_COLOR = '--sider-dark-bg-color'
const SIDER_DARK_DARKEN_BG_COLOR = '--sider-dark-darken-bg-color'
const SIDER_LIGHTEN_BG_COLOR = '--sider-dark-lighten-bg-color'

const SYSTEM_SELECT_COLOR = '--system-select-color'
const MENU_THEME_COLOR = '--menu-theme-color'
const TOP_THEME_COLOR = '--top-theme-color'

let menuFont = ''

export async function updateSystemColor(color?: string) {
  const settingsStore = useSettingsStore()
  settingsStore.setSettings({ [SettingsHandlerEnum.SYSTEM_THEME_COLOR]: color })

  setCssVar(SYSTEM_SELECT_COLOR, color)

  const colors = generateColors({
    mixDarken,
    mixLighten,
    tinycolor,
    color
  })

  return await replaceStyleVariables({
    colorVariables: [...getThemeColors(color), ...colors]
  })
}

export function updateHeaderBackground(color?: string) {
  const settingsStore = useSettingsStore()

  const isDark = settingsStore.getDarkMode === DarkModeEnum.DARK

  if (!color) color = isDark ? '#151515' : '#ffffff'
  settingsStore.setSettings({ [SettingsHandlerEnum.HEADER_COLOR]: color })

  const hoverColor = lighten(color, 10)
  setCssVar(HEADER_BG_COLOR_VAR, color)
  setCssVar(HEADER_BG_HOVER_COLOR_VAR, hoverColor)
  setCssVar(
    HEADER_MENU_ACTIVE_BG_COLOR_VAR,
    color === '#ffffff' ? '#0960bd' : hoverColor
  )
  setCssVar(TOP_MENU_ACTIVE_COLOR, color === '#ffffff' ? '#d9d9d9' : hoverColor)

  if (settingsStore.getLayoutMode === 'TOP_MIXIN') {
    if (isDark && color === '#ffffff') {
      setCssVar(LOGO_BOTTOM_BORDER_COLOR, '#e5ebe7')
    } else if (!isDark && !blackish.includes(color)) {
      setCssVar(LOGO_BOTTOM_BORDER_COLOR, '#e5ebe7')
    } else {
      setCssVar(LOGO_BOTTOM_BORDER_COLOR, '#3a3a3a')
    }
  }

  setCssVar(TOP_THEME_COLOR, color)
}

export function updateSiderBackground(color?: string) {
  const settingsStore = useSettingsStore()
  const isDark = settingsStore.getDarkMode === DarkModeEnum.DARK

  if (!color) color = isDark ? '#151515' : '#ffffff'
  settingsStore.setSettings({ [SettingsHandlerEnum.SIDER_COLOR]: color })

  setCssVar(SIDER_DARK_BG_COLOR, color)
  setCssVar(SIDER_DARK_DARKEN_BG_COLOR, darken(color, 6))
  setCssVar(SIDER_LIGHTEN_BG_COLOR, lighten(color, 5))
  setCssVar(
    LOGO_BOTTOM_BORDER_COLOR,
    color === '#ffffff' ? '#e5ebe7' : '#3a3a3a'
  )

  if (color === '#ffffff') menuFont = '#000'
  else menuFont = '#ffffff'
  setCssVar(MENU_THEME_COLOR, menuFont)
}
