import type { App } from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBell, faUser } from "@fortawesome/free-regular-svg-icons";
import { faDesktop, faToiletPaper } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

export function setupIcons (app: App<Element>) {
  library.add(...[faBell, faUser, faDesktop, faToiletPaper]);

  app.component('FontAwesomeIcon', FontAwesomeIcon);
}