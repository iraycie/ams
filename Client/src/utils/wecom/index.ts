function isWeComApp() {
  return navigator.userAgent.toLowerCase().indexOf('micromessenger') !== -1;
}

export function DetectEnd(): number {
  // 0：App端企业微信，1：PC端企业微信，2：浏览器
  let flag = navigator.userAgent.match(
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
  ) == null ? false : true;

  return flag ? isWeComApp() ? UA.WeComApp : UA.BrowserApp : isWeComApp() ? UA.WeComClient : UA.Browser;
}

export enum UA {
  WeComApp = 0,
  WeComClient = 1,
  Browser = 2,
  BrowserApp = -1
}