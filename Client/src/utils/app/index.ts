import type { App } from 'vue'
import { getEnumDisplayName } from './enum'
import { formatToDate } from './dateUtil'

import { DetectEnd } from '@/utils/wecom'
import eruda from 'eruda'
import * as api from '@/api/modules/system-settings'
import { SystemSettings } from '@/types/modules/system-settings'
import { useSettingsStoreWithOut } from '@/store/modules/layout'

const settingsStore = useSettingsStoreWithOut()

export default async function initAppUtils(app: App) {
  app.config.globalProperties.$getEnumDisplayName = getEnumDisplayName
  app.config.globalProperties.$formatToDate = formatToDate
  app.config.globalProperties.$ua = DetectEnd()

  let res = await api.getSystemSettings()

  settingsStore.setSystemSettings(res.data as SystemSettings);

  if (res.status == 0) {
    if ((res.data as SystemSettings).showConsole) {
      eruda.init()
    }
  }
}