import NProgress from 'nprogress'
import '@/style/nprogress.less'

NProgress.configure({
  template:
    '<div class="bar" role="bar"><div class="peg"></div></div>',
  // Easing Mode
  easing: 'ease',
  // Speed
  speed: 500,
  // Is show Loading ico
  showSpinner: true,
  // Auto increment interval
  trickleSpeed: 200,
  // Minimum percentage at initialization
  minimum: 0.3
})

export default NProgress
