import { http } from '@/utils/http'
import { BasicPageParams } from '@/types'
import { User, ConditionSelectorParams } from '@/types/modules/user'

export const getConditionSelector = (params: BasicPageParams & ConditionSelectorParams) => {
  return http.request<User>('get', '/api/demo/condition-selector', {
    params
  })
}

export const tryAcquireAsync = (target: string, requireContent: string) => {
  var data = { target: target, requireContent: requireContent }

  return http.request('post', `/api/demo/acquire`, {
    data
  })
}

export const grant = (key: string) => {
  return http.request('post', `/api/demo/grant`, {
    data: key
  })
}

export const reject = (key: string) => {
  return http.request('post', `/api/demo/reject`, {
    data: key
  })
}

export const getAuthData = (key: string, code: string) => {
  return http.request<User>('get', `/api/demo/temp-auth-one?key=${key}&code=${code}`)
}