import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { PluginOption } from 'vite'
import Components from 'unplugin-vue-components/vite'
import { createHtmlPlugin } from 'vite-plugin-html'
import configMockPlugin from './mock'
import configSvgIconsPlugin from './svg'
import { configThemePlugin } from './theme'
import { configStyleImportPlugin } from './styleImport'
import importToCDN from 'vite-plugin-cdn-import';

export function getPluginsList(
  viteEnv: ViteEnv,
  isBuild: boolean
): (PluginOption | PluginOption[])[] {
  const { VITE_USE_MOCK, VITE_APP_TITLE } = viteEnv

  const vitePlugins: (PluginOption | PluginOption[])[] = [
    vue(),
    vueJsx(),
    createHtmlPlugin({
      inject: {
        data: {
          title: VITE_APP_TITLE
        }
      }
    }),
    Components()
  ]

  // vite-plugin-mock
  VITE_USE_MOCK && vitePlugins.push(configMockPlugin(isBuild))

  // vite-plugin-svg-icons
  vitePlugins.push(configSvgIconsPlugin(isBuild))

  // vite-plugin-style-import
  vitePlugins.push(configStyleImportPlugin(isBuild))

  // vite-plugin-theme
  vitePlugins.push(configThemePlugin(isBuild))

  vitePlugins.push(importToCDN({
    modules: [
      {
        name: 'WWLogin',
        var: 'WWLogin',
        path: 'https://wwcdn.weixin.qq.com/node/wework/wwopen/js/wwLogin-1.2.4.js'
      }
    ]
  }))

  return vitePlugins
}